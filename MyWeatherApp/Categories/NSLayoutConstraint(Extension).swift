//
//  ACMWLayoutHelper.swift
//
//
//  Created by Alex Crow on 15-12-22.
//  Copyright © 2015 Alex Crow. All rights reserved.
//

import UIKit
private protocol allignmentConvertion {
	var value: NSLayoutFormatOptions { get }
}

private extension allignmentConvertion {
	func getAllignment() -> NSLayoutFormatOptions {
		return NSLayoutFormatOptions.alignAllCenterY
	}

}
struct metricsOption {
	var superviewTop: Double = 8
	var superviewBottom: Double = 8
	var superviewLeading: Double = 8
	var superviewTrailling: Double = 8
	var innerSpace: Double = 8
	init(top: Double, bottom: Double, leading: Double, trailing: Double, inner: Double) {
		superviewTop = top
		superviewBottom = bottom
		superviewTrailling = trailing
		superviewLeading = leading
		innerSpace = inner
	}

}

enum NSLayoutConstraintVerticalAllignment: allignmentConvertion {
    case top
    case bottom
    case center
    var value: NSLayoutFormatOptions {
        var option: NSLayoutFormatOptions
        switch self {
        case .top: option = .alignAllTop
        case .bottom: option = .alignAllBottom
        case .center: option = .alignAllCenterY
        }
        return option
    }

}

enum NSLayoutConstraintHorizontalAllignment: allignmentConvertion {
    case leading
    case trailling
    case center
    var value: NSLayoutFormatOptions {
        var option: NSLayoutFormatOptions
        switch self {
        case .leading: option = .alignAllLeading
        case .trailling: option = .alignAllTrailing
        case .center: option = .alignAllCenterX
        }
        return option
    }
}

extension NSLayoutConstraint {

    @discardableResult
	func install() -> Bool {
		if self.isUnary(),
			let firstView = self.firstItem as? UIView {
				firstView.addConstraint(self)
				return true
		}

		if let firstView = self.firstItem as? UIView,
			let secondView = self.secondItem as? UIView {
				if let view = firstView.nearestCommonAncestor(forView: secondView) {
					view.addConstraint(self)
					return true
				}
		}
		print("ERROR: NO COMMON ACCESSORS between \(self.firstItem) and \(String(describing: self.secondItem))")
		return false
	}

	func remove() -> Bool {
		if self.isUnary(),
			let firstView = self.firstItem as? UIView {
				firstView.removeConstraint(self)
				return true
		}

		if let firstView = self.firstItem as? UIView,
			let secondView = self.secondItem as? UIView {
				if let view = firstView.nearestCommonAncestor(forView: secondView) {
					view.removeConstraint(self)
					return true
				}
		}
		print("ERROR: NO COMMON ACCESSORS between \(self.firstItem) and \(String(describing: self.secondItem))")
		return false
	}

	class func allignVerticaly(_ views: [UIView],
    horizontalAllignmentOptions option: NSLayoutConstraintHorizontalAllignment,
                boundToSuperView bound: Bool) {
        let format = getFormat(formatAxis.vertical, views: views, leadTop: bound, trailBottom: bound)
		let constraints = NSLayoutConstraint.constraints(withVisualFormat: format, options: option.value, metrics: nil, views: getBindings(views))
		installConstraints(constraints)
		turnOffAutoresizingMask(views)
	}

	class func trailling(_ views: [UIView], option: NSLayoutConstraintVerticalAllignment) {
        let format = getFormat(formatAxis.horizontal, views: views, leadTop: false, trailBottom: true)
		let constraints = NSLayoutConstraint.constraints(withVisualFormat: format, options: option.value, metrics: nil, views: getBindings(views))
		installConstraints(constraints)
	}

	class func leading(_ views: [UIView], option: NSLayoutConstraintVerticalAllignment) {
        let format = getFormat(formatAxis.horizontal, views: views, leadTop: true, trailBottom: false)
		let constraints = NSLayoutConstraint.constraints(withVisualFormat: format, options: option.value, metrics: nil, views: getBindings(views))
		installConstraints(constraints)
	}

	class func top(_ views: [UIView], option: NSLayoutConstraintHorizontalAllignment) {
        let format = getFormat(formatAxis.vertical, views: views, leadTop: true, trailBottom: false)
		let constraints = NSLayoutConstraint.constraints(withVisualFormat: format, options: option.value, metrics: nil, views: getBindings(views))
		installConstraints(constraints)
	}

	class func bottom(_ views: [UIView], option: NSLayoutConstraintHorizontalAllignment) {
        let format = getFormat(formatAxis.vertical, views: views, leadTop: false, trailBottom: true)
		let constraints = NSLayoutConstraint.constraints(withVisualFormat: format, options: option.value, metrics: nil, views: getBindings(views))
		installConstraints(constraints)
	}

	class func centerHorizontallyInSuperview(_ view: UIView) {
		if let superview = getSuperview([view]) {
			let constraint = NSLayoutConstraint(item: view,
                                           attribute: .centerX,
                                           relatedBy: .equal,
                                              toItem: superview,
                                           attribute: .centerX,
                                          multiplier: 1,
                                            constant: 0)
			constraint.install()
		}
		turnOffAutoresizingMask([view])
	}

	class func centerVerticallyInSuperview(_ view: UIView, attribute: NSLayoutAttribute) {
		if let superview = getSuperview([view]) {
			let constraint = NSLayoutConstraint(item: view,
                                           attribute: attribute,
                                           relatedBy: .equal,
                                              toItem: superview,
                                           attribute: .centerY,
                                          multiplier: 1,
                                            constant: 0)
			constraint.install()
		}
		turnOffAutoresizingMask([view])
	}

	class func allignHorizontaly(_ views: [UIView], verticalAllignmentOptions option: NSLayoutConstraintVerticalAllignment, boundToSuperView bound: Bool) {
        let format = getFormat(formatAxis.horizontal, views: views, leadTop: bound, trailBottom: bound)
		let constraints = NSLayoutConstraint.constraints(withVisualFormat: format, options: option.value, metrics: nil, views: getBindings(views))
		for constraint in constraints {
			constraint.install()
		}
		turnOffAutoresizingMask(views)
	}

	class func equalWitdh(_ views: [UIView]) {
		turnOffAutoresizingMask(views)
		installConstraints(getEqualSizeConstraints(.horizontal, views: views))
	}

	class func equalWitdh(_ views: [UIView], multiplier: Double) {
		if let superview = getSuperview(views) {
			for view in views {
				let constraint = NSLayoutConstraint(item: view,
                                               attribute: .width,
                                               relatedBy: .equal,
                                                  toItem: superview,
                                               attribute: .width,
                                              multiplier: CGFloat(multiplier),
                                                constant: 0)
				constraint.install()
			}
		}
		turnOffAutoresizingMask(views)
	}

	class func compressionPriority(_ views: [UIView], priority: Float) {
		for view in views {
			view.setContentCompressionResistancePriority(priority, for: UILayoutConstraintAxis.horizontal)
			view.setContentCompressionResistancePriority(priority, for: UILayoutConstraintAxis.vertical)
		}
	}

	class func hugging(_ views: [UIView], priority: Float) {
		for view in views {
			view.setContentHuggingPriority(priority, for: UILayoutConstraintAxis.horizontal)
			view.setContentHuggingPriority(priority, for: UILayoutConstraintAxis.vertical)
		}
	}

	class func equalHeightToSuperView(_ views: [UIView]) {
		installConstraints(getEqualSizeConstraints(.vertical, views: views))
		turnOffAutoresizingMask(views)
	}

	class func equalHeightToSuperView(_ views: [UIView], multiplier: Double) {
		if let superview = getSuperview(views) {
			for view in views {
				let constraint = NSLayoutConstraint(item: view,
                                               attribute: .height,
                                               relatedBy: .equal,
                                                  toItem: superview,
                                               attribute: .height,
                                              multiplier: CGFloat(multiplier),
                                                constant: 0)
				constraint.install()
				turnOffAutoresizingMask(views)
			}
		}
	}

	class func aspectRatio(_ width: Double, height: Double, views: UIView...) {
		for view in views {
			let constraint = NSLayoutConstraint(item: view,
                                           attribute: .width,
                                           relatedBy: .equal,
                                              toItem: view,
                                           attribute: .height,
                                          multiplier: CGFloat(width / height),
                                            constant: 0)
			constraint.install()
		}
	}

	class func updateConstraintConstant(referenceView view: UIView, value: Double, type: NSLayoutAttribute) {
		if let array = getConstraints(view, attribute: type) {
			for constraint in array {
				if constraint.secondItem!.isEqual(view) {
					constraint.constant = CGFloat(value)
				} else if constraint .firstItem.isEqual(view) {
					constraint.constant = CGFloat(value)
				}
			}

			if array.isEmpty {
				print("\(#function) There are no constraints for this requery ")
			}
		}
	}

	class func changeConstraintRelation(referenceView view: UIView, relation: NSLayoutRelation, attribute: NSLayoutAttribute) {
		var constraintToRemove: NSLayoutConstraint?
		if let array = getConstraints(view, attribute: attribute) {
			for constraint in array {
				if constraint.secondItem!.isEqual(view) {
					constraintToRemove = constraint
				}
			}
		}
		if let constr = constraintToRemove {
			let newConstraint = NSLayoutConstraint(item: constr.firstItem,
                                              attribute: constr.firstAttribute,
                                              relatedBy: relation,
                                                 toItem: constr.secondItem,
                                              attribute: constr.secondAttribute,
                                             multiplier: constr.multiplier,
                                               constant: constr.constant)
			constr.remove()
			newConstraint.install()
		}
	}

	private enum formatAxis: String {
		case horizontal
		case vertical
		func value() -> String {
			switch self {
			case .horizontal: return "H:"
			case .vertical: return "V:"
			}
		}
	}

	private class func getEqualSizeConstraints(_ axisType: formatAxis, views: [UIView]) -> [NSLayoutConstraint] {
		var format: String = axisType.value()
		format += "|"
		var constraints = [NSLayoutConstraint]()

		for (index, _) in views.enumerated() {
			constraints += NSLayoutConstraint.constraints(withVisualFormat: format + "[view\(index)]|",
                                                                   options: .alignAllCenterX,
                                                                   metrics: nil,
                                                                     views: getBindings(views))
		}
		return constraints
	}

	private class func getFormat(_ axisType: formatAxis, views: [UIView], leadTop: Bool, trailBottom: Bool) -> String {
		var format: String = axisType.value()
		if leadTop {
			format += "|"
		}

		for (index, _) in views.enumerated() {
			format += "[view\(index)]"
		}

		if trailBottom {
			format += "|"
		}
		return format
	}

	private class func getBindings(_ views: [UIView]) -> [String: UIView] {
		var bindings = [String: UIView]()

		for (index, view) in views.enumerated() {
			bindings["view\(index)"] = view
		}
		return bindings
	}

	private class func getSuperview(_ views: [UIView]) -> UIView? {
		var view: UIView
		if !views.isEmpty {
			for subview in views {
				if subview.superview == nil {
					fatalError("no superview for view: \(views[0])")
				}
			}
			view = views[0]
			if let superview = view.superview {
				return superview
			} else {
				return nil
			}
		}
		return nil
	}

	private class func turnOffAutoresizingMask(_ views: [UIView]) {
		for view in views {
			view.translatesAutoresizingMaskIntoConstraints = false
		}
	}

	private func isUnary() -> Bool {
		if self.secondItem == nil { return true }
		return false
	}

	private class func getConstraints(_ view: UIView, attribute: NSLayoutAttribute) -> [NSLayoutConstraint]? {
		return (view.superview?.constraintsReferenceView(view))?.filter({ (constraint) -> Bool in
			let firstValue = constraint.firstAttribute.rawValue
			let secondValue = constraint.secondAttribute.rawValue
			let secondView = constraint.secondItem as? UIView
			if firstValue == attribute.rawValue && constraint.firstItem.isEqual(view) ||
			secondView != nil && secondView!.isEqual(constraint.secondItem) && secondValue == attribute.rawValue {
				return true
			} else {
				return false
			}
		})
	}

	private class func installConstraints(_ constraints: [NSLayoutConstraint]) {
		for constraint in constraints {
			constraint.install()
		}
	}
}
