//
//  ACMWWeekDayForecastCell.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-18.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

class ACMWWeekDayForecastCell: ACMWForecastCell {
    var feelsLikeTemperature: ACMWLabel!
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override init(frame: CGRect) {
        let temperatureBuilder = ACMWLabelBuilder.getLabelBuilder(ACMWLabelBuilder.labelTypes.temperature)
        temperatureBuilder.textAllignment = NSTextAlignment.right
        self.feelsLikeTemperature = temperatureBuilder.buildLabel()
        self.feelsLikeTemperature.textColor = ACMWDesignProxy.color(fromHexString: ACMWDAILY_FORECAST_FEEL_COLOR)
        super.init(frame: frame)

    }

     override func configureCell(_ forecastData: ACMWCellData) {
        self.temperatureLabel.text = String(forecastData.temperature)
        self.feelsLikeTemperature.text = String(forecastData.feels_like)
        self.dayLabel.text = forecastData.day.uppercased()
        self.forecastIcon.weatherType = forecastData.weatherIcon
    }

    override internal func configureAutoLayout() {
        self.contentView.addSubviews(forecastIcon, dayLabel, temperatureLabel, feelsLikeTemperature)
        forecastIcon.contentMode = UIViewContentMode.scaleAspectFit
        NSLayoutConstraint.leading([self.dayLabel], option: .center)
        NSLayoutConstraint.trailling([self.temperatureLabel, self.feelsLikeTemperature], option: .center)
        NSLayoutConstraint.equalHeightToSuperView([self.dayLabel, self.temperatureLabel, self.feelsLikeTemperature])
        NSLayoutConstraint.equalHeightToSuperView([self.forecastIcon], multiplier: 0.9)
        NSLayoutConstraint.centerHorizontallyInSuperview(self.forecastIcon)
    }

}
