//
//  ACWundegroundQuadrantsTimeModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-29.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundegroundWindQuadrantsModel: ACMappableProtocol {
    private(set) var comment: String?
    private(set) var quad_1: String?
    private(set) var quad_2: String?
    private(set) var quad_3: String?
    private(set) var quad_4: String?
    init(dict: ACDictionary) {
        self.comment <~ dict["comment"]
        self.quad_1 <~ dict["quad_1"]
        self.quad_2 <~ dict["quad_2"]
        self.quad_3 <~ dict["quad_3"]
        self.quad_4 <~ dict["quad_4"]
    }
}
