//
//  ISearchOutputMock.swift
//  MyWeatherApp
//
//  Created by Alex Hmelevski on 2017-06-25.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import Foundation
import ALEither
@testable import MyWeatherApp

final class SearchOutputMock: ISearchOutput {
    
    private(set) var presentResponses: [ALEither<[AHWundergroundAutocompleteModel], ACMWNetworkError>] = []
    func present(with response: ALEither<[AHWundergroundAutocompleteModel], ACMWNetworkError>) {
        presentResponses.append(response)
    }
}
