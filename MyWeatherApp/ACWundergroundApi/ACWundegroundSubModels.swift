//
//  ACWundegroundEnums.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-28.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

public struct ACWundergroundValue: ACMappableProtocol, Equatable {
    private(set) public var value: ACWundergroundMetric<Double>
    public init(dict: ACDictionary) {
        self.value = ACWundergroundMetric(english: (dict["F"] ?? dict["fahrenheit"] ?? dict["english"] ?? dict["in"]),
                                          metric: (dict["C"] ?? dict["celsius"] ?? dict["metric"] ?? dict["cm"]))
    }
}

public func == (lhs: ACWundergroundValue, rhs: ACWundergroundValue ) -> Bool {
    return lhs.value.English == rhs.value.English && lhs.value.Metric == rhs.value.Metric
}

public struct ACWundegroundTemperatureModel: ACMappableProtocol {
    private(set) public var recordyear: String?
    private(set) public var normal: ACWundergroundValue?
    private(set) public var record: ACWundergroundValue?

    public init(dict: ACDictionary) {
        self.recordyear <~ dict["recordyear"]
        self.normal <~ dict["normal"]
        self.record <~ dict["record"]
    }
}

struct ACWundergroundDewpointModel: ACMappableProtocol {
    private(set) var avg: ACWundergroundValue?
    private(set) var max: ACWundergroundValue?
    private(set) var min: ACWundergroundValue?

    init(dict: ACDictionary) {
        self.avg <~ dict["avg"]
        self.max <~ dict["max"]
        self.min <~ dict["min"]
    }
}

struct ACWundegroundImageModel: ACMappableProtocol {
    private(set) var link: String?
    private(set) var title: String?
    private(set) var url: String?

    init(dict: ACDictionary) {
        self.link <~ dict["link"]
        self.title <~ dict["title"]
        self.url <~ dict["url"]
    }
}
