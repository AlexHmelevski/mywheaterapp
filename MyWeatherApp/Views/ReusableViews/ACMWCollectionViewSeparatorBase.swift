//
//  ACMWCollectionViewSeparatorBase.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 15-10-28.
//  Copyright © 2015 Alex Crow. All rights reserved.
//

import Foundation

class ACMWCollectionViewSeparatorBase: UICollectionReusableView {
    enum SeparatorDirectionType {
        case vertical
        case horizontal
        case defaultType
    }
    enum SeparatorSyleType {
        case doted
        case plain
        case defaultType
    }

    private var imageView: UIImageView!
    private var separatorLayer: CAShapeLayer!

    internal var lineWidth: CGFloat = 0.0 {
        didSet { separatorLayer?.lineWidth = CGFloat(lineWidth) }
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    required override init(frame: CGRect) {
        super.init(frame: frame)
        let proxy = ACMWDesignProxy()
        lineWidth = proxy.separatorWidth(for: self)
    }

    override func layoutSubviews() {
// TODO: Check for best Swift solution
        if separatorLayer == nil {
            separatorLayer = CAShapeLayer()
            layer.addSublayer(separatorLayer)
            let path = UIBezierPath()
            path.move(to: CGPoint.zero)

            let (directionType, styleType) = getSeparatorType()

            if directionType == .horizontal {
                path.move(to: CGPoint(x: 0, y: lineWidth))
                 path.addLine(to: CGPoint(x: frame.width, y: lineWidth))
            } else if directionType == .vertical {
                path.move(to: CGPoint(x: lineWidth, y: 0))
                path.addLine(to: CGPoint(x: lineWidth, y: frame.height))
            }

            if styleType == SeparatorSyleType.doted {
                separatorLayer.lineDashPattern = [2, 3]
            }
            separatorLayer.strokeColor = UIColor.white.cgColor
            separatorLayer.path = path.cgPath
            separatorLayer.lineWidth = lineWidth
        }
    }

    private func getSeparatorType() -> (direction: SeparatorDirectionType, style: SeparatorSyleType) {
        switch self {
            case is ACMWHorizontalPlainSeparator : return (.horizontal, .plain)
            case is ACMWHorizontalDotedSeparator : return (.horizontal, .doted)
            case is ACMWVerticalSeparatorPlain : return (.vertical, . plain)
            case is ACMWVerticalSeparatorDoted : return (.vertical, .doted)
            default: return(.defaultType, .defaultType)
        }
    }

    class func lineWidthForClass() -> CGFloat {
        let proxy = ACMWDesignProxy()
        return proxy.separatorWidth(forKind: kind)
    }
}
