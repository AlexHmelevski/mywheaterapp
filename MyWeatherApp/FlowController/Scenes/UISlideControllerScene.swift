//
//  UISlideControllerScene.swift
//  MyWeatherApp
//
//  Created by Alex Hmelevski on 2017-02-13.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import Foundation

class UISlideControllerScene: IUIScene {

    private weak var vc: UIViewController?
    private let transitionManager = ACMWSlideTransitioningDelegate()

    internal required init(withInitialVC vc: UIViewController) {
        self.vc = vc
    }

    internal func change(completion: (() -> Void)?) {
        let slide = ACMWSlideMenuViewController.create()
        slide.transitioningDelegate = transitionManager
        vc?.present(slide, animated: true, completion: completion)
    }

    deinit {
        print("DEAD")
    }
}
