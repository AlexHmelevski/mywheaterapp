//
//  ACMWSerializer.swift
//  MyWeatherApp
//
//  Created by Alex Hmelevski on 2017-02-01.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import Foundation
import ALEither
import AHFuture
import AHJSONSerializer
protocol ISerializer {

    func serialize<U: ACMappableProtocol>(data: Data) -> AHFuture<U, ACMWNetworkError>
    func serialize<U: JSONDecodable>(data: Data) -> AHFuture<U, ACMWNetworkError>
}

final class ACMWSerializer: ISerializer {

    func serialize<U>(data: Data) -> AHFuture<U, ACMWNetworkError> where U : ACMappableProtocol {
        return AHFuture(scope: { (completion) in
            let dic = try?  JSONSerialization.jsonObject(with: data, options: .allowFragments) as? ACDictionary
            let obj = dic?.map(U.init)
                .map(ALEither<U, ACMWNetworkError>.right)
                ?? .wrong(value: ACMWNetworkError.serializeError(object: data))
            completion(obj)
        })
    }

    func serialize<U>(data: Data) -> AHFuture<U, ACMWNetworkError> where U : JSONDecodable {
        return AHFuture(scope: { (completion) in
            let dic = try?  JSONSerialization.jsonObject(with: data, options: .allowFragments) as? ACDictionary
            let obj = dic?.map(U.init)
                .map(ALEither<U, ACMWNetworkError>.right)
                ?? .wrong(value: ACMWNetworkError.serializeError(object: data))
            completion(obj)
        })
    }
}
