//
//  ACMWWeatherIcon.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 15-12-19.
//  Copyright © 2015 Alex Crow. All rights reserved.
//

import UIKit

class ACMWWeatherIcon: UIImageView {
	let proxy = ACMWDesignProxy()
	required init?(coder aDecoder: NSCoder) {
		weatherType = .Clear
		super.init(coder: aDecoder)
	}
	override init(frame: CGRect) {
		weatherType = .Clear
		super.init(frame: frame)
		self.backgroundColor = .clear
	}

	var weatherType: ACMWForecastType {
		willSet {
			self.image = UIImage(named: newValue.rawValue)
		}
	}
}
