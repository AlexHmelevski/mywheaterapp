//
//  SmallCapsFormatter.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-01-05.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation
protocol AttributedTextFormatter {
    func convertToAttributed(_ text: String) -> NSAttributedString
    var fontName: String { get }
    var fontSize: Double { get set }
    var basicFont: UIFont? { get set }
    init(fontName: String, fontSize: Double)
}

public class ACMWTextFormatter {

    public enum FormatterStyle {
                case temperature
                case textLabels
            }

    public private(set) var fontName: String!
    public var fontSize: Double! {
                didSet {
                    if let font = formatter?.basicFont {
                       self.formatter.basicFont = font.withSize(CGFloat(fontSize))
                       self.formatter.fontSize = fontSize
                    }
                }
            }

    public init(fontName: String, fontSize: Double, type: FormatterStyle) {
        self.fontSize = fontSize
        self.fontName = fontName
        switch type {
            case .temperature: self.formatter = TemperatureTextFormatter(fontName: self.fontName, fontSize: self.fontSize)
            case .textLabels: self.formatter = FakeSmallCapsFormatter(fontName: self.fontName, fontSize: self.fontSize)
        }
    }

    public func convert(_ text: String) -> NSAttributedString {
        //TODO: test for general setUp
        return  self.formatter.convertToAttributed(text)
    }

    private var formatter: AttributedTextFormatter!

}

private class TemperatureTextFormatter: AttributedTextFormatter {

    var fontName: String
    var fontSize: Double
    var basicFont: UIFont?
    var multiplier = 1.0
    func convertToAttributed(_ text: String) -> NSAttributedString {
        let convertedString = NSMutableAttributedString(string: text)
        if let ind = nonNumberCharacterIndex(text) {

            if ind > 0 { multiplier = 0.7 }
            if let smallFont = basicFont?.withSize(CGFloat(fontSize * multiplier)) {
                convertedString.beginEditing()
                convertedString.addAttribute(NSFontAttributeName, value: basicFont!, range: NSRange(location: 0, length: ind))

                let range = NSRange(location: ind, length: convertedString.length - ind)
                convertedString.addAttribute(NSFontAttributeName, value: smallFont, range: range)

                convertedString.addAttribute(NSBaselineOffsetAttributeName, value: fontSize * multiplier * 0.5, range: range)
                convertedString.endEditing()
            }
        } else {
                convertedString.beginEditing()
                convertedString.addAttribute(NSFontAttributeName, value: basicFont!, range: NSRange(location: 0, length: convertedString.length))
                convertedString.endEditing()
        }

        return convertedString
    }

    required init(fontName: String, fontSize: Double) {
        self.fontName = fontName
        self.fontSize = fontSize
        self.basicFont = UIFont(name: fontName, size: CGFloat(fontSize))
    }

    private func nonNumberCharacterIndex(_ string: String) -> Int? {
        let digits = CharacterSet.decimalDigits
        for (index, char) in string.unicodeScalars.enumerated() {
            if !digits.contains(char) && char != "-" {
                return index
            }
        }

        return nil
    }
}

private class FakeSmallCapsFormatter: AttributedTextFormatter {

    var fontName: String
    var fontSize: Double
    var basicFont: UIFont?

    func convertToAttributed(_ text: String) -> NSAttributedString {
        let convertedString = NSMutableAttributedString(string: text)
        guard convertedString.length > 0 else {
            return convertedString
        }

        let font = UIFont.getThickerFont(self.basicFont)
        if let smallFont = font?.withSize(CGFloat(fontSize * 0.7)) {
            convertedString.beginEditing()
            convertedString.addAttribute(NSFontAttributeName, value: basicFont!, range: NSRange(location: 0, length: 1))
            if  convertedString.length > 1 {
            convertedString.addAttribute(NSFontAttributeName, value: smallFont, range: NSRange(location: 1, length: convertedString.length - 1))
            }
            convertedString.endEditing()
        }

        return convertedString
    }

    required init(fontName: String, fontSize: Double) {
        self.fontName = fontName
        self.fontSize = fontSize
        self.basicFont = UIFont(name: fontName, size: CGFloat(fontSize))
    }
}
