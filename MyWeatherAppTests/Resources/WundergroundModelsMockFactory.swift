//
//  WundergroundModelsMockFactory.swift
//  MyWeatherApp
//
//  Created by Alex Hmelevski on 2017-06-25.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import Foundation
import AHJSONSerializer
import ALEither
@testable import MyWeatherApp



final class WundergroundModelsMockFactory {
    
    
    var autocompleteModel: AHWundergroundAutocompleteResultModel {
        return jsonDecoder(for: "AutocompleteResult") » AHWundergroundAutocompleteResultModel.init
    }
    
    
    private func dataFromJSONFile(with name: String) -> Data? {
       return  Bundle(for: WundergroundModelsMockFactory.self as! AnyClass)
                .path(forResource: name, ofType: "json")
                .flatMap(URL.init)
                .flatMap({ try? Data.init(contentsOf: $0) })
    }
    
    private func JSONDictionaryForFile(with name: String) -> [String : Any] {
       return dataFromJSONFile(with: name)
        .flatMap({ try? JSONSerialization.jsonObject(with: $0,        options: .allowFragments)})
                                    .flatMap({$0 as? [String : Any]})
        ?? [ : ]
        
    }
    
    private func jsonDecoder(for fileName: String) -> AHJSONDecoder {
         return AHJSONDecoder(json: JSONDictionaryForFile(with: fileName))
    }
    
    private func serializeToJSON(data: Data) -> [String : Any] {
        return(try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap({$0 as? [String : Any]})
                                                                                       ?? [ : ]
    }
}
