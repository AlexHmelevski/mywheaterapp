//
//  ACMWHourlyForecastCollectionViewCell.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 15-11-30.
//  Copyright © 2015 Alex Crow. All rights reserved.
//

import Foundation

class ACMWHourlyForecastCollectionViewCell: ACMWCollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate {
    private let collectionItemSizeMultiplier: CGFloat = 6.5
    private let collectionItemVerticalSpacing: CGFloat = 5.0
    private let numberOfItemsInSection = 24
    private let numberOfSections = 1
    private let aACMWHourlyForecastCellID = "hourlyForecastCell"
    private let aACMWHourlyForecastCellNib = "ACMWHourlyForecastCell"
    private var dataSource: [ACMWCellData]?

    private var hourlyForecastColletionView: UICollectionView!
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        hourlyForecastColletionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        hourlyForecastColletionView.translatesAutoresizingMaskIntoConstraints = false
        hourlyForecastColletionView.backgroundColor = .clear
        contentView.addSubview(hourlyForecastColletionView)

        if #available(iOS 9.0, *) {
            hourlyForecastColletionView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
            hourlyForecastColletionView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
            hourlyForecastColletionView.widthAnchor.constraint(equalTo: contentView.widthAnchor).isActive = true
            hourlyForecastColletionView.heightAnchor.constraint(equalTo: contentView.heightAnchor).isActive = true

        } else {
            // Fallback on earlier versions
        }

        hourlyForecastColletionView.dataSource = self
        hourlyForecastColletionView.delegate = self
        ACMWForecastCell.registerCellClass(hourlyForecastColletionView)
        layout.itemSize = CGSize(width: frame.size.width / collectionItemSizeMultiplier, height: frame.size.height - (collectionItemVerticalSpacing * 2))
        layout.minimumInteritemSpacing = 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let id = ACMWForecastCellType.HourForecast.description
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: id, for: indexPath) as? ACMWForecastCell else {
            fatalError("unexpected cell tyoe")
        }
        guard let source = self.dataSource else {
            return cell
        }
        cell.configureCell(source[indexPath.row])

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSource?.count ?? 0
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return numberOfSections
    }

    override func configureCell(_ forecastData: [ACMWCellData]) {
        self.dataSource = forecastData
        self.hourlyForecastColletionView.reloadData()
    }
}
