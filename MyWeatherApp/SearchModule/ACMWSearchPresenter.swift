//
//  ACMWSearchOutput.swift
//  MyWeatherApp
//
//  Created by Alex Hmelevski on 2017-06-23.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import Foundation
import ALEither

protocol ISearchOutput {
    func present(with response: ALEither<[AHWundergroundAutocompleteModel], ACMWNetworkError>)
}

enum UIUpdateState: Equatable {
    case loading
    case loaded
    case error(Error)
}

func == (lhs: UIUpdateState, rhs: UIUpdateState) -> Bool {
    switch (lhs, rhs) {
        case (.loading, .loading), (.loaded, .loaded), (.error, .error): return true
    default : return false
    }
}

func != (lhs: UIUpdateState, rhs: UIUpdateState) -> Bool {
    return !(lhs == rhs)
}

struct ACMWSearchResultViewModel {
    let names: [String]
    let state: UIUpdateState
}

final class ACMWSearchPresenter: ISearchOutput {
    var searchViewOutput: ISearchPresentationOutput

    init(viewOutput: ISearchPresentationOutput) {
        self.searchViewOutput = viewOutput
    }

    func present(with response: ALEither<[AHWundergroundAutocompleteModel], ACMWNetworkError>) {
        response.map(transform: { $0.map({ $0.name }) })
                .map(transform: { (names: $0, state: .loaded) })
                .catchError(transform: { (names: [], state: .error($0)) })
                .map(transform: ACMWSearchResultViewModel.init)
                .do(work: self.searchViewOutput.update)
    }
}
