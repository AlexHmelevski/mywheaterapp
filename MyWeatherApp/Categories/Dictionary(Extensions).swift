//
//  Dictionary(Extensions).swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-03.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

// TODO: Find best solution to work only with indexPath

extension Dictionary where Key: ReferenceConvertible, Value: UICollectionViewLayoutAttributes {
	func containsElement(_ elemenKind: String, section: Int) -> [UICollectionViewLayoutAttributes]? {
		let array = self.map({ $0.1 }).filter({ $0.indexPath.section == section && $0.representedElementKind == elemenKind })

		return array
	}

}

extension Dictionary {
	init(setupFunc: (() -> [Key: Value])) {
		self.init()
		for item in setupFunc() {
			self[item.0] = item.1
		}
	}
}
