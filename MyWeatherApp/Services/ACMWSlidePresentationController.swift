//
//  ACMWSlidePresentationController.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-05.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

class ACMWSlidePresentationController: UIPresentationController {
    private lazy var dimmingView = UIView()
    private let sizeMultiplier = 0.7
    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        self.prepareDimmingView()
    }

    override func presentationTransitionWillBegin() {
        guard let containerView = self.containerView else { return }
        self.dimmingView.frame = containerView.bounds
        self.dimmingView.alpha = 0.0
        containerView.insertSubview(self.dimmingView, at: 0)
        self.presentedViewController.transitionCoordinator?.animate(alongsideTransition: { (_) -> Void in
            self.dimmingView.alpha = 1.0
            }, completion: nil)
    }

    override func dismissalTransitionWillBegin() {
        self.presentedViewController.transitionCoordinator?.animate(alongsideTransition: { (_) -> Void in
            self.dimmingView.alpha = 0.0
            }, completion: nil)
    }

    override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
        return CGSize(width: parentSize.width * CGFloat(self.sizeMultiplier), height: parentSize.height)
    }

    override var frameOfPresentedViewInContainerView: CGRect {
        var presentedViewFrame = CGRect.zero
        guard let containerBounds = self.containerView?.bounds else { return presentedViewFrame }
        presentedViewFrame.size = self.size(forChildContentContainer: self.presentedViewController, withParentContainerSize: containerBounds.size)
        presentedViewFrame.origin.x = 0
        return presentedViewFrame
    }

    private func prepareDimmingView() {
        self.dimmingView.backgroundColor = UIColor(white: 0.0, alpha: 0.2)
        self.dimmingView.alpha = 0.0
        let gesture = UITapGestureRecognizer(target: self, action: #selector(ACMWSlidePresentationController.dimmingViewTapped))
        self.dimmingView.addGestureRecognizer(gesture)
    }

    func dimmingViewTapped() {
        self.presentedViewController.dismiss(animated: true, completion: nil)
    }
}
