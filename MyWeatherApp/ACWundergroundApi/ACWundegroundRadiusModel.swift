//
//  ACWundegroundRadiusModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-29.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundegroundRadiusModel: ACMappableProtocol {
    private(set) var NE = String("NA")
    private(set) var NW = String("NA")
    private(set) var SE = String("NA")
    private(set) var SW = String("NA")
    init(dict: ACDictionary) {
        self.NE <~ dict["NE"]
        self.NW <~ dict["NW"]
        self.SE <~ dict["SE"]
        self.SW <~ dict["SW"]
    }
}
