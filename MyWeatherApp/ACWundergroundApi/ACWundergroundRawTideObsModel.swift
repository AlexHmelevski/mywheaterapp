//
//  ACWundergroundRawTideObsModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-30.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundergroundRawTideObsModel: ACMappableProtocol {
    private(set) var epoch: Int?
    private(set) var height: Double?
    init(dict: ACDictionary) {
        self.epoch <~ dict["epoch"]
        self.height <~ dict["height"]
    }
}
