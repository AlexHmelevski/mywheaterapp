//
//  ACWundergroundAstronomy.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-01.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

public struct ACWundergroundAstronomy: ACMappableProtocol {
    private(set) public var ageOfMoon: Int = 0
    private(set) public var hemisphere = String()
    private(set) public var current_time = ACWundegroundTimeModel(dict: ["": "" as AnyObject])
    private(set) public var moonrise = ACWundegroundTimeModel(dict: ["": "" as AnyObject])
    private(set) public var moonset = ACWundegroundTimeModel(dict: ["": "" as AnyObject])
    private(set) public var percentIlluminated: Int = 0
    private(set) public var phaseofMoon = String()
    private(set) public var sunrise = ACWundegroundTimeModel(dict: ["": "" as AnyObject])
    private(set) public var sunset = ACWundegroundTimeModel(dict: ["": "" as AnyObject])

    public init(dict: ACDictionary) {
        self.ageOfMoon <~ dict["ageOfMoon"]
        self.current_time <~ dict["current_time"]
        self.hemisphere <~ dict["hemisphere"]
        self.moonrise <~ dict["moonrise"]
        self.moonset <~ dict["moonset"]
        self.percentIlluminated <~ dict["percentIlluminated"]
        self.phaseofMoon <~ dict["phaseofMoon"]
        self.sunrise <~ dict["sunrise"]
        self.sunset <~ dict["sunset"]
    }
}
