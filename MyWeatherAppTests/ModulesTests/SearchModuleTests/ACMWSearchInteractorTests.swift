//
//  ACMWSearchInteractorTests.swift
//  MyWeatherApp
//
//  Created by Alex Hmelevski on 2017-06-25.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import XCTest
@testable import MyWeatherApp

class ACMWSearchInteractorTests: XCTestCase {
    
    var interactorToTest: ACMWSearchInteractor!
    var outputMock: SearchOutputMock!
    var autocompleteService: AutocompleteServiceMock!
    var exp: XCTestExpectation!
    override func setUp() {
        
        super.setUp()
        outputMock = SearchOutputMock()
        autocompleteService = AutocompleteServiceMock()
        interactorToTest = ACMWSearchInteractor(searchPresenter: outputMock, service: ACWundergroundAutocomplete())
        exp = expectation(description: "SearchTests")
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    
    func test_search_success_calls_output() {
        interactorToTest.search(for: "Montreal")
        
        fullfill(expectation: exp, in:.milliseconds(10))
        wait(for: [exp], timeout: 1)
        
        XCTAssertEqual(outputMock.presentResponses.count, 1)
    }
    
    
    private func fullfill(expectation: XCTestExpectation, in interval: DispatchTimeInterval) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + interval) { 
            expectation.fulfill()
        }
    }
}
