//
//  GlobalConstants.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 15-11-20.
//  Copyright © 2015 Alex Crow. All rights reserved.
//

import Foundation
import ALEither

typealias ALResult<T> = ALEither<T, Error>
public typealias ALWundergroundNetworkResultType = ALEither<ACWundergroundModel, Error>
public typealias ALWundergroundResultType = ALEither<ACWundergroundModel, Error>
public typealias ALWundergroundCompletionClosure = (ALWundergroundResultType) -> Void

public typealias ALVoidClosure = () -> Void
