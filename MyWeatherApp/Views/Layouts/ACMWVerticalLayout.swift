//
//  ACMWVerticalLayout.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-02-26.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import UIKit

class ACMWVerticalLayout: UICollectionViewLayout {
//    private struct CGAttributes {
//        let itemSize: CGSize
//        let initialOffsets: CGVector
//        let sectionInsets: UIEdgeInsets
//        let itemSpacig: CGFloat
//    }

    private let headerHeight = CGFloat(44)
    private var attributesForCell = [IndexPath: ACMWCollectionViewLayoutAttributes]()
    private var attributesForSupplementartView = [IndexPath: ACMWCollectionViewLayoutAttributes]()
    private var dictionaryForDecoration = [IndexPath: ACMWCollectionViewLayoutAttributes]()
    private var cDictionaryForDecoration = [IndexPath: ACMWCollectionViewLayoutAttributes]()
    private var stickyAttribute = ACMWCollectionViewLayoutAttributes()

    private var activeRefreshOffset: CGPoint = .zero

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.preConfigureLayout()
    }

    override init() {
        super.init()
        self.preConfigureLayout()
    }

    override func prepare() {
        super.prepare()

        guard let cv = self.collectionView else {
           return
        }

        var tAttributesForCell = [IndexPath: ACMWCollectionViewLayoutAttributes]()
        var tAttributesForSupplementartView = [IndexPath: ACMWCollectionViewLayoutAttributes]()
        var tDictionaryForDecoration = [IndexPath: ACMWCollectionViewLayoutAttributes]()

        self.cDictionaryForDecoration = self.dictionaryForDecoration

        let sectionCount = cv.numberOfSections

        var yOffset: CGFloat = 0.0
        var xOffset: CGFloat = 0.0

        self.stickyAttribute = ACMWCollectionViewLayoutAttributes(forSupplementaryViewOfKind: ACMWNavigationBar.kind, with: IndexPath(row: 0, section: 0))

        let dy = cv.contentOffset.y > 0 ? cv.contentOffset.y : 0
        self.stickyAttribute.frame = CGRect(origin: CGPoint(x: 0, y: dy), size: CGSize(width: cv.bounds.width, height: self.headerHeight))
        self.stickyAttribute.zIndex = 9999

        for section in 0..<sectionCount {

            guard let initialItemAttr = self.itemSizeAndOffsets(section) else {
                break
            }

            yOffset += initialItemAttr.sectionInsets.top + initialItemAttr.initialOffsets.dy
            xOffset += initialItemAttr.sectionInsets.left + initialItemAttr.initialOffsets.dx

            let itemCount = cv.numberOfItems(inSection: section)
            for row in 0..<itemCount {
                let indexPath = IndexPath(row: row, section: section)

                if section == 2 {

                    //================================ Header Attributes ===============================================
                    if row == 0 {
                    //================================ Decoration Attributes For Detail Cell =============================
                        let dAttr = ACMWCollectionViewLayoutAttributes(forDecorationViewOfKind: ACMWDecorationView.kind, with: indexPath)
                        guard let nextAttr = self.itemSizeAndOffsets(section + 1),
                              let collection = self.collectionView else {
                            break
                        }
                        let headerHeight = cv.frame.height * 0.95 / 17.624
                        let shadowHeight = initialItemAttr.itemSize.height * 2 + CGFloat(collection.numberOfItems(inSection: section + 1)) * nextAttr.itemSize.height + headerHeight

                        dAttr.frame = CGRect(x: initialItemAttr.initialOffsets.dx, y: yOffset, width: cv.frame.width * 0.95, height: shadowHeight)
                        dAttr.zIndex = -1
                        tDictionaryForDecoration[indexPath] = dAttr

                        let sAttr = ACMWCollectionViewLayoutAttributes(forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, with: indexPath)
                        sAttr.frame = CGRect(x: xOffset, y: yOffset, width: initialItemAttr.itemSize.width, height: headerHeight)
                        tAttributesForSupplementartView[indexPath] = sAttr
                        yOffset += headerHeight
                    }
                }

                //================================  Cell Attributes    ===============================================

                let attr = ACMWCollectionViewLayoutAttributes(forCellWith: indexPath)
                attr.frame = CGRect(x: xOffset, y: yOffset, width: initialItemAttr.itemSize.width, height: initialItemAttr.itemSize.height)
                tAttributesForCell[indexPath] = attr

                if section == 1 {

                //================================ Vertical Separators ===============================================
                    xOffset -= initialItemAttr.itemSpacig / 2
                    if indexPath.row > 0 {
                        let dAttr = ACMWCollectionViewLayoutAttributes(forDecorationViewOfKind: ACMWVerticalSeparatorDoted.kind, with: indexPath)
                        dAttr.frame = CGRect(x: xOffset, y: yOffset + 5, width: 1, height: initialItemAttr.itemSize.height - 5)
                        tDictionaryForDecoration[indexPath] = dAttr
                    }

                    xOffset += initialItemAttr.itemSize.width + initialItemAttr.itemSpacig / 2 + initialItemAttr.itemSpacig

                }

                if section == 3 {
                //================================ Horizontal Separators ===============================================
                    let dAttr = ACMWCollectionViewLayoutAttributes(forDecorationViewOfKind: ACMWHorizontalDotedSeparator.kind, with: indexPath)
                    dAttr.frame = CGRect(x: xOffset, y: yOffset - 1, width: initialItemAttr.itemSize.width, height: 1)
                   tDictionaryForDecoration[indexPath] = dAttr

                    if row == itemCount - 1 {
                    //================================ Footter Attributes ===============================================
                        let sAttr = ACMWCollectionViewLayoutAttributes(forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, with: indexPath)
                        yOffset += 1
                        sAttr.frame = CGRect(x: xOffset, y: yOffset + initialItemAttr.itemSize.height, width: initialItemAttr.itemSize.width, height: initialItemAttr.itemSize.height)
                        tAttributesForSupplementartView[indexPath] = sAttr
                    }
                }

                if section != 1 {
                    yOffset += initialItemAttr.itemSize.height
                }
            }

            if section == 1 {
                yOffset += initialItemAttr.itemSize.height
            }

            yOffset += initialItemAttr.sectionInsets.bottom
            xOffset = 0
        }

        self.attributesForCell = tAttributesForCell
        self.attributesForSupplementartView = tAttributesForSupplementartView
        self.dictionaryForDecoration = tDictionaryForDecoration

    }

    override var collectionViewContentSize: CGSize {
        guard let cvSize = self.collectionView?.frame.size else {
            return .zero
        }
        let attributes = dictionaryForDecoration.sorted { (before, after) -> Bool in
            return before.value.frame.origin.y + before.value.frame.size.height < after.value.frame.origin.y + after.value.frame.size.height
        }

        if let last = attributes.last {
            return CGSize(width: cvSize.width, height: last.value.frame.size.height + last.value.frame.origin.y + 10)
        }
        return CGSize(width: cvSize.width, height: cvSize.height)
    }

    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool { return true }

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard let cv = self.collectionView else {
            return nil
        }
        var attributes  = [ACMWCollectionViewLayoutAttributes]()
        let visibleRect = CGRect(x: 0, y: cv.contentOffset.y + self.headerHeight, width: cv.bounds.width, height: cv.bounds.height - self.headerHeight)

        attributes += self.attributesForCell.values.filter({ visibleRect.intersects($0.frame) })
        attributes += self.dictionaryForDecoration.values.filter({ visibleRect.intersects($0.frame) })
        attributes += self.attributesForSupplementartView.values.filter({ visibleRect.intersects($0.frame) })
        self.stickyAttribute.contentOffset = cv.contentOffset

        return attributes + [self.stickyAttribute]
    }

    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        guard let attr = self.attributesForCell[indexPath] else {
            return nil
        }
        return attr
    }

    override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        guard let attr = self.attributesForSupplementartView.containsElement(elementKind, section: indexPath.section) else {
            return nil
        }
        return attr.last
    }

    override func layoutAttributesForDecorationView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        if elementKind == ACMWHorizontalDotedSeparator.kind {
            if let attr = self.dictionaryForDecoration.containsElement(elementKind, section: indexPath.section) {
                return attr.last
            }
        } else if let attr = self.dictionaryForDecoration[indexPath] {
             return attr
        }
        return nil
    }

    override func initialLayoutAttributesForAppearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {

        let attr = self.layoutAttributesForItem(at: IndexPath(row: 4, section: itemIndexPath.section))
        attr?.alpha = 0
        return attr
    }

    override func finalLayoutAttributesForDisappearingDecorationElement(ofKind elementKind: String, at decorationIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let attr = self.layoutAttributesForItem(at: IndexPath(row: 4, section: decorationIndexPath.section))
        attr?.alpha = 0
        return attr
    }

    override func initialLayoutAttributesForAppearingDecorationElement(ofKind elementKind: String, at decorationIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return self.cDictionaryForDecoration[decorationIndexPath]
    }

    private func itemSizeAndOffsets(_ section: Int) -> CGAttributes? {
        guard let cvFrame = self.collectionView?.frame else {
           return nil
        }
        return CGAttributes(frame: cvFrame, section: section)
    }

    private func preConfigureLayout() {
        self.register(ACMWDecorationView.self, forDecorationViewOfKind: ACMWDecorationView.kind)
        self.register(ACMWVerticalSeparatorDoted.self, forDecorationViewOfKind: ACMWVerticalSeparatorDoted.kind)
        self.register(ACMWHorizontalDotedSeparator.self, forDecorationViewOfKind: ACMWHorizontalDotedSeparator.kind)
    }

}
