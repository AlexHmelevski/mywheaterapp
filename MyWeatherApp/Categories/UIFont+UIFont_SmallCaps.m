//
//  UIFont+UIFont_SmallCaps.m
//  MyWeatherApp
//
//  Created by Alex Crow on 2015-08-16.
//  Copyright (c) 2015 Alex Crow. All rights reserved.
//

#import "UIFont+UIFont_SmallCaps.h"
#import <CoreText/CoreText.h>

@implementation UIFont (UIFont_SmallCaps)
+ (UIFont *) applicationSmallCapsFontWithSize:(CGFloat) size {
    
    //  Use this to log all of the properties for a particular font
     UIFont *font = [UIFont fontWithName: @"HelveticaNeue" size: 17];
     CFArrayRef  fontProperties  =  CTFontCopyFeatures ( ( __bridge CTFontRef ) font ) ;
     NSLog(@"properties = %@", fontProperties);

    
    NSArray *fontFeatureSettings = @[ @{ UIFontFeatureTypeIdentifierKey: @(kLowerCaseType),
                                         UIFontFeatureSelectorIdentifierKey : @(kLowerCaseSmallCapsSelector) } ];
    
    NSDictionary *fontAttributes = @{ UIFontDescriptorFeatureSettingsAttribute: fontFeatureSettings ,
                                      UIFontDescriptorNameAttribute: @"LiberationSans-Regular" } ;
    
    UIFontDescriptor *fontDescriptor = [ [UIFontDescriptor alloc] initWithFontAttributes: fontAttributes ];
    
    return [UIFont fontWithDescriptor:fontDescriptor size:size];
    
    
}


@end
