//
//  NSAttributedString+NSString_SmallCaps.h
//  MyWeatherApp
//
//  Created by Alex Crow on 2015-08-16.
//  Copyright (c) 2015 Alex Crow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSAttributedString (NSString_SmallCaps)

@end
