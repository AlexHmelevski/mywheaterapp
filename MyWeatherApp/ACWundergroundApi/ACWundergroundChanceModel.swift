//
//  ACWundergroundChanceModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-30.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundergroundChanceModel: ACMappableProtocol {
    private(set) var description: String?
    private(set) var name: String?
    private(set) var percentage: Double?

    init(dict: ACDictionary) {
        self.description <~ dict["description"]
        self.name <~ dict["name"]
        self.percentage <~ dict["percentage"]
    }
}

struct ACWundergroundChanceOfModel: ACMappableProtocol {
    private(set) var chanceofcloudyday: ACWundergroundChanceModel?
    private(set) var chanceoffogday: ACWundergroundChanceModel?
    private(set) var chanceofhumidday: ACWundergroundChanceModel?
    private(set) var chanceofpartlycloudyday: ACWundergroundChanceModel?
    private(set) var chanceofprecip: ACWundergroundChanceModel?
    private(set) var chanceofhailday: ACWundergroundChanceModel?
    private(set) var chanceofrainday: ACWundergroundChanceModel?
    private(set) var chanceofsnowday: ACWundergroundChanceModel?
    private(set) var chanceofsnowonground: ACWundergroundChanceModel?

    private(set) var chanceofsultryday: ACWundergroundChanceModel?
    private(set) var chanceofsunnycloudyday: ACWundergroundChanceModel?
    private(set) var chanceofthunderday: ACWundergroundChanceModel?
    private(set) var chanceoftornadoday: ACWundergroundChanceModel?
    private(set) var chanceofwindyday: ACWundergroundChanceModel?

    private(set) var tempbelowfreezing: ACWundergroundChanceModel?
    private(set) var tempoverfreezing: ACWundergroundChanceModel?
    private(set) var tempoverninety: ACWundergroundChanceModel?
    private(set) var tempoversixty: ACWundergroundChanceModel?

    init(dict: ACDictionary) {
        self.chanceofcloudyday <~ dict["chanceofcloudyday"]
        self.chanceoffogday <~ dict["chanceoffogday"]
        self.chanceofhumidday <~ dict["chanceofhumidday"]
        self.chanceofpartlycloudyday <~ dict["chanceofpartlycloudyday"]
        self.chanceofprecip <~ dict["chanceofprecip"]
        self.chanceofhailday <~ dict["chanceofhailday"]
        self.chanceofrainday <~ dict["chanceofrainday"]
        self.chanceofsnowday <~ dict["chanceofsnowday"]
        self.chanceofsnowonground <~ dict["chanceofsnowonground"]
        self.chanceofsultryday <~ dict["chanceofsultryday"]
        self.chanceofsunnycloudyday <~ dict["chanceofsunnycloudyday"]
        self.chanceofthunderday <~ dict["chanceofthunderday"]
        self.chanceoftornadoday <~ dict["chanceoftornadoday"]
        self.chanceofwindyday <~ dict["chanceofwindyday"]
        self.tempbelowfreezing <~ dict["tempbelowfreezing"]
        self.tempoverfreezing <~ dict["tempoverfreezing"]
        self.tempoverninety <~ dict["tempoverninety"]
        self.tempoversixty <~ dict["tempoversixty"]

    }
}
