//
//  ACMWShortForecastCell.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-18.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

class ACMWShortForecastCell: ACMWWeekDayForecastCell {
    var shadowLayer: CAShapeLayer?

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

     override func configureCell(_ forecastData: ACMWCellData) {
        self.temperatureLabel.text = String(forecastData.temperature)
        self.forecastIcon.weatherType = forecastData.weatherIcon
        self.dayLabel.text = forecastData.day
        appearanceProxy.label(dayLabel, with: designType())
    }

    override internal func configureAutoLayout() {
        contentView.layer.cornerRadius = frame.size.width * 0.05
        CellGradientDecorator.decorate(self)
        let subviews: [UIView] = [dayLabel, forecastIcon, temperatureLabel]
        self.contentView.addSubviews(dayLabel, forecastIcon, temperatureLabel)
        NSLayoutConstraint.compressionPriority([forecastIcon], priority: UILayoutPriorityDefaultLow - 1)
        NSLayoutConstraint.allignVerticaly(subviews, horizontalAllignmentOptions: .trailling, boundToSuperView: true)
        NSLayoutConstraint.equalWitdh(subviews)
        forecastIcon.contentMode = UIViewContentMode.scaleAspectFit
    }

}
