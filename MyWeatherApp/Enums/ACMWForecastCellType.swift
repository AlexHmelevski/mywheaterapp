//
//  ACMWForecastCellType.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-18.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

enum ACMWForecastCellType: String, CustomStringConvertible {
	case WeekDay
	case ShortForecast
	case FullForecast
	case HourForecast
	case BaseCell

	var description: String {

        return self.rawValue

	}
}
