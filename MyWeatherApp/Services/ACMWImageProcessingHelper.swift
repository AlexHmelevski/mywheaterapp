//
//  ACMWImageProcessingHelper.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-11.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

class ACMWImageProcessing {
    class func cropImage(_ image: UIImage, cropRect: CGRect) -> UIImage? {

        guard let cgImage = image.cgImage else { return nil }
        let context = CIContext(options: nil)
        guard let mainBounds = UIApplication.shared.keyWindow?.bounds else { return nil }
        let scaleHeight = mainBounds.height / image.size.height
        let scaleWidth = mainBounds.width / image.size.width

        let nRect = CGRect(x: 0, y: 2208 - cropRect.height / scaleHeight, width: cropRect.width / scaleWidth, height: cropRect.height / scaleHeight)
        let params = CIVector(cgRect: nRect)
        let ciImage = CIImage(cgImage: cgImage)

        let  filter = CIFilter(name: "CICrop")
        filter?.setValue(ciImage, forKey: kCIInputImageKey)
        filter?.setValue(params, forKey: "inputRectangle")

        if let out = filter?.outputImage,
            let cgimg = context.createCGImage(out, from: out.extent) {

            return UIImage(cgImage: cgimg)
        }
        return nil
    }
}
