//
//  ACMWCollectionForecastFooterView.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 15-10-31.
//  Copyright © 2015 Alex Crow. All rights reserved.
//

import Foundation

@IBDesignable
class ACMWCollectionForecastFooterView: UICollectionReusableView {
    typealias callback = (_ selectedItem: Int) -> Void
    private var plainSeparator: ACMWHorizontalPlainSeparator!
    private var verticalPlain: ACMWVerticalSeparatorPlain!
    private var verticalSeparator: CAShapeLayer!
    private var selectedItem: NSInteger = 0
    private var leftItem: ACMWButton!
    private var rightItem: ACMWButton!
    private var dotedSeparator: ACMWHorizontalDotedSeparator!
    private var userComplition: callback?

    private enum ButtonStrings: CustomStringConvertible {
        case forecast10Days
        case forecast5Days
        var description: String {

            switch self {
                case .forecast10Days : return NSLocalizedString("10D", comment: "")
                case .forecast5Days : return NSLocalizedString("5D", comment: "")
            }
        }

    }

    override func prepareForInterfaceBuilder() {
        backgroundColor = UIColor.red
    }

    required  init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

    }
    override init(frame: CGRect) {
        super.init(frame: frame)

        self.leftItem = ACMWButton(type: UIButtonType.custom)
        self.buttonInitialize(self.leftItem, buttonTitle: "\(ButtonStrings.forecast5Days)")
        self.leftItem.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        self.leftItem.addTarget(self, action: #selector(self.buttonPressed), for: UIControlEvents.touchUpInside)

        verticalPlain = ACMWVerticalSeparatorPlain()
        self.addSubview(verticalPlain)
        verticalPlain.translatesAutoresizingMaskIntoConstraints = false
        verticalPlain.widthAnchor.constraint(equalToConstant: verticalPlain.lineWidth).isActive = true
        verticalPlain.heightAnchor.constraint(equalTo: self.leftItem.heightAnchor, multiplier: 0.2).isActive = true
        verticalPlain.centerYAnchor.constraint(equalTo: self.leftItem.centerYAnchor, constant: 0).isActive = true
        verticalPlain.leadingAnchor.constraint(equalTo: self.leftItem.trailingAnchor, constant: 0).isActive = true

        self.rightItem = ACMWButton(frame: CGRect.zero)
        self.buttonInitialize(self.rightItem, buttonTitle: "\(ButtonStrings.forecast10Days)")
        self.rightItem.leadingAnchor.constraint(equalTo: verticalPlain.trailingAnchor, constant: 0).isActive = true
        self.rightItem.addTarget(self, action: #selector(self.buttonPressed), for: UIControlEvents.touchUpInside)

        dotedSeparator = ACMWHorizontalDotedSeparator()
        self.addSubview(dotedSeparator)
        dotedSeparator.translatesAutoresizingMaskIntoConstraints = false
        dotedSeparator.widthAnchor.constraint(equalTo: self.widthAnchor, constant: 0).isActive = true
        dotedSeparator.heightAnchor.constraint(equalToConstant: dotedSeparator.lineWidth).isActive = true
        dotedSeparator.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        dotedSeparator.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true

        plainSeparator = ACMWHorizontalPlainSeparator()
        self.addSubview(plainSeparator)
        plainSeparator.translatesAutoresizingMaskIntoConstraints = false
        plainSeparator.widthAnchor.constraint(equalTo: self.widthAnchor, constant: 0).isActive = true
        plainSeparator.heightAnchor.constraint(equalToConstant: dotedSeparator.lineWidth).isActive = true
        plainSeparator.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        plainSeparator.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        plainSeparator.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true

        self.leftItem.isSelected = true
        self.rightItem.isSelected = false
    }

    private func buttonInitialize(_ button: UIButton, buttonTitle: String) {
        let proxy = ACMWDesignProxy()
        proxy.label(button.titleLabel, with: ACMWForecastFooter)
        button.setAttributedTitle(proxy.smallCapsString(from: buttonTitle, with:  ACMWForecastFooter), for: UIControlState())
        self.addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 1).isActive = true
        button.widthAnchor.constraint(equalTo: button.widthAnchor, multiplier: 1).isActive = true
        button.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        button.setTitleColor(.white, for: UIControlState.selected)
        button.setTitleColor(.gray, for: UIControlState())

    }

   @objc func buttonPressed(_ sender: UIButton!) {
        if sender == self.leftItem {
            self.leftItem.isSelected = true
            self.selectedItem = 0
        } else {
            self.leftItem.isSelected = false
            self.selectedItem = 1
        }
        self.rightItem.isSelected = !self.leftItem.isSelected
        self.leftItem.isUserInteractionEnabled = !self.leftItem.isSelected
        self.rightItem.isUserInteractionEnabled = self.leftItem.isSelected
        self.userComplition?(self.selectedItem)
    }

    func addButtonSelectedCallBack(selectedButtonFunction function: @escaping callback) {
        self.userComplition = function
    }

}
