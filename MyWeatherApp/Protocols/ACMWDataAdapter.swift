//
//  ACMWDataAdapter.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-20.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

protocol ACMWDataConverter {
	func convert(_ data: ACWundergroundModel, metricSytem: ACWundergroundMetricType) -> [ACMWCellData]
}
