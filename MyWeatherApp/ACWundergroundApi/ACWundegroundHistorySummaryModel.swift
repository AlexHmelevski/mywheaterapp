//
//  ACWundegroundHistorySummaryModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-01.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundegroundHistorySummaryModel: ACMappableProtocol {
    private(set) var conds: String?
    private(set) var coolingdegreedays: Int?
    private(set) var coolingdegreedaysnormal: String?
    private(set) var date: ACWundegroundTimeModel?
    private(set) var fog: Int?
    private(set) var gdegreedays: Int?
    private(set) var hail: Int?
    private(set) var heatingdegreedays: Int?
    private(set) var heatingdegreedaysnormal: String?
    private(set) var humidity: Int?
    private(set) var maxdewpti: Int?
    private(set) var maxdewptm: Int?
    private(set) var maxhumidity: Int?
    private(set) var maxpressurei: Double?
    private(set) var maxpressurem: Double?
    private(set) var maxtempi: Int?
    private(set) var maxtempm: Int?
    private(set) var maxvisi: Double?
    private(set) var maxvism: Double?
    private(set) var maxwspdi: Int?
    private(set) var maxwspdm: Int?
    private(set) var meandewpti: Int?
    private(set) var meandewptm: Int?

    private(set) var meanpressurei: Double?
    private(set) var meanpressurem: Double?
    private(set) var meantempi: Int?
    private(set) var meantempm: Int?
    private(set) var meanvisi: Double?

    private(set) var meanvism: Double?
    private(set) var meanwdird: Int?
    private(set) var meanwdire: String?

    private(set) var meanwindspdi: Int?
    private(set) var meanwindspdm: Int?
    private(set) var mindewpti: Int?
    private(set) var mindewptm: Int?
    private(set) var minhumidity: Int?
    private(set) var minpressurei: Double?
    private(set) var minpressurem: Double?
    private(set) var mintempi: Int?
    private(set) var mintempm: Int?

    private(set) var minvisi: Double?
    private(set) var minvism: Double?
    private(set) var minwspdi: Int?
    private(set) var minwspdm: Int?

    private(set) var monthtodatecoolingdegreedays: String?
    private(set) var monthtodatecoolingdegreedaysnormal: String?
    private(set) var monthtodateheatingdegreedays: String?
    private(set) var monthtodateheatingdegreedaysnormal: String?
    private(set) var monthtodatesnowfalli: String?
    private(set) var monthtodatesnowfallm: String?
    private(set) var precipi: String?
    private(set) var precipm: Int?
    private(set) var precipsource: String?
    private(set) var rain: Int?

    private(set) var since1jancoolingdegreedays: String?
    private(set) var since1jancoolingdegreedaysnormal: String?
    private(set) var since1julheatingdegreedays: String?
    private(set) var since1julheatingdegreedaysnormal: String?
    private(set) var since1julsnowfalli: String?
    private(set) var since1julsnowfallm: String?
    private(set) var since1sepcoolingdegreedays: String?
    private(set) var since1sepcoolingdegreedaysnormal: String?
    private(set) var since1sepheatingdegreedays: String?
    private(set) var since1sepheatingdegreedaysnormal: String?
    private(set) var snow: Int?
    private(set) var snowdepthi: Int?
    private(set) var snowdepthm: Int?
    private(set) var snowfalli: Int?
    private(set) var snowfallm: Int?
    private(set) var thunder: Int?
    private(set) var tornado: Int?

    init(dict: ACDictionary) {
        self.conds <~ dict["conds"]
        self.coolingdegreedays <~ dict["coolingdegreedays"]
        self.coolingdegreedaysnormal <~ dict["coolingdegreedaysnormal"]
        self.date <~ dict["date"]
        self.fog <~ dict["fog"]
        self.gdegreedays <~ dict["gdegreedays"]
        self.hail <~ dict["hail"]
        self.heatingdegreedays <~ dict["heatingdegreedays"]
        self.heatingdegreedaysnormal <~ dict["heatingdegreedaysnormal"]
        self.humidity <~ dict["humidity"]
        self.maxdewpti <~ dict["maxdewpti"]
        self.maxdewptm <~ dict["maxdewptm"]
        self.maxhumidity <~ dict["maxhumidity"]
        self.maxpressurei <~ dict["maxpressurei"]
        self.maxpressurem <~ dict["maxpressurem"]
        self.maxtempi <~ dict["maxtempi"]
        self.maxtempm <~ dict["maxtempm"]
        self.maxvisi <~ dict["maxvisi"]
        self.maxvism <~ dict["maxvism"]
        self.maxwspdi <~ dict["maxwspdi"]
        self.maxwspdm <~ dict["maxwspdm"]
        self.meandewpti <~ dict["meandewpti"]
        self.meandewptm <~ dict["meandewptm"]
        self.meanpressurei <~ dict["meanpressurei"]
        self.meanpressurem <~ dict["meanpressurem"]
        self.meantempi <~ dict["meantempi"]
        self.meantempm <~ dict["meantempm"]
        self.meanvisi <~ dict["meanvisi"]
        self.meanvism <~ dict["meanvism"]
        self.meanwdird <~ dict["meanwdird"]
        self.meanwdire <~ dict["meanwdire"]
        self.meanwindspdi <~ dict["meanwindspdi"]
        self.meanwindspdm <~ dict["meanwindspdm"]
        self.mindewpti <~ dict["mindewpti"]
        self.mindewptm <~ dict["mindewptm"]
        self.minhumidity <~ dict["minhumidity"]
        self.minpressurei <~ dict["minpressurei"]
        self.minpressurem <~ dict["minpressurem"]
        self.mintempi <~ dict["mintempi"]
        self.mintempm <~ dict["mintempm"]
        self.minvisi <~ dict["minvisi"]
        self.minvism <~ dict["minvism"]
        self.minwspdi <~ dict["minwspdi"]
        self.minwspdm <~ dict["minwspdm"]
        self.monthtodatecoolingdegreedays <~ dict["monthtodatecoolingdegreedays"]
        self.monthtodatecoolingdegreedaysnormal <~ dict["monthtodatecoolingdegreedaysnormal"]
        self.monthtodateheatingdegreedays <~ dict["monthtodateheatingdegreedays"]
        self.monthtodateheatingdegreedaysnormal <~ dict["monthtodateheatingdegreedaysnormal"]
        self.monthtodatesnowfalli <~ dict["monthtodatesnowfalli"]
        self.monthtodatesnowfallm <~ dict["monthtodatesnowfallm"]
        self.precipi <~ dict["precipi"]
        self.precipm <~ dict["precipm"]
        self.precipsource <~ dict["precipsource"]
        self.rain <~ dict["rain"]
        self.since1jancoolingdegreedays <~ dict["since1jancoolingdegreedays"]
        self.since1jancoolingdegreedaysnormal <~ dict["since1jancoolingdegreedaysnormal"]
        self.since1julheatingdegreedays <~ dict["since1julheatingdegreedays"]
        self.since1julheatingdegreedaysnormal <~ dict["since1julheatingdegreedaysnormal"]
        self.since1julsnowfalli <~ dict["since1julsnowfalli"]
        self.since1julsnowfallm <~ dict["since1julsnowfallm"]
        self.since1sepcoolingdegreedays <~ dict["since1sepcoolingdegreedays"]
        self.since1sepcoolingdegreedaysnormal <~ dict["since1sepcoolingdegreedaysnormal"]
        self.since1sepheatingdegreedays <~ dict["since1sepheatingdegreedays"]
        self.since1sepheatingdegreedaysnormal <~ dict["since1sepheatingdegreedaysnormal"]
        self.snow <~ dict["snow"]
        self.snowdepthi <~ dict["snowdepthi"]
        self.snowdepthm <~ dict["snowdepthm"]
        self.snowfalli <~ dict["snowfalli"]
        self.snowfallm <~ dict["snowfallm"]
        self.thunder <~ dict["thunder"]
        self.tornado <~ dict["tornado"]
    }
}
