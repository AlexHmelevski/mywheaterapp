//
//  ACWundergroundTideModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-30.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation
struct ACWundergroundTideModel: ACMappableProtocol {
    private(set) var tideInfo: [ACWundergroundTideInfoModel]?
    private(set) var tideSummary: [ACWundergroundTideSummaryModel]?
    private(set) var tideSummaryStats: [ACWundergroundRawTideStatsModel]?

    init(dict: ACDictionary) {
        self.tideSummary <~ dict["tideSummary"]
        self.tideSummaryStats <~ dict["tideSummaryStats"]
        self.tideInfo <~ dict["tideInfo"]

    }
}
