//
//  ACMWMainForecastRouterModule.swift
//  MyWeatherApp
//
//  Created by Alex Hmelevski on 2017-06-28.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import Foundation

protocol ForecastModuleDelegate: class {
    func didPressAdd()
    func didPressSettings()
}

final class ACMWMainForecastRouterModule: IRouterModule {

    var viewController: UIViewController {
        return vc
    }
    private let vc: ACMWCityForecastViewController
    private let mediator: ACMWMainForecastMediator

    init(types: [AHWundegroundQueryType], delegate: ForecastModuleDelegate? = nil) {
        vc = ACMWCityForecastViewController(source: ACMWCollectionViewDataSource())
        let service = ACMWForecastService(with: types)
        let presenter = ACMWMainForecastPresenter(output: vc)
        let interactor = ACMWMainForecastInteractor(forecastService: service, presenter: presenter)
        mediator = ACMWMainForecastMediator(interactor: interactor, delegate: delegate)
        vc.output = mediator
    }

}
