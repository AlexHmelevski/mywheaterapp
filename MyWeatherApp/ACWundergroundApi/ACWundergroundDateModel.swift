//
//  ACWundergroundDateModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-30.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation
struct ACWundergroundDateModel: ACMappableProtocol {
    private(set) var date: ACWundegroundTimeModel?

    init(dict: ACDictionary) {
        self.date <~ dict["date"]
    }
}
