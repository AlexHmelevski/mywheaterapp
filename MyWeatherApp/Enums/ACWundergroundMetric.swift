//
//  ACWundergroundMetric.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-19.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

public enum ACWundergroundMetricType {
	case english
	case metric
}

public struct ACWundergroundMetric<Value: PrimitiveTypes>: Equatable where Value: Equatable {
	private(set) var English: Value
	private(set) var Metric: Value
	private var tempEngl: Value?
	private var tempMetric: Value?

    init(english: Any?, metric: Any?) {
        let str1 = english.flatMap({ $0 }).map({ String(describing: $0) }) ?? "0"
        let str2 = metric.flatMap({ $0 }).map({ String(describing: $0) }) ?? "0"
        self = ACWundergroundMetric<Value>(english:str1, metric:str2)
    }

	init(english: String, metric: String) {
        self.tempEngl <~ english
        self.tempMetric <~ metric

        English = tempEngl!
        Metric = tempMetric!

	}

	func valueForType(_ type: ACWundergroundMetricType) -> Value {
		switch type {
		case .english: return self.English
		case .metric: return self.Metric
		}

	}
}

public func == <Value: Equatable>(lhs: ACWundergroundMetric<Value>, rhs: ACWundergroundMetric<Value>) -> Bool {

	return lhs.English == rhs.English && lhs.Metric == rhs.Metric
}
