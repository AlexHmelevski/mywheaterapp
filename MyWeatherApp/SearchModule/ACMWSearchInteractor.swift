//
//  ACMWSearchInteractor.swift
//  MyWeatherApp
//
//  Created by Alex Hmelevski on 2017-06-23.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import Foundation
import AHFuture

struct ACMWSearchRestultDataModel {
    let name: String
    let queryZMW: String
}
protocol ISearchInteractor {
    func search(for location: String)
    func model(at index: Int) -> ACMWSearchRestultDataModel?
}

protocol IAutocompleteService {
    func getAutocompleteResults(for text: String) -> AHFuture<AHWundergroundAutocompleteResultModel, ACMWNetworkError>
}

extension ACWundergroundAutocomplete: IAutocompleteService {}

final class ACMWSearchInteractor {
    fileprivate let searchPresenter: ISearchOutput
    fileprivate let service: IAutocompleteService
    fileprivate var results: [AHWundergroundAutocompleteModel] = []

    init(searchPresenter: ISearchOutput,
         service: IAutocompleteService = ACWundergroundAutocomplete()) {
        self.searchPresenter = searchPresenter
        self.service = service
    }
}

extension ACMWSearchInteractor: ISearchInteractor {

    func search(for location: String) {
        service.getAutocompleteResults(for: location)
               .map(transform: { $0.results.filter({ $0.type.lowercased() == "city" }) })
               .onSuccess(callback: {[weak self] (results) in
                 self?.results = results.filter({ $0.name.contains(location) })
               })
               .onComplete(callback: { [weak self] (response) in
                    self?.searchPresenter.present(with: response)
               })
               .run(on: DispatchQueue.global())
               .observe(on: .main)
               .execute()

    }

    func model(at index: Int) -> ACMWSearchRestultDataModel? {
       return results.enumerated()
                     .filter({ $0.offset == index })
                     .map({ ($0.element.name, $0.element.queryString ) })
                     .map(ACMWSearchRestultDataModel.init)
                     .first
    }
}
