//
//  ACMWFullForecastCell.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-18.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

class ACMWFullForecastCell: ACMWWeekDayForecastCell {
    private var humidityText: ACMWLabel!
    private var humidityValue: ACMWLabel!
    private var windText: ACMWLabel!
    private var windValue: ACMWLabel!
    private var feelsLikeText: ACMWLabel!
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override init(frame: CGRect) {
        let labelBuilder = ACMWLabelBuilder.getLabelBuilder(ACMWLabelBuilder.labelTypes.smallCapsLabels)
        labelBuilder.fontSize = 17
        self.humidityText = labelBuilder.buildLabel()
        self.windText = labelBuilder.buildLabel()
        self.windValue = labelBuilder.buildLabel()
        labelBuilder.textAllignment = NSTextAlignment.left
        self.feelsLikeText = labelBuilder.buildLabel()
        self.windText.text = NSLocalizedString("WIND", comment: String())
        self.windValue.text = NSLocalizedString("CALM", comment: String())
        self.humidityText.text = NSLocalizedString("HUMIDITY", comment: String())
        self.feelsLikeText.text = NSLocalizedString("FEELS LIKE", comment: String())

        let humidityBuilder = ACMWLabelBuilder.getLabelBuilder(ACMWLabelBuilder.labelTypes.percentValues)
        self.humidityValue = humidityBuilder.buildLabel()
        self.humidityValue.text = ""

        super.init(frame: frame)
        self.feelsLikeTemperature.textAlignment = NSTextAlignment.left

      //  appearanceProxy.setImage(forecastIcon, withType: ACMWWeatherIconTypeDayRainy)

    }

    override internal func configureAutoLayout() {
        self.contentView.layer.cornerRadius = frame.size.width * 0.05
        CellGradientDecorator.decorate(self)
        let temperatureBuilder = ACMWLabelBuilder.getLabelBuilder(ACMWLabelBuilder.labelTypes.temperature)
        temperatureBuilder.fontSize = 50
        self.temperatureLabel = temperatureBuilder.buildLabel()
        self.feelsLikeTemperature.textColor = UIColor.white

        self.contentView.addSubviews(self.forecastIcon, self.feelsLikeTemperature, self.temperatureLabel, self.humidityText, self.humidityValue, self.feelsLikeText, self.windValue, self.windText)
        NSLayoutConstraint.compressionPriority([forecastIcon], priority: UILayoutPriorityDefaultLow - 1)

        self.contentView.setTranslatesAutoresizingMaskInSubviews(false)
        NSLayoutConstraint.equalWitdh([self.forecastIcon], multiplier: 0.45)
        NSLayoutConstraint.aspectRatio(1, height: 0.8, views: self.forecastIcon)
        self.temperatureLabel.textAlignment = NSTextAlignment.left

        NSLayoutConstraint.top([self.forecastIcon, self.windText, self.windValue], option: .center)
        NSLayoutConstraint.allignVerticaly([self.temperatureLabel, self.feelsLikeText, self.humidityText, self.humidityValue], horizontalAllignmentOptions: .leading, boundToSuperView: false)
        NSLayoutConstraint.allignHorizontaly([self.windText, self.humidityText], verticalAllignmentOptions: .top, boundToSuperView: false)
        NSLayoutConstraint.allignHorizontaly([self.forecastIcon, self.temperatureLabel], verticalAllignmentOptions: .top, boundToSuperView: true)
        NSLayoutConstraint.changeConstraintRelation(referenceView: self.windText, relation: NSLayoutRelation.greaterThanOrEqual, attribute: NSLayoutAttribute.trailing)
        NSLayoutConstraint.changeConstraintRelation(referenceView: self.feelsLikeText, relation: NSLayoutRelation.greaterThanOrEqual, attribute: NSLayoutAttribute.bottom)
        NSLayoutConstraint.trailling([self.feelsLikeText, self.feelsLikeTemperature], option: .bottom )
        NSLayoutConstraint.updateConstraintConstant(referenceView: self.feelsLikeTemperature, value: 15, type: NSLayoutAttribute.trailing)
        NSLayoutConstraint.updateConstraintConstant(referenceView: self.feelsLikeText, value: 8, type: NSLayoutAttribute.trailing)
        NSLayoutConstraint.updateConstraintConstant(referenceView: self.forecastIcon, value: 15, type: NSLayoutAttribute.top)
        NSLayoutConstraint.updateConstraintConstant(referenceView: self.forecastIcon, value: 15, type: NSLayoutAttribute.leading)
        NSLayoutConstraint.hugging([self.feelsLikeTemperature], priority: UILayoutPriorityDefaultLow - 1)
    }

    override func configureCell(_ forecastData: ACMWCellData) {
        temperatureLabel.text = String(forecastData.temperature)
        self.humidityValue.text = String(forecastData.humidiy)
        self.feelsLikeTemperature.text = String(forecastData.feels_like)
       // appearanceProxy.label(dayLabel, withType: designType())
        self.dayLabel.text = forecastData.day
        self.forecastIcon.weatherType = forecastData.weatherIcon
    }

}
