//
//  ACMWMainForecastViewController.swift
//  MyWeatherApp
//
//  Created by Alex Hmelevski on 2017-06-28.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import Foundation

struct ForecastViewModel {
    let cellData: [IndexPath : ACMWCellData]
    let state: UIUpdateState
    let cityName: String
}

protocol ForecastOutput: class {
    func update(with model: ForecastViewModel)
}
