//
//  ACOperators.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-27.
//  Copyright © 2016 Alex Crow. All rights reserved.
//
import CoreGraphics
import Foundation

public protocol PrimitiveTypes {}
public protocol Literals {}

extension String: Literals {}
extension NSString: Literals {}
extension Int: PrimitiveTypes {}
extension Bool: PrimitiveTypes {}
extension Double: PrimitiveTypes {}
extension Float: PrimitiveTypes {}
extension CGFloat: PrimitiveTypes {}

precedencegroup MultiplicationPrecedence {
    associativity: right
    higherThan: AdditionPrecedence
}

infix operator <~ : MultiplicationPrecedence
public typealias ACDictionary = [String: Any]

public func <~ <T>( property: inout T?, anyObject: String) {
    var newValue = anyObject as? T
    mapToType(&newValue, anyObject: anyObject)
    property = newValue
}

public func <~ <T>( property: inout T, anyObject: String) {
    var newValue = anyObject as? T
    mapToType(&newValue, anyObject: anyObject)
    guard let val = newValue else {
        return
    }
    property = val
}

public func <~ <T>( property: inout T?, anyObject: T?) {
    property = anyObject
}
public func <~ <T>( property: inout T, anyObject: T) {
    property = anyObject
}

public func <~ <T>( property: inout T?, anyObject: Any?) {
    property = anyObject as? T
}

public func <~ <T>( property: inout T, anyObject: Any?) {
    guard let anObject = anyObject as? T  else {
       return
    }
    property = anObject
}

public func <~ <T: PrimitiveTypes>(property: inout T, anyObject: Any?) {
    if let anObject = anyObject as? T {
        property = anObject
    } else {
        var newValue: T?
        newValue <~ anyObject
        guard let obj = newValue else { return }
        property <~ obj
//        newValue.do(work: { property = $0 })
    }
}

public func <~ <T: PrimitiveTypes>( property: inout T?, anyObject: Any?) {
    var newValue = anyObject as? T
    mapToType(&newValue, anyObject: anyObject)
    property = newValue
}

public func <~ <T: CustomFromStringConvertable>( property: inout T?, anyObject: Any) {
    property = (anyObject as? String).flatMap(T.init)
}

public func <~ <T: CustomFromStringConvertable>( property: inout T?, anyObject: Any?) {
    guard let obj = anyObject else { return }
    property <~ obj
//    anyObject.do(work: { property <~ $0 })
}

public func <~ <T: ACMappableProtocol>( property: inout [T]?, anyObject: Any) {
    property = (anyObject as? [ACDictionary])?.map(T.init)
}

public func <~ <T: ACMappableProtocol>( property: inout [T]?, anyObject: Any?) {
    guard let obj = anyObject else { return }
    property <~ obj
//    anyObject.do(work: {(value) in
//        property <~ value
//    })
}

public func <~ <T: ACMappableProtocol>(property: inout T, anyObject: Any) {
    if let dict = anyObject as? ACDictionary {
        property = T(dict: dict)
    }
}

public func <~ <T: ACMappableProtocol>(property: inout T, anyObject: Any?) {
    property = (anyObject as? ACDictionary).map(T.init) ?? T(dict: [:])
}

public func <~ <T: ACMappableProtocol>(property: inout T?, anyObject: Any) {
    property = (anyObject as? ACDictionary).map(T.init)
}

public func <~ <T: ACMappableProtocol>(property: inout T?, anyObject: Any?) {
    guard let obj = anyObject else { return }
    property <~ obj
//    anyObject.do(work: {(value) in
//        property <~ value
//    })
}

public func <~ <T: PrimitiveTypes>( property: inout [T]?, anyObject: Any?) {
    property = [T]()

    if let obj = anyObject as? [T] {
        property = obj
    } else if let anyArr = anyObject as? [AnyObject] {

        property = anyArr.map(mapObj)
                         .flatMap({ $0 })

    }
}

public func <~ <T>(property: inout [T], anyObject: Any?) {
    if let obj = anyObject as? [String] {
        property <~ obj
    } else if let obj = anyObject as? [T] {
        property = obj
    }
}

public func <~ (property: inout Date?, anyObject: Any) -> Date? {
    if let object = anyObject as? Date {
        property = object
    } else if let string = anyObject as? String {
        if let timeStamp = TimeInterval(string) {
            let formatter = DateFormatter()
            formatter.dateStyle = DateFormatter.Style.full
            property = Date(timeIntervalSince1970: timeStamp)
        }
    }
    return property
}

public func <~ <T>( property: inout [T], object: [String]) {

    property = object.map { (element) -> T? in
        var nElement: T?
        nElement <~ element
        return nElement
    }
    .flatMap({ $0 })
}

private func mapObj<T: PrimitiveTypes>(obj: AnyObject) -> T? {
    var nElement: T?
    nElement <~ obj
    return nElement
}

private func mapToType <T>( _ value: inout T?, anyObject: Any?) {
    if let strObject = anyObject as? NSString {
        let type = T.self
        switch type {
            case is Int.Type: value = strObject.integerValue as? T
            case is Double.Type: value = strObject.doubleValue as? T
            case is Float.Type: value = strObject.floatValue as? T
            case is CGFloat.Type: value = (CGFloat(atof(strObject.utf8String))) as? T
            case is Bool.Type: value = strObject.boolValue as? T
        default: break
        }
    }
}
