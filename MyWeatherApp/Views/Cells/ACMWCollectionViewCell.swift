//
//  ACMWCollectionViewCell.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-19.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

public class ACMWCollectionViewCell: UICollectionViewCell {
    public func configureCell(_ forecastData: ACMWCellData) {
        fatalError("Sublcass \(self.classForCoder) must override \(#function) at \(#column)")
    }

    public func configureCell(_ forecastData: [ACMWCellData]) {
        fatalError("Sublcass \(self.classForCoder) must override \(#function) at \(#column)")
    }
}
