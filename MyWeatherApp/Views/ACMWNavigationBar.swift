//
//  ACMWNavigationBar.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-05.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import UIKit
import CoreImage

enum NavigationButtonTypes {
    case settings
    case share
    case add
}

class ACMWNavigationBar: UICollectionReusableView {

    var title = UILabel()
    var subTitle = UILabel()
    let shareButton = UIButton(type: .system)
    let addButton = UIButton(type: .system)
    let settingsButton = UIButton(type: .system)
    private var blurSnap = UIImageView()
    private var titleContainer = UIView()
    private let gradientLayer = ACMWGradientViewBase(frame: .zero, color: .black, endAlpha: 0)
    let proxy = ACMWDesignProxy()

    var backgroundImage: UIImage? {
        get {
            return  self.blurSnap.image
        }
        set {
            if let image = newValue,
              let blured = ACMWDesignProxy().bluredBackgroundImage(for: image) {
              self.blurSnap.image = ACMWImageProcessing.cropImage(blured, cropRect: self.bounds)
            }
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addGradient()
        self.addCustomTitleView()
        self.addButtons()
        self.configureSnapViews()
    }

    private func addGradient() {
        self.gradientLayer.cornerRadius = 0
        self.addSubviews(self.gradientLayer)

        NSLayoutConstraint.allignHorizontaly([self.gradientLayer], verticalAllignmentOptions: .center, boundToSuperView: true)
        NSLayoutConstraint.top([self.gradientLayer], option: .center)
        NSLayoutConstraint.equalHeightToSuperView([self.gradientLayer], multiplier: 1)
    }

    private func configureSnapViews() {
        self.insertSubview(self.blurSnap, aboveSubview: self.gradientLayer)
        NSLayoutConstraint.allignHorizontaly([self.blurSnap], verticalAllignmentOptions: .center, boundToSuperView: true)
        NSLayoutConstraint.allignVerticaly([self.blurSnap], horizontalAllignmentOptions: .center, boundToSuperView: true)
        self.blurSnap.alpha = 1

        self.blurSnap.contentMode = UIViewContentMode.scaleToFill
        self.blurSnap.backgroundColor  = .gray
        self.backgroundColor = .clear
    }

    private func addCustomTitleView() {
        self.addSubview(self.titleContainer)
        self.titleContainer.addSubviews(self.title, self.subTitle)
        NSLayoutConstraint.centerHorizontallyInSuperview(self.title)
        NSLayoutConstraint.centerVerticallyInSuperview(self.title, attribute: NSLayoutAttribute.lastBaseline)
        NSLayoutConstraint.allignVerticaly([self.title, self.subTitle], horizontalAllignmentOptions: .center, boundToSuperView: false)
        NSLayoutConstraint.changeConstraintRelation(referenceView: self.subTitle, relation: NSLayoutRelation.greaterThanOrEqual, attribute: NSLayoutAttribute.bottom)
        NSLayoutConstraint.top([self.titleContainer], option: .center)
        NSLayoutConstraint.centerHorizontallyInSuperview(self.titleContainer)
        NSLayoutConstraint.equalHeightToSuperView([self.titleContainer])
        self.proxy.label(self.title, with: ACMWNavigationBarT)
        self.proxy.subTitleLabel(self.subTitle, with: ACMWNavigationBarT)
    }

    private func addButtons() {
        let buttons = [self.settingsButton, self.addButton, self.shareButton]
        self.addSubviews(self.settingsButton, self.addButton, self.shareButton)
        NSLayoutConstraint.equalHeightToSuperView(buttons, multiplier: 0.4)
        NSLayoutConstraint.centerVerticallyInSuperview(self.shareButton, attribute: NSLayoutAttribute.centerY)
        NSLayoutConstraint.centerVerticallyInSuperview(self.settingsButton, attribute: NSLayoutAttribute.centerY)
        NSLayoutConstraint.aspectRatio(1, height: 1, views: self.settingsButton, self.addButton, self.shareButton)
        NSLayoutConstraint.allignHorizontaly([settingsButton, titleContainer, addButton, shareButton],
                                             verticalAllignmentOptions: .center,
                                             boundToSuperView: true)
        NSLayoutConstraint.changeConstraintRelation(referenceView: settingsButton,
                                                    relation: .greaterThanOrEqual,
                                                    attribute: .trailing)
        NSLayoutConstraint.updateConstraintConstant(referenceView: self.shareButton, value: 16, type: NSLayoutAttribute.trailing)
        NSLayoutConstraint.updateConstraintConstant(referenceView: self.addButton, value: 16, type: NSLayoutAttribute.trailing)
        NSLayoutConstraint.updateConstraintConstant(referenceView: self.settingsButton, value: 16, type: NSLayoutAttribute.leading)
        //TODO: Take images from design containter. Any color settings from design proxy
        self.addButton.setImage(UIImage(named: "addButton"), for: [])
        self.shareButton.setImage(UIImage(named: "shareButton"), for: [])
        self.settingsButton.setImage(UIImage(named: "settings"), for: [])
    }

    func addTargetFor(buttonType type: NavigationButtonTypes, target: AnyObject?, action: Selector) {
        var button: UIButton
        switch type {
            case .add: button = self.addButton
            case .settings: button = self.settingsButton
            case .share: button = self.shareButton
        }
        button.addTarget(target, action: action, for: .touchUpInside)
    }

    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        if let lAttr = layoutAttributes as? ACMWCollectionViewLayoutAttributes {
            let progress = lAttr.contentOffset.y / self.frame.height
            self.blurSnap.alpha = CGFloat(progress)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}
