//
//  ACMWMainViewController.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-06.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import UIKit

class ACMWMainViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    private let sDataControllerID = "mainDataController"
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self

        let nManager = AHWundergroundProxy(queryTypes: [.country(name: "Canada"), .city(name: "Montreal")])
        let dataSource = ACMWCollectionViewDataSource()
        let cityController = ACMWCityForecastViewController(source: dataSource)

        self.setViewControllers([cityController], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        return nil
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        return nil
    }

}
