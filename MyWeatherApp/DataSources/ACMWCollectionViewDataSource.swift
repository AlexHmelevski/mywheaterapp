//
//  ACMWCollectionViewDataSource.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-18.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation
import AHFuture
import ALEither

protocol ICollectionViewDataSource {
    func update(with viewModel: ForecastViewModel)
}

public class ACMWCollectionViewDataSource: ICollectionViewDataSource {
	private var collectionView: ACMWCollectionView!
	private var numberOfRowsInForecast: Int = 5
    fileprivate var viewModel = ForecastViewModel(cellData: [:], state: .loading, cityName: "")
	private weak var footer = ACMWCollectionForecastFooterView()

	public func accossiateWith(_ collectionView: ACMWCollectionView) {
		self.collectionView = collectionView
	}

	@available(iOS 6.0, *)
	public func numberOfItemsIn(_ section: Int) -> Int {
		switch section {
		case 0: return 1
		case 1: return 3
		case 2: return 1
		case 3: return self.numberOfRowsInForecast
		default: return 0
		}

	}

	// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
 @available(iOS 6.0, *)
	func cellForItemAt(_ indexPath: IndexPath) -> UICollectionViewCell {
        let id: String
        let configureFunction = configure(with: viewModel, for: indexPath)
        switch indexPath.section {
            case 0: id = viewModel.cellData.isEmpty ? ACMWForecastCellType.BaseCell.description : ACMWForecastCellType.FullForecast.description
            case 1: id = ACMWForecastCellType.ShortForecast.description
            case 2: id = ACMWHourlyForecastCollectionViewCell.kind
            case 3: id = ACMWForecastCellType.WeekDay.description
        default: id = ACMWForecastCellType.BaseCell.description
        }
		let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: id, for: indexPath)

		configureFunction(cell)

		return cell

	}

    private func configure(with model: ForecastViewModel, for index: IndexPath) -> (ACMWCollectionViewCell) -> Void {
        return { (cell) in
            switch index.section {
            case 0, 1, 3: self.viewModel.cellData[index].do(work: cell.configureCell)
            case 2:  cell.configureCell(self.viewModel.cellData.filter({ $0.key.section == index.section })
                                                          .map({ $0.value }))
            default: break
            }

        }

    }

	@available(iOS 6.0, *)

	public func numberOfSections() -> Int {
        return viewModel.cellData.isEmpty ? 1 : 4
	}

	// The view that is returned must be retrieved from a call to -dequeueReusableSupplementaryViewOfKind:withReuseIdentifier:forIndexPath:
	@available(iOS 6.0, *)
	public func collectionView(_ collectionView: UICollectionView,
         viewForSupplementaryElementOfKind kind: String,
                                   at indexPath: IndexPath) -> UICollectionReusableView {

		var reusableView = UICollectionReusableView()
		if kind == UICollectionElementKindSectionHeader {
			reusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                   withReuseIdentifier: ACMWCollectionForecastHeaderView.kind,
                                                                   for: indexPath)
		}

		if kind == UICollectionElementKindSectionFooter {
			if let unFooter = self.footer {
				reusableView = unFooter
			} else {
                guard let footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                        withReuseIdentifier: ACMWCollectionForecastFooterView.kind,
                                                        for: indexPath) as? ACMWCollectionForecastFooterView else {
                    fatalError("Unexpected Footer Type")
                }
				footer.addButtonSelectedCallBack(selectedButtonFunction: self.updateRowCountWith)
				self.footer = footer
				reusableView = footer
			}
		}

		if kind == ACMWNavigationBar.kind {
			reusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: ACMWNavigationBar.kind, for: indexPath)
            if let navigation = reusableView as? ACMWNavigationBar {
                navigation.title.text = viewModel.cityName
            }
		}

		return reusableView
	}

	func updateRowCountWith(_ selectedItem: Int) {
		precondition(!(selectedItem > 1 && selectedItem < 0), "selected item should be 0 or 1")
		self.collectionView.performBatchUpdates({ () -> Void in
			if selectedItem == 0 {
				self.numberOfRowsInForecast = 5
				self.collectionView.deleteItems(at: self.arrayOfIndexes)
			} else {
				self.numberOfRowsInForecast = 10
                self.collectionView.insertItems(at: self.arrayOfIndexes)
			}

		}) { (_) -> Void in
			self.collectionView.reloadData()
		}

	}

	private var arrayOfIndexes: [IndexPath] {

        var array = [IndexPath] ()
        for idx in 5..<10 {
            array.append(IndexPath(row: idx, section: 3))
        }
        return array

	}

    func update(with viewModel: ForecastViewModel) {
        self.viewModel = viewModel
        collectionView.reloadData()
    }
}
