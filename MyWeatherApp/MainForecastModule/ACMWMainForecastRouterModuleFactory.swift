//
//  ACMWMainForecastRouterModuleFactory.swift
//  MyWeatherApp
//
//  Created by Alex Hmelevski on 2017-07-12.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import Foundation

enum ForecastModuleType {
    case empty
    case withQuery(types: [AHWundegroundQueryType])
}

protocol MainForecastModuleFactory {
    func module(for type: ForecastModuleType, withDelegate delegate: ForecastModuleDelegate?) -> IRouterModule

}

final class ACMWMainForecastModuleFactory: MainForecastModuleFactory {
    func module(for type: ForecastModuleType, withDelegate delegate: ForecastModuleDelegate?) -> IRouterModule {
        let rModule: IRouterModule
        switch type {
            case .empty: rModule = module(withTypes: [], andDelegate: delegate)
            case let .withQuery(types): rModule = module(withTypes: types, andDelegate: delegate)
        }
        return rModule
    }

    private func module(withTypes types: [AHWundegroundQueryType], andDelegate delegate: ForecastModuleDelegate?) -> IRouterModule {
        return ACMWMainForecastRouterModule(types: types, delegate: delegate)
    }
}
