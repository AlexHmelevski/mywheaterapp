//
//  ACMWUIFontExtTests.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-10.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import XCTest

class ACMWUIFontExtTests: XCTestCase {
    func testGetThickerFont() {
        let fonts = UIFont.familyNames
        for family in fonts {
            let names = UIFont.fontNames(forFamilyName: family)
            for fName in names {
                let font = UIFont(name: fName, size: 15)
                let thickerFont = UIFont.getThickerFont(font)
                XCTAssertNotNil(thickerFont, "Couldn't find thiker font for font with name \(fName)")
                XCTAssertGreaterThanOrEqual(thickerFont!.fontWeight(), font!.fontWeight())
            }
        }

        let font = UIFont(name: "", size: 1)
        let thickerFont = UIFont.getThickerFont(font)
        XCTAssertNil(thickerFont, "Expected nil, as long as the font is Nil")
    }
}
