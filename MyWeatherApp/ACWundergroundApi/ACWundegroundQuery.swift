//
//  ACWundegroundQuery.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-25.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

enum AHWundegroundQueryType {
    case city(name: String)
    case zip(code: String)
    case country(name: String)
    case zmw(value: String)

    var path: String {
        switch self {
            case let .city(name): return "/\(name)"
            case let .country(name): return "/\(name)"
            case let .zip(code): return "/\(code)"
            case let .zmw(value): return "/zmw:\(value)"
        }
    }
}

public struct AHWundegroundQuery {
    var path: String
    init(with types: [AHWundegroundQueryType], and format: ACWundegroundFormat = .JSON) {
        path = "/q" + types.map({ $0.path }).joined() + format.rawValue
    }
}
