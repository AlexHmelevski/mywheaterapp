//
//  ACMWCollectionDataSourceMock.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-15.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import UIKit
import Foundation
import ACWundergroundAPI

class ACMWCollectionDataSourceMock: ACMWCollectionDataSource {

    override func updateDataSource() {
        print(self.dataModel)
    }
}
