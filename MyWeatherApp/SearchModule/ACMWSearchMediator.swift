//
//  ACMWSearchInput.swift
//  MyWeatherApp
//
//  Created by Alex Hmelevski on 2017-06-23.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import Foundation

protocol ISearchInput: class {
    func didType(text: String)
    func didSelectResult(at index: Int)
    func didClose()
}

protocol ACMWSearchRouterModuleDelegate: class {
    func didFinish(with result: ACMWSearchRestultDataModel?)
}

final class ACMWSearchMediator {
    let interactor: ISearchInteractor
    fileprivate weak var delegate: ACMWSearchRouterModuleDelegate?

    init(interactor: ISearchInteractor,
         delegate: ACMWSearchRouterModuleDelegate?) {
        self.interactor = interactor
        self.delegate = delegate
    }
}

extension ACMWSearchMediator: ISearchInput {

    func didType(text: String) {
        interactor.search(for: text)
    }

    func didClose() {
        delegate?.didFinish(with: nil)
    }

    func didSelectResult(at index: Int) {
        interactor.model(at: index).do { (model) in
            self.delegate?.didFinish(with: model)
        }
    }
}
