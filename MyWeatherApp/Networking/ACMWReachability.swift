//
//  ACMWReachability.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-06.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation
import SystemConfiguration

// @TODO: don't expose SCNetworkReachabilityFlags, convert to own proper type
class ACMWReachability {
	let domain: String
	var prCallBack: SCNetworkReachabilityCallBack?
	var usersCallBack: ((SCNetworkReachabilityFlags) -> Void)?

	var isReachable: Bool {

        var flags = SCNetworkReachabilityFlags()
        let reachability = SCNetworkReachabilityCreateWithName(nil, self.domain)
        if let uReachability = reachability {
            SCNetworkReachabilityGetFlags(uReachability, &flags)
        }
        return flags.contains([.reachable])

	}

	func checkStatusChange(_ callback: ((SCNetworkReachabilityFlags) -> Void)?) {
		let reachability = SCNetworkReachabilityCreateWithName(nil, self.domain)
		var context = SCNetworkReachabilityContext()
		self.usersCallBack = callback
        let a = unsafeBitCast(self, to: UnsafeMutablePointer<ACMWReachability>.self)
		context.info = UnsafeMutableRawPointer(a)

		self.prCallBack = { (Reachability, ReachabilityFlags, context) -> Void in
			let mySelf = Unmanaged<ACMWReachability>.fromOpaque(UnsafeMutableRawPointer(context!)).takeUnretainedValue()
			mySelf.usersCallBack?(ReachabilityFlags)
		}

		SCNetworkReachabilitySetCallback(reachability!, self.prCallBack, &context)

		SCNetworkReachabilitySetDispatchQueue(reachability!, DispatchQueue.main)
	}

	init(domain: String) {
		self.domain = domain
	}
}
