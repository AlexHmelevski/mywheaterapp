//
//  GradientDecorator.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-01-04.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

class CellGradientDecorator: CellDecoration {
	class func decorate(_ cell: UICollectionViewCell) {
		GradientDecorator.decorate(cell.contentView, direction: ACMWGradientViewBase.gradientDirections.directionDown)
	}

	class func changeGradientDirection(_ cell: UICollectionViewCell, devOrientation: UIDeviceOrientation) {
		var nDirection: ACMWGradientViewBase.gradientDirections
		switch devOrientation {
            case .landscapeLeft: nDirection = ACMWGradientViewBase.gradientDirections.directionLeft
            case .landscapeRight: nDirection = ACMWGradientViewBase.gradientDirections.directionRight
            case .portrait: nDirection = ACMWGradientViewBase.gradientDirections.directionDown
            case .portraitUpsideDown: nDirection = ACMWGradientViewBase.gradientDirections.directionUp
		default: nDirection = ACMWGradientViewBase.gradientDirections.directionDown
		}
		GradientDecorator.decorate(cell.contentView, direction: nDirection)
	}
}

private protocol ViewDecoration {
	static func decorate(_ view: UIView, direction: ACMWGradientViewBase.gradientDirections?)
}

private class GradientDecorator: ViewDecoration {
	private static let kGratientTag = 95_678
	class func decorate(_ view: UIView, direction: ACMWGradientViewBase.gradientDirections?) {
		if !view.containsObjectOfClass(ACMWGradientViewBase.classForCoder()) {
			let gradient = ACMWGradientViewBase()
			if let dir = direction {
				gradient.direction = dir
			}
			gradient.translatesAutoresizingMaskIntoConstraints = false
			view.insertSubview(gradient, at: 0)
			view .addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|",
                                                                         options: .alignAllCenterY,
                                                                         metrics: nil,
                                                                           views: ["view": gradient]))
			view .addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|",
                                                                         options: .alignAllCenterX,
                                                                         metrics: nil,
                                                                           views: ["view": gradient]))
			gradient.cornerRadius = Float(view.layer.cornerRadius)
			gradient.tag = kGratientTag
		} else {
			if let gradient = view.viewWithTag(kGratientTag) as? ACMWGradientViewBase,
				let nDirection = direction {
					gradient.direction = nDirection
			}
		}

	}
}

private protocol CellDecoration {
	static func decorate(_ cell: UICollectionViewCell)
}
