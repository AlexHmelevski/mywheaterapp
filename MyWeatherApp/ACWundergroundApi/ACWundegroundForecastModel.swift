//
//  ACWundegroundForecastModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-29.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundegroundForecastModel: ACMappableProtocol {
    private(set) var simpleforecast: ACWundergroundForecastArrayModel?
    private(set) var txt_forecast: ACWundergroundForecastArrayModel?
    init(dict: ACDictionary) {
        self.simpleforecast <~ dict["simpleforecast"]
        self.txt_forecast <~ dict["txt_forecast"]
    }
}
