//
//  ACWundegroundFeaturesModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-28.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundegroundFeaturesModel: ACMappableProtocol {
    private(set) var alerts: Bool?
    private(set) var almanac: Bool?
    private(set) var astronomy: Bool?
    private(set) var conditions: Bool?
    private(set) var currenthurricane: Bool?
    private(set) var forecast: Bool?
    private(set) var forecast10day: Bool?
    private(set) var geolookup: Bool?
    private(set) var history: Bool?
    private(set) var hourly: Bool?
    private(set) var hourly10day: Bool?
    private(set) var planner: Bool?
    private(set) var rawtide: Bool?
    private(set) var tide: Bool?
    private(set) var webcams: Bool?
    private(set) var yesterday: Bool?

    init(dict: ACDictionary) {
        self.alerts <~ dict["alerts"]
        self.almanac <~ dict["almanac"]
        self.astronomy <~ dict["astronomy"]
        self.conditions <~ dict["conditions"]
        self.currenthurricane <~ dict["currenthurricane"]
        self.forecast <~ dict["forecast"]
        self.forecast10day <~ dict["forecast10day"]
        self.geolookup <~ dict["geolookup"]
        self.history <~ dict["history"]
        self.hourly <~ dict["hourly"]
        self.hourly10day <~ dict["hourly10day"]
        self.planner <~ dict["planner"]
        self.rawtide <~ dict["rawtide"]
        self.tide <~ dict["tide"]
        self.webcams <~ dict["webcams"]
        self.yesterday <~ dict["yesterday"]
    }
}
