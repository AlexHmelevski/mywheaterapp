//
//  ACMWAstronomyTest.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-22.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import XCTest

class ACMWAstronomyTest: ACMWModelTest {

    override var modelName: String {
        return "AstronomySource"
    }

    func testParse() {
        let model = ACWundergroundModel(dict: self.dataDictionary!)
        XCTAssertEqual(model.moon_phase!.percentIlluminated, 81)
        XCTAssertEqual(model.moon_phase!.ageOfMoon, 10)
        XCTAssertEqual(model.moon_phase!.current_time.hour, 9)
        XCTAssertEqual(model.moon_phase!.current_time.min, 56)
        XCTAssertEqual(model.moon_phase!.sunrise.hour, 7)
        XCTAssertEqual(model.moon_phase!.sunrise.min, 01)
        XCTAssertEqual(model.moon_phase!.sunset.hour, 16)
        XCTAssertEqual(model.moon_phase!.sunset.min, 56)
    }
}
