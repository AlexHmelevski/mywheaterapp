//
//  ACMWConditionsTest.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-22.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import XCTest

class ACMWConditionsTest: ACMWModelTest {
    override var modelName: String {
        return "ConditionsSource"
    }

    func testParse() {
        let conditions = ACWundergroundModel(dict: self.dataDictionary!)
        XCTAssertEqual(conditions.conditions!.station_id, "KCASANFR58")
        XCTAssertEqual(conditions.conditions!.observation_time, "Last Updated on June 27, 5:27 PM PDT")
        XCTAssertEqual(conditions.conditions!.observation_time_rfc822, "Wed, 27 Jun 2012 17:27:13 -0700")
         XCTAssertEqual(conditions.conditions!.local_time_rfc822, "Wed, 27 Jun 2012 17:27:14 -0700")
        XCTAssertEqual(conditions.conditions!.local_epoch, 1_340_843_234)
        XCTAssertEqual(conditions.conditions!.local_tz_long, "America/Los_Angeles")
        XCTAssertEqual(conditions.conditions!.weather!.Day, ACMWForecastType.PartlyCloudy)
        XCTAssertEqual(conditions.conditions!.weather!.Night, ACMWForecastType.Night_PartlyCloudy)
        XCTAssertEqual(conditions.conditions!.icon!.Day, ACMWForecastType.PartlyCloudy)
        XCTAssertEqual(conditions.conditions!.icon!.Night, ACMWForecastType.Night_PartlyCloudy)

        XCTAssertEqual(conditions.conditions!.temp, ACWundergroundMetric<Double>(english: 66.3, metric:  19.1))
        XCTAssertEqual(conditions.conditions!.wind_gust, ACWundergroundMetric<Double>(english: "28.0", metric:  "45.1"))
        XCTAssertEqual(conditions.conditions!.dewpoint, ACWundergroundMetric<Double>(english: 54.0, metric:  12.0))

        XCTAssertEqual(conditions.conditions!.windchill, ACWundergroundMetric<Double>(english: 54.0, metric:  12.0))
        XCTAssertEqual(conditions.conditions!.feelslike, ACWundergroundMetric<Double>(english: "66.3", metric:  19.1))
        XCTAssertEqual(conditions.conditions!.visibility, ACWundergroundMetric<Double>(english: "10.0", metric: 16.1))
         XCTAssertEqual(conditions.conditions!.precip_today, ACWundergroundMetric<Double>(english: 9.0, metric: 18.0))
        XCTAssertEqual(conditions.conditions!.precip_1hr, ACWundergroundMetric<Double>(english: 9.0, metric: 18.0))

    }

}
