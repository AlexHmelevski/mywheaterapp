//
//  ACWundegroundHourlyModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-30.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundergroundHourlyModel: ACMappableProtocol {
    private(set) var FCTTIME = ACWundegroundTimeModel(dict: ["": "" as AnyObject])
    private(set) var condition = String()
    private(set) var dewpoint = ACWundergroundValue(dict: ["C": 0 as AnyObject, "F": 0 as AnyObject])
    private(set) var fctcode: Int = 0
    private(set) var feelslike = ACWundergroundValue(dict: ["C": 0 as AnyObject, "F": 0 as AnyObject])
    private(set) var heatindex = ACWundergroundValue(dict: ["english": 0 as AnyObject, "metric": 0 as AnyObject])
    private(set) var humidity: Int = 0
    private(set) var icon = ACMWForecastWeatherType("")
    private(set) var icon_url = String()
    private(set) var mslp = ACWundergroundValue(dict: ["english": 0 as AnyObject, "metric": 0 as AnyObject])
    private(set) var pop: Int = 0
    private(set) var qpf = ACWundergroundValue(dict: ["english": 0 as AnyObject, "metric": 0 as AnyObject])
    private(set) var sky: Int = 0
    private(set) var snow = ACWundergroundValue(dict: ["english": 0 as AnyObject, "metric": 0 as AnyObject])
    private(set) var temp = ACWundergroundValue(dict: ["C": 0 as AnyObject, "F": 0 as AnyObject])
    private(set) var uvi: Int = 0
    private(set) var wdir = ACWundegroundMovementModel(dict: ["": "" as AnyObject])
    private(set) var windchill = ACWundergroundValue(dict: ["english": 0 as AnyObject, "metric": 0 as AnyObject])
    private(set) var wspd = ACWundergroundValue(dict: ["english": 0 as AnyObject, "metric": 0 as AnyObject])
    private(set) var wx = ACMWForecastWeatherType("")

    init(dict: ACDictionary) {
        self.FCTTIME <~ dict["FCTTIME"]
        self.condition <~ dict["condition"]
        self.dewpoint <~ dict["dewpoint"]
        self.fctcode <~ dict["fctcode"]
        self.feelslike <~ dict["feelslike"]
        self.heatindex <~ dict["feelslike"]
        self.humidity <~ dict["humidity"]
        self.icon <~ dict["icon"]
        self.icon_url <~ dict["icon_url"]
        self.mslp <~ dict["mslp"]
        self.pop <~ dict["pop"]
        self.qpf <~ dict["qpf"]
        self.sky <~ dict["sky"]
        self.snow <~ dict["snow"]
        self.temp <~ dict["temp"]
        self.uvi <~ dict["uvi"]
        self.wdir <~ dict["wdir"]
        self.windchill <~ dict["windchill"]
        self.wspd <~ dict["wspd"]
        self.wx <~ dict["wx"]

    }
}
