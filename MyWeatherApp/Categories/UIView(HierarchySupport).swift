//
//  UIView(HierarchySupport).swift
//
//
//  Created by Alex Crow on 15-12-24.
//  Copyright © 2015 Alex Crow. All rights reserved.
//
import CoreImage
import Foundation

extension UIView {
	// Return an array of all superviews »

	func superviews() -> [UIView] {
		var array: [UIView] = []
		var superview = self.superview
		repeat {
			if let view = superview {
				array += [view]
				superview = view.superview
			} else {
				break
			}

		} while true
		return array
	}

	// Return the nearest common ancestor between self and another view »
	func nearestCommonAncestor(forView view: UIView) -> UIView? {

		if self == view { return self }
		if self.isAncestor(ofView: view) { return self }
		if view.isAncestor(ofView: self) { return view }
		let ancestors = self.superviews()
		for aView in view.superviews() {
			if ancestors.contains(aView) { return aView }
		}
		return nil
	}

	private func isAncestor(ofView view: UIView) -> Bool {
		return view.superviews().contains(self)
	}

}
