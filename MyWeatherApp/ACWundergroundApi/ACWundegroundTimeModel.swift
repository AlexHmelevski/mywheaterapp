//
//  ACWundegroundHurricaneTimeModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-29.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

public struct ACWundegroundTimeModel: ACMappableProtocol {
    private(set) public var UTCDATE = String()
    private(set) public var age = String()
    private(set) public var ampm = String()
    private(set) public var civil = String()
    private(set) public var epoch: Int = 0
    private(set) public var hour: Int = 0
    private(set) public var hour_padded = String()
    private(set) public var isdst: Int = 0
    private(set) public var mday: Int = 0
    private(set) public var mday_padded = String()
    private(set) public var min: Int = 0
    private(set) public var mon: Int = 0

    private(set) public var mon_abbrev = String()
    private(set) public var mon_padded = String()
    private(set) public var month_name = String()
    private(set) public var month_name_abbrev = String()
    private(set) public var pretty = String()
    private(set) public var sec: Int = 0

    private(set) public var tz_long = String()
    private(set) public var tz_short = String()

    private(set) public var weekday_name = String()
    private(set) public var weekday_name_abbrev = String()
    private(set) public var weekday_name_night = String()
    private(set) public var weekday_name_night_unlang = String()
    private(set) public var weekday_name_unlang = String()
    private(set) public var yday: Int = 0
    private(set) public var year: Int = 0

    public init(dict: ACDictionary) {
        self.UTCDATE <~ dict["UTCDATE"]
        self.age <~ dict["age"]
        self.ampm <~ dict["ampm"]
        self.civil <~ dict["civil"]
        self.epoch <~ dict["epoch"]
        self.hour <~ (dict["hour"] ?? dict["hour"])
        self.hour_padded <~ dict["hour_padded"]
        self.isdst <~ dict["isdst"]
        self.mday <~ (dict["mday"] ?? dict["day"])
        self.mday_padded <~ dict["mday_padded"]
        self.min <~ (dict["min"] ?? dict["minute"])
        self.mon <~ (dict["mon"] ??  dict["monthname"])
        self.mon_abbrev <~ (dict["mon_abbrev"] ?? dict["monthname_short"])
        self.mon_padded <~ dict["mon_padded"]
        self.month_name <~ (dict["month_name"] ?? dict["month_name"])
        self.pretty <~ dict["pretty"]
        self.sec <~ dict["sec"]
        self.tz_long <~ (dict["tz_long"] ?? dict["tz"])
        self.tz_short <~ (dict["tz_short"] ?? dict["tzname"])
        self.weekday_name <~ (dict["weekday_name"] ?? dict["weekday"])
        self.weekday_name_abbrev <~ (dict["weekday_name_abbrev"] ?? dict["weekday_short"])
        self.weekday_name_night <~ dict["weekday_name_night"]
        self.weekday_name_night_unlang <~ dict["weekday_name_night_unlang"]
        self.weekday_name_unlang <~ dict["weekday_name_unlang"]
        self.yday <~ dict["yday"]
        self.year <~ dict["year"]

    }
}
