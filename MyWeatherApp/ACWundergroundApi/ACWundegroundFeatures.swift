//
//  ACWundegroundFeatures.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-25.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

enum AHWundegroundFeatureType {
    case alerts
    case almanac
    case astronomy
    case conditions
    case currenthurricane
    case forecast
    case forecast10day
    case geolookup
    case history
    case hourly
    case hourly10day
    case planner(start: String, end: String)
    case rawtide
    case tide
    case webcams
    case yesterday

    var path: String {
        switch self {
        case        .alerts                 : return "/alerts"
        case        .almanac                : return "/almanac"
        case        .astronomy              : return "/astronomy"
        case        .conditions             : return "/conditions"
        case        .currenthurricane       : return "/currenthurricane"
        case        .forecast               : return "/forecast"
        case        .forecast10day          : return "/forecast10day"
        case        .geolookup              : return "/geolookup"
        case        .history                : return "/history"
        case        .hourly                 : return "/hourly"
        case        .hourly10day            : return "/hourly10day"
        case let    .planner(start, end)     : return "/planner_\(start)\(end)"
        case        .rawtide                : return "/rawtide"
        case        .tide                   : return "/tide"
        case        .webcams                : return "/webcams"
        case        .yesterday              : return "/yesterday"
        }
    }
}
