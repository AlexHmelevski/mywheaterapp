//
//  ACMWCellDataAdapter.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-18.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

enum ForecastCellType {
    case currentDay
    case weekInfo
    case hourly
}

class ACMWCellDataAdapter {
	private var dataModel: ACWundergroundModel?
	private var metricSystemType: ACWundergroundMetricType?
    let converters: [ForecastCellType : ACMWDataConverter]

    init(converters: [ForecastCellType : ACMWDataConverter] = [.currentDay: ACMWCurrentDayDataConverter(),
                                                              .weekInfo: ACMWWeekInfoDataConverter(),
                                                              .hourly: ACMWDayHourlyConverter()]) {
        self.converters = converters
	}

    func set(model: ACWundergroundModel) {
        self.dataModel = model
    }

    func set(metric: ACWundergroundMetricType) {
        self.metricSystemType = metric
    }

    func cellData(forType type: ForecastCellType) -> [ACMWCellData] {
        guard let model = dataModel, let metric = metricSystemType else { return [] }
        return converters[type]?.convert(model, metricSytem: metric) ?? []
    }
}
