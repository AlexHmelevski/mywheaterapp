//
//  ACMWCollectionForecastHeaderViewSwift.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 15-11-20.
//  Copyright © 2015 Alex Crow. All rights reserved.
//

import Foundation

class ACMWCollectionForecastHeaderView: UICollectionReusableView {

    private struct ACMWCollectionForecastHeaderViewStrings {
        static let  ACMWForecastTitle = "FORECAST"
    }

    private var titleLabel: UILabel!
    private var separator: ACMWHorizontalPlainSeparator!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        let labelYPosition = frame.size.height / 3.667
        let proxy = ACMWDesignProxy()
        let labelFrame = proxy.frame(toFit: NSLocalizedString(ACMWCollectionForecastHeaderViewStrings.ACMWForecastTitle, comment: ""), with: ACMWForecastHeader)
        titleLabel = UILabel(frame: CGRect(x: 0, y: labelYPosition, width: labelFrame.size.width, height: labelFrame.size.height))
        proxy.label(titleLabel, with: ACMWForecastHeader)

        let attrString = proxy.smallCapsString(from: NSLocalizedString(ACMWCollectionForecastHeaderViewStrings.ACMWForecastTitle, comment: ""), with: ACMWForecastHeader)
        titleLabel.attributedText = attrString
        insertSubview(titleLabel, at: 0)

        separator = ACMWHorizontalPlainSeparator(frame: CGRect(x: 0, y: frame.size.height, width: frame.size.width, height: ACMWHorizontalPlainSeparator.lineWidthForClass()))
        insertSubview(separator, at: 0)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        separator.frame = CGRect(x: 0, y: frame.size.height, width: frame.size.width, height: ACMWHorizontalPlainSeparator.lineWidthForClass())
    }
}
