//
//  ACWundergroundWebCamModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-31.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

class ACWundergroundWebCamModel: ACMappableProtocol {
    private(set) var CAMURL: String?
    private(set) var CURRENTIMAGEURL: String?
    private(set) var WIDGETCURRENTIMAGEURL: String?
    private(set) var assoc_station_id: String?
    private(set) var cameratype: String?
    private(set) var camid: String?
    private(set) var camindex: Int?
    private(set) var city: String?
    private(set) var country: String?
    private(set) var downloaded: String?
    private(set) var handle: String?
    private(set) var isrecent: Int?

    private(set) var lat: Double?
    private(set) var link: String?
    private(set) var linktext: String?
    private(set) var lon: Double?
    private(set) var neighborhood: String?
    private(set) var organization: String?
    private(set) var state: String?
    private(set) var tzname: String?
    private(set) var updated: String?
    private(set) var updated_epoch: Double?
    private(set) var zip: Int?

    required init(dict: ACDictionary) {
        self.CAMURL <~ dict["CAMURL"]
        self.CURRENTIMAGEURL <~ dict["CURRENTIMAGEURL"]
        self.WIDGETCURRENTIMAGEURL <~ dict["WIDGETCURRENTIMAGEURL"]
        self.assoc_station_id <~ dict["assoc_station_id"]
        self.cameratype <~ dict["cameratype"]
        self.camid <~ dict["camid"]
        self.camindex <~ dict["camindex"]
        self.city <~ dict["city"]
        self.country <~ dict["country"]
        self.downloaded <~ dict["downloaded"]
        self.handle <~ dict["handle"]
        self.isrecent <~ dict["isrecent"]
        self.lat <~ dict["lat"]
        self.link <~ dict["link"]
        self.linktext <~ dict["linktext"]
        self.lon <~ dict["lon"]
        self.neighborhood <~ dict["neighborhood"]
        self.organization <~ dict["organization"]
        self.state <~ dict["state"]
        self.tzname <~ dict["tzname"]
        self.updated <~ dict["updated"]
        self.updated_epoch <~ dict["updated_epoch"]
        self.zip <~ dict["zip"]

    }
}
