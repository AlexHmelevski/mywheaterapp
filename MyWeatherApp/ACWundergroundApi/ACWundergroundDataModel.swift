//
//  ACWundergroundDataModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-30.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundergroundDataModel: ACMappableProtocol {
    private(set) var height: Double?
    private(set) var type: String?
    init(dict: ACDictionary) {
        self.height <~ dict["height"]
        self.type <~ dict["type"]
    }
}
