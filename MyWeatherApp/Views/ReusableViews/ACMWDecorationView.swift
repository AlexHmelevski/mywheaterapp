//
//  ACMWDecorationView.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 15-10-28.
//  Copyright © 2015 Alex Crow. All rights reserved.
//

import Foundation

class ACMWDecorationView: UICollectionReusableView {
    var gradient: CAGradientLayer!

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        layer.cornerRadius = self.bounds.size.width * 0.02
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        //TODO: find solution to make it more "Swifty"
        guard let _ = self.gradient else {
            self.gradient = CAGradientLayer.alphaGradientLayer(.black, startAlpha: 1, endAlpha: 0.3)
            self.gradient.frame = bounds
            self.gradient.cornerRadius = layer.cornerRadius
            self.gradient.opacity = 0.25
            self.layer.insertSublayer(gradient, at: 1)
            return
        }
        gradient.frame = CGRect(x: self.gradient.frame.origin.x, y: self.gradient.frame.origin.y, width: self.frame.size.width, height: self.frame.size.height)
    }

}
