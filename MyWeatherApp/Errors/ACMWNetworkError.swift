//
//  ACMWNetworkError.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-17.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

public enum ACMWNetworkError: ACMWErrorType {
	case notJSONModel
	case errorGettingURLFromString(url: String)
	case networkError(domain: String, code: Int, descrpition: String?, reason: String?)
	case tooManyResults
	case noConnection
    case serializeError(object: Any)
    case unknown

    public var description: String {
		switch self {
		case .notJSONModel: return "The returned object is not a JSON model"
		case .errorGettingURLFromString(let url): return "Error getting url from the string \(url)"
		case .networkError(let domain, let code, let descrpition, let reason):
            return "Network domain \(domain), Network code \(code), Network description: \(descrpition ?? ""), reason: \(reason ?? "")"
		case .tooManyResults: return "The result of the request is too vague."
		case .noConnection: return "Check your internet connection"
        case .serializeError(let obj): return "Cannot serialize obj \(obj)"
        case .unknown: return "Unknown error"
		}
	}

	init(error: NSError?) {
		if let unwError = error {
			self = .networkError(domain: unwError.domain, code: unwError.code, descrpition: unwError.description, reason: unwError.localizedFailureReason)
		} else {
			self = .unknown
		}
	}
}
