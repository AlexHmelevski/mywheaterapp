//
//  ACMWForecastCell.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-18.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

public class ACMWForecastCell: ACMWCollectionViewCell {
    var temperatureLabel: ACMWLabel!
    var forecastIcon: ACMWWeatherIcon!
    var dayLabel: ACMWLabel!
    let appearanceProxy = ACMWDesignProxy()

    class func registerCellClass(_ collectionView: UICollectionView) {
        collectionView.register(ACMWShortForecastCell.self, forCellWithReuseIdentifier: ACMWForecastCellType.ShortForecast.description)
        collectionView.register(ACMWFullForecastCell.self, forCellWithReuseIdentifier: ACMWForecastCellType.FullForecast.description)
        collectionView.register(ACMWHourForecastCell.self, forCellWithReuseIdentifier: ACMWForecastCellType.HourForecast.description)
        collectionView.register(ACMWWeekDayForecastCell.self, forCellWithReuseIdentifier: ACMWForecastCellType.WeekDay.description)
        collectionView.register(ACMWForecastCell.self, forCellWithReuseIdentifier: ACMWForecastCellType.BaseCell.description)
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        if self.temperatureLabel == nil {
            let temperatureBuilder = ACMWLabelBuilder.getLabelBuilder(ACMWLabelBuilder.labelTypes.temperature)
            self.temperatureLabel = temperatureBuilder.buildLabel()
            self.temperatureLabel.textAlignment = .center
        }
        if self.forecastIcon == nil {
            self.forecastIcon = ACMWWeatherIcon(frame: .zero)
        }
        if self.dayLabel == nil {
            let labelBuilder = ACMWLabelBuilder.getLabelBuilder(ACMWLabelBuilder.labelTypes.smallCapsLabels)
            self.dayLabel = labelBuilder.buildLabel()
            self.dayLabel.textAlignment = .center
        }
        self.configureAutoLayout()
    }

    func configureCell(_ day: String, temperature: Int, feelsLike: Int) {
        fatalError("Sublcass \(self.classForCoder) must override \(#function) at \(#column)")
    }

    func configureAutoLayout() {
        //  fatalError("Sublcass \(self.classForCoder) must override \(#function) at \(#column)")
    }

    func designType() -> ACMWDesignType {
        switch self {
            case is ACMWHourForecastCell : return ACMWDailyForecast
            case is ACMWShortForecastCell : return ACMWShortForecast
            case is ACMWFullForecastCell : return ACMWFullForecast
            case is ACMWWeekDayForecastCell : return ACMWShortForecast
        default: break
        }
        return ACMWDailyForecast
    }
}
