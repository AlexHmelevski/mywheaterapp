//
//  ACWundegroundMovementModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-29.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundegroundMovementModel: ACMappableProtocol {
    private(set) var degrees: Int = 0
    private(set) var text = String()
    init(dict: ACDictionary) {
        self.degrees <~ (dict["Degrees"] ??  dict["degrees"])
        self.text <~ (dict["Text"] ?? dict["dir"])
    }
}
