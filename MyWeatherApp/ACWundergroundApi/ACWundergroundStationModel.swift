//
//  ACWundergroundAirportModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-29.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundergroundStationModel: ACMappableProtocol {
    private(set) var city = String("NA")
    private(set) var country = String("NA")
    private(set) var icao = String("NA")
    private(set) var lat: Double = 0
    private(set) var lon: Double = 0
    private(set) var state = String("NA")

    init(dict: ACDictionary) {
        self.city <~ dict["city"]
        self.country <~ dict["country"]
        self.icao <~ dict["icao"]
        self.lat <~ dict["lat"]
        self.lon <~ dict["lon"]
        self.state <~ dict["state"]
    }
}
