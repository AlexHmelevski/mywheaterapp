//
//  ForecastModelData.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 15-12-19.
//  Copyright © 2015 Alex Crow. All rights reserved.
//

import Foundation
import UIKit

struct GeneralProjectConfig {
    static var isTestTarget: Bool {
        return false
        #if TESTING
            return true
        #else
            return false
        #endif
    }
}

public enum ACMWForecastType: String, CustomFromStringConvertable {
	case Clear = "clear"
	case Night_Clear = "nt_clear"
	case Cloudy = "cloudy"
	case Night_Cloudy = "nt_cloudy"
	case MousltyCloudy = "mostlycloudy"
	case Night_MousltyCloudy = "nt_mostlycloudy"
	case Rain = "rain"
	case Night_Rain = "nt_rain"
	case ChanceRain = "chancerain"
	case Night_ChanceRain = "nt_chancerain"

	case Snow = "Snow"
	case Night_Snow = "nt_snow"
	case PartlyCloudy = "partlycloudy"
	case Night_PartlyCloudy = "nt_partlycloudy"
	case Storms = "storms"
	case Night_Storms = "nt_storms"
	case Chancetstorms = "chancetstorms"
	case Night_Chancetstorms = "nt_chancetstorms"

	private static let allValues = [Clear,
	                                Night_Clear,
	                                Cloudy,
	                                MousltyCloudy,
	                                Night_Cloudy,
	                                Night_MousltyCloudy,
	                                Rain,
	                                Night_Rain,
	                                Snow,
	                                Night_Snow,
	                                PartlyCloudy,
	                                Night_PartlyCloudy,
	                                ChanceRain,
	                                Night_ChanceRain,
	                                Storms,
	                                Night_Storms,
	                                Chancetstorms,
	                                Night_Chancetstorms]

	private static let dayValues = [Clear, Cloudy, MousltyCloudy]

	public init?(_ text: String) {
		for type in ACMWForecastType.allValues {
			if type.rawValue.uppercased() == text.uppercased() {
				self = type
				return
			}
		}
		return nil
	}
}
