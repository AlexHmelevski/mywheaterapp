//
//  ACWundergroundTideInfoModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-30.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundergroundTideInfoModel: ACMappableProtocol {
    private(set) var lat: Double?
    private(set) var lon: Double?
    private(set) var tideSite: String?
    private(set) var type: String?
    private(set) var tzname: String?
    private(set) var units: String?

    init(dict: ACDictionary) {
        self.lat <~ dict["lat"]
        self.lon <~ dict["lon"]
        self.tideSite <~ dict["tideSite"]
        self.type <~ dict["type"]
        self.tzname <~ dict["tzname"]
        self.units <~ dict["units"]
    }
}
