//
//  ACMWContentViewController.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-06.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import UIKit

protocol ICityForecastVCObserver: class {
    func buttonPressed(type: NavigationButtonTypes)
}

class ACMWCityForecastViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate {
    private var numberOfRows = 5
    fileprivate var mainCollectionView: ACMWCollectionView!
    private let dProxy = ACMWDesignProxy()
    private let nType = ACMWWeatherIconTypeHeavyRain
    private weak var footer = ACMWCollectionForecastFooterView()
    fileprivate let dataSource: ACMWCollectionViewDataSource
    private static let sDataControllerID =  "ACMWCityForecastViewController"
    var output: ForecastInput!
    var errorHandler: IErrorHandler = BasicErrorHandler.createChain(from: [GlobalAlertErrorHandler.self])

    @IBOutlet private var backgroundImageView: UIImageView!
    private var backgroundEffect = UIVisualEffectView()
    private var animator = UIViewPropertyAnimator()

    required init(source: ACMWCollectionViewDataSource) {
        self.dataSource = source
        super.init(nibName: ACMWCityForecastViewController.sDataControllerID, bundle: Bundle.main)

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Not Implemented")
    }

    private func setUpBlur() {
        view.insertSubview(backgroundEffect, aboveSubview: backgroundImageView)
        backgroundEffect.translatesAutoresizingMaskIntoConstraints = false
        backgroundEffect.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundEffect.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        backgroundEffect.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        backgroundEffect.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true

        animator = UIViewPropertyAnimator(duration: 1, curve: .linear)
        animator.addAnimations {
            self.backgroundEffect.effect = UIBlurEffect(style: .light)
        }

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dProxy.setBackgroundImage(self.backgroundImageView, with: self.nType)
        self.configureCollectionView()
        self.configureBlurImage(self.nType)
        setUpBlur()
        self.dataSource.accossiateWith(self.mainCollectionView)
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.mainCollectionView.frame = self.view.bounds
        self.mainCollectionView.collectionViewLayout.invalidateLayout()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        output?.didRequestForecast()
        mainCollectionView.refresh?.onStart = { [weak self] in self?.output?.didRequestForecast() }

    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return dataSource.numberOfItemsIn(section)
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return dataSource.numberOfSections()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       return dataSource.cellForItemAt(indexPath)
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        animator.fractionComplete = scrollView.contentOffset.y / ( self.mainCollectionView.frame.size.height * 5)
    }

    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {

        let reusableView = dataSource.collectionView(collectionView, viewForSupplementaryElementOfKind: kind, at: indexPath)

        if let navBar = reusableView as? ACMWNavigationBar {
            navBar.backgroundImage = self.backgroundImageView.image
            navBar.addTargetFor(buttonType: .settings, target: self, action: #selector(settingsPressed))
            navBar.addTargetFor(buttonType: .share, target: self, action: #selector(sharePressed))
            navBar.addTargetFor(buttonType: .add, target: self, action: #selector(addCity))
        }

        return reusableView
    }

    @objc
    private func addCity() {
        output?.didPressAdd()
    }

    @objc
    private func sharePressed() {

    }

    @objc
    private func settingsPressed() {
        output?.didPressSettings()
    }

    private func configureCollectionView() {
        let layout = ACMWVerticalLayout()
        mainCollectionView = ACMWCollectionView(frame: self.view.bounds, collectionViewLayout: layout)
        view.addSubview(self.mainCollectionView)
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
        mainCollectionView.backgroundColor = .clear

        mainCollectionView.register(ACMWHourlyForecastCollectionViewCell.self,
                                    forCellWithReuseIdentifier: ACMWHourlyForecastCollectionViewCell.kind)
        mainCollectionView.register(ACMWCollectionForecastFooterView.self,
                                         forSupplementaryViewOfKind: UICollectionElementKindSectionFooter,
                                                withReuseIdentifier: ACMWCollectionForecastFooterView.kind)
        mainCollectionView.register(ACMWCollectionForecastHeaderView.self,
                                         forSupplementaryViewOfKind: UICollectionElementKindSectionHeader,
                                                withReuseIdentifier: ACMWCollectionForecastHeaderView.kind)
        ACMWForecastCell.registerCellClass(self.mainCollectionView)
        self.mainCollectionView.register(ACMWNavigationBar.self,
                                         forSupplementaryViewOfKind: ACMWNavigationBar.kind,
                                                withReuseIdentifier: ACMWNavigationBar.kind)
    }

    private func configureBlurImage(_ type: ACMWWeatherIconType) {
//        self.dProxy.setBackgroundImage(self.backgroundImageViewWithFX, with: type)
//        self.backgroundImageViewWithFX.image = self.dProxy.bluredBackgroundImage(for: self.backgroundImageView.image)
//        self.backgroundImageViewWithFX.alpha = 0
    }

}

extension ACMWCityForecastViewController: ForecastOutput {
    func update(with model: ForecastViewModel) {
        dataSource.update(with: model)
        if model.state != .loading {
            mainCollectionView.refresh?.refreshing = false
        }

    }
}
