//
//  ACMWLabel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-01-04.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import UIKit

class ACMWLabelBuilder {
	var fontSize: Double = 11.0 {
		didSet {
			self.attributedFormatter?.fontSize = fontSize
		}
	}
	var fontColor: UIColor = .black
	var shadowColor: UIColor?
	var shadowOffset: CGSize = .zero
	var fakeSmallCaps: Bool = false
	var attributedFormatter: ACMWTextFormatter?
	var textFormatter: ValueFormatter?
	var frame: CGRect = CGRect.zero
	var textAllignment: NSTextAlignment = NSTextAlignment.center

	enum labelTypes {
		case temperature
		case smallCapsLabels
		case percentValues
	}

	class func getLabelBuilder(_ type: labelTypes) -> ACMWLabelBuilder {
		switch type {
			case .temperature: return TemperatureLabelBuilder()
			case .smallCapsLabels: return TextLabelBuilder()
			case .percentValues: return PercentLabelBuilder()
		}
	}

	func buildLabel() -> ACMWLabel {
		return ACMWLabel(attributesFormatter: attributedFormatter,
                               textFormatter: textFormatter,
                                   fontColor: fontColor, shadowCollor: shadowColor,
                                shadowOffset: shadowOffset,
                                    fontSize: fontSize,
                              textAllignment: self.textAllignment,
                                       frame: frame)
	}

}

class ACMWLabel: UILabel {
	var textFormatter: ValueFormatter?

	var attributedFormatter: ACMWTextFormatter? {
		didSet {
			if let attString = self.attributedText,
				let formatter = self.attributedFormatter {
					self.attributedText = formatter.convert(attString.string)
			}
		}
	}

	final override var text: String? {
		didSet {
			// TODO: check for general settings
			var formattedValue: String?
			if let value = self.text {
				if let txtFormatter = self.textFormatter,
					let intValue = Int(value) {
						formattedValue = txtFormatter.formatValue(intValue)
						text = formattedValue
				}
				if let formatter = self.attributedFormatter,
					let value = text {
						self.attributedText = formatter.convert(value)
				}
			}
		}
	}
// swiftlint:disable next vertical_parameter_alignment
	init(attributesFormatter: ACMWTextFormatter?,
               textFormatter: ValueFormatter?,
                   fontColor: UIColor?,
                shadowCollor: UIColor?,
                shadowOffset: CGSize?,
                    fontSize: Double,
              textAllignment: NSTextAlignment?,
                       frame: CGRect) {
		if let attFormatter = attributesFormatter {
			self.attributedFormatter = attFormatter
		}

		if let txtFormatter = textFormatter {
			self.textFormatter = txtFormatter
		}

		super.init(frame: frame)

		if let shColor = shadowCollor {
			self.shadowColor = shColor
		}

		if let offset = shadowOffset {
			self.shadowOffset = offset
		}

		if let color = fontColor {
			self.textColor = color
		}

		if let allignment = textAllignment {
			self.textAlignment = allignment
		}

	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

    override var intrinsicContentSize: CGSize {
		var size = super.intrinsicContentSize
		if let _ = self.textFormatter {
			// Here want to be sure that the label wont be less than needed size. If we have a formatted value like + "C" or + "%".
			// Assuming that we have formatter adds max 2 extra characters. So we have 2 digits for temperature and 2 special characters
			// Aspect ration w/h in font usually 0.5
			size.width = CGFloat(self.font.fontSize() * 0.5 * 5)
		}

		return size
	}
}

fileprivate protocol TemperatureLabelFormatter {
	func setTemperature(_ value: Int)
	var formatter: ValueFormatter! { get set }
}

fileprivate class TemperatureLabelBuilder: ACMWLabelBuilder {
	fileprivate override init() {
		super.init()

		// TODO: get initial settings from SettingsManager
		self.fontSize = 18
		self.fontColor = .white
		self.shadowColor = .black
		self.shadowOffset = CGSize(width: 1, height: 1)
		self.textAllignment = NSTextAlignment.right
		self.textFormatter = ValueFormatter(formatType: ACMWFormatType.Celsius)
		self.attributedFormatter = ACMWTextFormatter(fontName: "HelveticaNeue", fontSize: fontSize, type: ACMWTextFormatter.FormatterStyle.temperature)

	}

}

fileprivate class TextLabelBuilder: ACMWLabelBuilder {
	fileprivate override init() {
		super.init()
		// TODO: get initial settings from SettingsManager
		self.fontSize = 18
		self.fontColor = .white
		self.shadowColor = .black
		self.shadowOffset = CGSize(width: 1, height: 1)
		self.textFormatter = nil
		self.attributedFormatter = ACMWTextFormatter(fontName: "HelveticaNeue", fontSize: fontSize, type: ACMWTextFormatter.FormatterStyle.textLabels)

	}
}

fileprivate class PercentLabelBuilder: ACMWLabelBuilder {
	fileprivate override init() {
		super.init()
		// TODO: get initial settings from SettingsManager
		self.fontSize = 18
		self.fontColor = .white
		self.shadowColor = .black
		self.shadowOffset = CGSize(width: 1, height: 1)
		self.textFormatter = ValueFormatter(formatType: ACMWFormatType.Percent)
		self.attributedFormatter = nil
		self.textAllignment = NSTextAlignment.left

	}
}
