//
//  ACMWGradientViewBase.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 15-12-17.
//  Copyright © 2015 Alex Crow. All rights reserved.
//

import UIKit

@IBDesignable
class ACMWGradientViewBase: UIView {

	private var gradientLayer = CAGradientLayer()
	private var startPoint: CGPoint = .zero
	private var endPoint: CGPoint = .zero

	struct gradientDirections: OptionSet {
		let rawValue: Int
		internal init(rawValue: Int) { self.rawValue = rawValue }
		static let directionLeft = gradientDirections(rawValue: 1)
		static let directionRight = gradientDirections(rawValue: 2)
		static let directionUp = gradientDirections(rawValue: 4)
		static let directionDown = gradientDirections(rawValue: 8)
	}

	var direction: gradientDirections = [.directionDown] {
		didSet {
			self.calculateGradientPoints()
			self.updateGradient()
		}
	}

	var cornerRadius: Float = 40

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}

	convenience init() {
		self.init(frame: .zero, color: .black, endAlpha: 0)
	}

	init(frame: CGRect, color: UIColor, endAlpha: Float) {
		super.init(frame: frame)
		self.gradientLayer = CAGradientLayer.alphaGradientLayer(color, startAlpha: 1, endAlpha: endAlpha)
		self.gradientLayer.frame.origin = .zero
		self.gradientLayer.frame.size = frame.size
		self.gradientLayer.opacity = 0.5
		layer.addSublayer(gradientLayer)
	}

	var opacity: Float {
		get {
			return self.gradientLayer.opacity
		}
		set {
			self.gradientLayer.opacity = newValue
			self.calculateGradientPoints()
			self.updateGradient()
			self.layoutIfNeeded()
		}
	}

	var endAlpha: Float? {
		get {
			return self.gradientLayer.locations?.last?.floatValue
		}
		set {
			self.gradientLayer.removeFromSuperlayer()
			if let alpha = newValue {
				self.gradientLayer = CAGradientLayer.alphaGradientLayer(.gray, startAlpha: 1, endAlpha: alpha)
				self.gradientLayer.frame.origin = .zero
				self.gradientLayer.frame.size = frame.size
				self.gradientLayer.opacity = 0.5
				layer.addSublayer(gradientLayer)
				self.setNeedsLayout()
				self.layoutIfNeeded()
			}
		}
	}

	override func layoutSubviews() {
		calculateGradientPoints()
		updateGradient()
		super.layoutSubviews()
	}

	private func calculateGradientPoints() {
		assert(!direction.contains([.directionUp, .directionDown]) && !direction.contains([.directionLeft, .directionRight]), "Cannot make gradient in opposite directions ")
		var xStart: CGFloat = 0.5
		var yStart: CGFloat = 0.5
		var xEnd: CGFloat = 0.5
		var yEnd: CGFloat = 0.5
		if direction.contains([.directionLeft]) {
			xStart = 0.0
			xEnd = 1.0
		}
		if direction.contains([.directionRight]) {
			xStart = 1.0
			xEnd = 0.0
		}

		if direction.contains([.directionDown]) {
			yStart = 0.0
			yEnd = 1.0
		}
		if direction.contains([.directionUp]) {
			yStart = 1.0
			yEnd = 0.0
		}

		startPoint = CGPoint(x: xStart, y: yStart)
		endPoint = CGPoint(x: xEnd, y: yEnd)
	}

	private func updateGradient() {
		gradientLayer.frame.origin = .zero
		gradientLayer.frame.size = frame.size
		gradientLayer.cornerRadius = CGFloat(cornerRadius)
		gradientLayer.startPoint = startPoint
		gradientLayer.endPoint = endPoint
	}
}
