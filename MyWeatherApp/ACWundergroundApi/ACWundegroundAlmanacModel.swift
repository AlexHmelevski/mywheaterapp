//
//  ACWundegroundAlmanacModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-28.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

public struct ACWundegroundAlmanacModel: ACMappableProtocol {
    private(set) public var airport_code = String()
    private(set) public var temp_high = ACWundegroundTemperatureModel(dict: ["": "" as AnyObject])
    private(set) public var temp_low = ACWundegroundTemperatureModel(dict: ["": "" as AnyObject])
    public init(dict: ACDictionary) {
        self.airport_code <~ dict["airport_code"]
        self.temp_high <~ dict["temp_high"]
        self.temp_low <~ dict["temp_low"]
    }
}
