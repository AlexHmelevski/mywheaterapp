//
//  ACMWButton.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-18.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

class ACMWButton: UIButton {
	override var isSelected: Bool {
		willSet {
			if newValue {
				self.titleLabel?.textColor = self.titleColor(for: UIControlState.selected)
			} else {
				self.titleLabel?.textColor = self.titleColor(for: [])
			}

		}
	}
}
