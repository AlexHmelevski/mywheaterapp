//
//  ACMWMetricConverterTest.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-19.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import XCTest

class ACMWMetricConverterTest: XCTestCase {

    func testDoubleMetricConvert() {
        let doubles = ACWundergroundMetric<Double>(english: 10.20, metric: -0.20)
        XCTAssertEqual(doubles.English, 10.20)
        XCTAssertEqual(doubles.Metric, -0.20)
    }

    func testStringsMetricConvert() {
        let fromStringsDoubles = ACWundergroundMetric<Double>(english: "10.20", metric: "-0.20")
        XCTAssertEqual(fromStringsDoubles.English, 10.20)
        XCTAssertEqual(fromStringsDoubles.Metric, -0.20)

        let fromStringsInts = ACWundergroundMetric<Int>(english: "10", metric: "-12")
        XCTAssertEqual(fromStringsInts.English, 10)
        XCTAssertEqual(fromStringsInts.Metric, -12)

    }

    func testIntMetricConvert() {
        let ints = ACWundergroundMetric<Int>(english: 10, metric: -12)
        XCTAssertEqual(ints.English, 10)
        XCTAssertEqual(ints.Metric, -12)
    }

    func testFloatMetricConvert() {
        let floats = ACWundergroundMetric<Float>(english: Float(10.20), metric: Float(-0.20))
        XCTAssertEqual(floats.English, 10.20)
        XCTAssertEqual(floats.Metric, -0.20)
    }

    func testCGFLoatMetricConvert() {
        let floats = ACWundergroundMetric<CGFloat>(english: CGFloat(10.20), metric: CGFloat(-0.20))
        XCTAssertEqual(floats.English, 10.20)
        XCTAssertEqual(floats.Metric, -0.20)
    }

    func testWrongTypesMetricConvert() {
        let fromStrings = ACWundergroundMetric<Double>(english: "fdfd", metric: "sss")
        XCTAssertEqual(fromStrings.English, 0)
        XCTAssertEqual(fromStrings.Metric, 0)

    }

}
