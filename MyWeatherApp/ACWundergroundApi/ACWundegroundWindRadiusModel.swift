//
//  ACWundegroundWindRadiusModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-29.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation
struct ACWundegroundWindRadiusModel: ACMappableProtocol {
    private(set) var r12: ACWundegroundRadiusModel?
    private(set) var r34: ACWundegroundRadiusModel?
    private(set) var r50: ACWundegroundRadiusModel?
    private(set) var r64: ACWundegroundRadiusModel?
    init(dict: ACDictionary) {
        self.r12 <~ dict["12"]
        self.r34 <~ dict["34"]
        self.r50 <~ dict["50"]
        self.r64 <~ dict["64"]
    }
}
