//
//  ACWundegroundForecastDayModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-29.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundegroundForecastDayModel: ACMappableProtocol {

    private(set) var avehumidity: Int = 0
    private(set) var conditions = String()
    private(set) var date = ACWundegroundTimeModel(dict: ["": "" as AnyObject])
    private(set) var fcttext = String()
    private(set) var fcttext_metric = String()
    private(set) var high = ACWundergroundValue(dict: ["english": 0 as AnyObject, "metric": 0 as AnyObject])

    private(set) var icon = ACMWForecastWeatherType("")
    private(set) var icon_url = String()
    private(set) var low = ACWundergroundValue(dict: ["english": 0 as AnyObject, "metric": 0 as AnyObject])
    private(set) var maxhumidity: Int = 0
    private(set) var maxwind = ACWundegroundSpeedModel(dict: ["": "" as AnyObject])
    private(set) var minhumidity: Int = 0
    private(set) var period: Int = 0
    private(set) var pop: Int = 0

    private(set) var qpf_allday = ACWundegroundPrecipitationSize(dict: ["in": 0 as AnyObject, "mm": 0 as AnyObject])
    private(set) var qpf_day = ACWundegroundPrecipitationSize(dict: ["in": 0 as AnyObject, "mm": 0 as AnyObject])
    private(set) var qpf_night = ACWundegroundPrecipitationSize(dict: ["in": 0 as AnyObject, "mm": 0 as AnyObject])

    private(set) var skyicon = String()
    private(set) var snow_allday = ACWundegroundPrecipitationSize(dict: ["in": 0 as AnyObject, "mm": 0 as AnyObject])
    private(set) var snow_day = ACWundegroundPrecipitationSize(dict: ["in": 0 as AnyObject, "mm": 0 as AnyObject])
    private(set) var snow_night = ACWundegroundPrecipitationSize(dict: ["in": 0 as AnyObject, "mm": 0 as AnyObject])
    private(set) var title = String()

    init(dict: ACDictionary) {
        self.avehumidity <~ dict["avehumidity"]
        self.conditions <~ dict["conditions"]
        self.date <~ dict["date"]
        self.fcttext <~ dict["fcttext"]
        self.fcttext_metric <~ dict["fcttext_metric"]
        self.high <~ dict["high"]
        self.icon <~ dict["icon"]
        self.icon_url <~ dict["icon_url"]
        self.low <~ dict["low"]
        self.maxhumidity <~ dict["maxhumidity"]
        self.maxwind <~ dict["maxwind"]
        self.minhumidity <~ dict["minhumidity"]
        self.period <~ dict["period"]
        self.pop <~ dict["pop"]
        self.qpf_allday <~ dict["qpf_allday"]
        self.qpf_day <~ dict["qpf_day"]
        self.qpf_night <~ dict["qpf_night"]
        self.skyicon <~ dict["skyicon"]
        self.snow_allday <~ dict["snow_allday"]
        self.snow_day <~ dict["snow_day"]
        self.snow_night <~ dict["snow_night"]
        self.title <~ dict["title"]
    }
}
