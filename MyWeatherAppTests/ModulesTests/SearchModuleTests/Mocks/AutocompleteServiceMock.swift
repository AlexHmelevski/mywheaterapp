//
//  AutocompleteServiceMock.swift
//  MyWeatherApp
//
//  Created by Alex Hmelevski on 2017-06-25.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import Foundation
import AHFuture
import ALEither
@testable import MyWeatherApp

final class AutocompleteServiceMock: IAutocompleteService {
    var shouldFail = false
    func getAutocompleteResults(for text: String) -> AHFuture<AHWundergroundAutocompleteResultModel, ACMWNetworkError> {
        return AHFuture(scope: { [weak self] (completeion) in
            guard let `self` = self else { return }
            self.shouldFail » self.convertToResponse » { completeion($0) }
            
        })
        
    }
    
    private func convertToResponse(bool: Bool)-> ALEither<AHWundergroundAutocompleteResultModel,ACMWNetworkError> {
        if self.shouldFail {
            return .wrong(value: .noConnection)
        } else {
            return .right(value: WundergroundModelsMockFactory().autocompleteModel)
        }
    }
}
