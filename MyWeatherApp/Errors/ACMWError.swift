//
//  ACMWError.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-17.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

public enum ACMWError: ACMWErrorType {
	case networkError(error: ACMWNetworkError)
	case persistenceError(error: ACMWPersistenceError)

    public var description: String {
		switch self {
		case .networkError(let err): return err.description
		case .persistenceError(let err): return err.description
		}
	}

	init(error: ACMWNetworkError) {
		self = ACMWError.networkError(error: error)
	}

	init(error: ACMWPersistenceError) {
		self = ACMWError.persistenceError(error: error)
	}
}
