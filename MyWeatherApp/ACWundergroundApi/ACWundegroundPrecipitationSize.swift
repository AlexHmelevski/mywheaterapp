//
//  ACWundegroundSrecipitationSize.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-29.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundegroundPrecipitationSize: ACMappableProtocol {
    private(set) var inches: Double = 0
    private(set) var mm: Double = 0

    init(dict: ACDictionary) {
        self.inches <~ dict["in"]
        self.mm <~ dict["mm"]
    }
}
