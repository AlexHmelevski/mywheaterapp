//
//  ACMWCellData.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-18.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

public protocol ACMWCellData {
	var temperature: Int { get set }
	var feels_like: Int { get set }
	var weatherIcon: ACMWForecastType { get set }
	var humidiy: Int { get set }
	var wind: String { get set }
	var day: String { get set }
	var hour: Int { get set }
}
