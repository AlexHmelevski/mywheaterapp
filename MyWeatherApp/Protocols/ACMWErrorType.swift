//
//  ACMWErrorType.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-17.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

public protocol ACMWErrorType: Error, CustomStringConvertible {

}
