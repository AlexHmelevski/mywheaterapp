//
//  ACMWPeriodOfTime.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-18.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

public enum ACMWPeriodOfTime: CustomStringConvertible {
	case night
	case morning
	case day
	case evening

	init(hour: Int) {
		precondition(hour >= 0 && hour < 24, "Should be a real time interval")
		switch hour {
            case 0..<6: self = .night
            case 6..<12: self = .morning
            case 12..<18: self = .day
            case 18..<24: self = .evening
		default: self = .night
		}
	}

	public var description: String {

        switch self {
            case .night: return NSLocalizedString("Night", comment: "")
            case .morning: return NSLocalizedString("Morning", comment: "")
            case .day: return NSLocalizedString("Day", comment: "")
            case .evening: return NSLocalizedString("Evening", comment: "")
        }
	}
}
