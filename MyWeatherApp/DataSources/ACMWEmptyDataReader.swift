//
//  ACMWEmptyDataReader.swift
//  MyWeatherApp
//
//  Created by Alex Hmelevski on 2017-02-13.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import Foundation
import AHFuture

final class ACMWEmptyDataReader: ACMWDataReader {
    func read() -> AHFuture<ACWundergroundModel, ACMWError> {
        return AHFuture(scope: { (completion) in
            completion(.right(value: ACWundergroundModel(dict: ["": ""])))
        })
    }

}
