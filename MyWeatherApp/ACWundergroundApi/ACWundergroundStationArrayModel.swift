//
//  ACWundergroundStationArrayModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-29.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundergroundStationArrayModel: ACMappableProtocol {
    private(set) var station: [ACWundergroundStationModel]?

    init(dict: ACDictionary) {
        self.station <~ dict["station"]

    }
}
