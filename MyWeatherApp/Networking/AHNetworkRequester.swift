//
//  ACMWNetworkingRequester.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-23.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

import AHFuture
import AHJSONSerializer
import AHNetwork

public class AHNetworkRequester {
    var serializer: ISerializer = ACMWSerializer()

    func getData<U: ACMappableProtocol>(for type: AHNetworkRequestType) -> AHFuture<U, ACMWNetworkError> {
        return  AHNetworkProvider().requestFuture(for: type.request).transform(transform: { $0.data },
                                                                             transformError: convert)
            .flatMap(transform: self.serializer.serialize)
    }

    func getData<U: JSONDecodable>(for type: AHNetworkRequestType) -> AHFuture<U, ACMWNetworkError> {
        return  AHNetworkProvider().requestFuture(for: type.request)
                                      .transform(transform: { $0.data },
                                                 transformError: convert)
                                      .flatMap(transform: self.serializer.serialize)
    }

    private func convert(error: Error) -> ACMWNetworkError {
        return transform(nsError: error as NSError)

    }

    private func transform(nsError error: NSError) -> ACMWNetworkError {
        return .networkError(domain: error.domain, code: error.code, descrpition: error.description, reason: nil)
    }

}
