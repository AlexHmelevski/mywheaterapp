//
//  IErrorHandler.swift
//  MyWeatherApp
//
//  Created by Alex Hmelevski on 2017-02-08.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import Foundation

protocol IErrorHandler {
    func catchError(error: Error)
}
