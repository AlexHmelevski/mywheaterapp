//
//  ACMWMainForecastPresenter.swift
//  MyWeatherApp
//
//  Created by Alex Hmelevski on 2017-06-28.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import Foundation

protocol ForecastPresenter {
    func present(forecastModel model: ForecastDataModel)
}

final class  ACMWMainForecastPresenter {
    fileprivate weak var output: ForecastOutput?
    fileprivate let adapter = ACMWCellDataAdapter()
    fileprivate let metric: ACWundergroundMetricType = .metric
    init(output: ForecastOutput) {
        self.output = output
    }
}

extension ACMWMainForecastPresenter: ForecastPresenter {

    func present(forecastModel model: ForecastDataModel) {
        adapter.set(model: model.wundergroundModel)
        adapter.set(metric: metric)
        let current = adapter.cellData(forType: .currentDay)
        let hourly = adapter.cellData(forType: .hourly)
        let weekly = adapter.cellData(forType: .weekInfo)
        let dictionary = merge(dictionaries: convert(data: current, for: 0),
                                             convert(data: current, for: 1),
                                             convert(data: hourly, for: 2),
                                             convert(data: weekly, for: 3))

        output?.update(with: ForecastViewModel(cellData: dictionary, state: model.state, cityName: model.wundergroundModel.geolookup?.city ?? ""))

    }

    private func convert(data: [ACMWCellData], for section: Int) -> [IndexPath: ACMWCellData] {
        return data.enumerated().reduce([:], { (partial, item: (offset: Int, element: ACMWCellData)) -> [IndexPath: ACMWCellData] in
            var new = partial
            let index = IndexPath(row: item.offset, section: section)
            new[index] = item.element
            return new
        })
    }

    private func merge(dictionaries: [IndexPath: ACMWCellData]...) -> [IndexPath: ACMWCellData] {
        return dictionaries.reduce([:], { (partial, current) -> [IndexPath: ACMWCellData]  in
            var mutable = partial
            current.keys.forEach({ (index) in
                mutable[index] = current[index]
            })
            return mutable
        })
    }

}
