//
//  ACWundegroundObservationLocation.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-28.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundergroundDisplayLocation: ACMappableProtocol {
    private(set) var city: String?
    private(set) var country: String?
    private(set) var country_iso3166: String?
    private(set) var country_name: String?
    private(set) var elevation: Double?
    private(set) var full: String?
    private(set) var latitude: Float?
    private(set) var longitude: Double?
    private(set) var magic: Int?
    private(set) var nearby_weather_stations: ACWundergroundNearbyStationModel?
    private(set) var requesturl: String?
    private(set) var state: String?
    private(set) var state_name: String?
    private(set) var type: String?
    private(set) var tz_long: String?
    private(set) var tz_short: String?

    private(set) var wmo: Int?
    private(set) var wuiurl: String?
    private(set) var zip: Int?

    init(dict: ACDictionary) {
        self.city <~ dict["city"]
        self.country <~ dict["country"]
        self.country_iso3166 <~ dict["country_iso3166"]
        self.country_name <~ dict["country_name"]

        self.elevation <~ dict["elevation"]
        self.full <~ dict["full"]
        self.latitude <~ (dict["latitude"] ?? dict["lat"])
        self.longitude <~ (dict["longitude"] ?? dict["long"])
        self.magic <~ dict["magic"]
        self.nearby_weather_stations <~ dict["nearby_weather_stations"]
        self.requesturl <~ dict["requesturl"]
        self.state <~ dict["state"]
        self.state_name <~ dict["state_name"]
        self.type <~ dict["type"]

        self.tz_long <~ dict["tz_long"]
        self.tz_short <~ dict["tz_short"]

        self.wmo <~ dict["wmo"]
        self.wuiurl <~ dict["wuiurl"]
        self.zip <~ dict["zip"]
    }
}
