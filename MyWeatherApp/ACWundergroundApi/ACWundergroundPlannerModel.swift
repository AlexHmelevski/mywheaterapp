//
//  ACWundergroundPlannerModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-30.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundergroundPlannerModel: ACMappableProtocol {
    private(set) var airport_code: String?
    private(set) var chance_of: ACWundergroundChanceOfModel?
    private(set) var dewpoint_high: ACWundergroundDewpointModel?
    private(set) var dewpoint_low: ACWundergroundDewpointModel?
    private(set) var error: String?
    private(set) var period_of_record: ACWundergroundPeriodModel?
    private(set) var precip: ACWundergroundDewpointModel?
    private(set) var temp_high: ACWundergroundDewpointModel?
    private(set) var temp_low: ACWundergroundDewpointModel?
    private(set) var title: String?

    init(dict: ACDictionary) {
        self.airport_code <~ dict["airport_code"]
        self.chance_of <~ dict["chance_of"]
        self.dewpoint_high <~ dict["dewpoint_high"]
        self.dewpoint_low <~ dict["dewpoint_low"]
        self.error <~ dict["error"]

        self.period_of_record <~ dict["period_of_record"]
        self.precip <~ dict["precip"]
        self.temp_high <~ dict["temp_high"]
        self.temp_low <~ dict["temp_low"]
        self.title <~ dict["title"]
    }
}
