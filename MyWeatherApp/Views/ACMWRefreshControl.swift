//
//  ACMWRefreshControl.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-07.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import UIKit

protocol ACMWRefreshControlProtocol {
    var refreshing: Bool { get set }
    var height: Float { get set }
}

class ACMWRefreshControl: UIView, ACMWRefreshControlProtocol {
    private let blurView = UIView()
    private let animation = ACMWAnimatedView()
    private let textLabel = UILabel()
    var onStart: ALVoidClosure?
    var refreshing: Bool = false {
        willSet {
            if newValue {
                self.animation.startAnimating()
                if !self.refreshing {
                    self.started()
                }

            } else if newValue != self.refreshing {
                self.animation.stopAnimating()
                if let sView = self.superview as? UICollectionView {
                    UIView.animate(withDuration: 0.5, animations: { () -> Void in
                        sView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                    })
                }
            }
        }
    }

    var height: Float = 60

    init() {
       super.init(frame: CGRect.zero)
        self.backgroundColor = .clear
        self.tintColor = UIColor.clear
        blurView.frame = self.bounds
        blurView.backgroundColor = UIColor(hue: 20, saturation: 10, brightness: 0, alpha: 0.5)
        self.addSubviews(self.blurView, self.animation, self.textLabel)
        NSLayoutConstraint.allignVerticaly([self.animation], horizontalAllignmentOptions: .center, boundToSuperView: false)
        NSLayoutConstraint.bottom([self.textLabel], option: .center)
        NSLayoutConstraint.centerHorizontallyInSuperview(self.animation)
        NSLayoutConstraint.allignHorizontaly([self.animation, self.textLabel], verticalAllignmentOptions: .bottom, boundToSuperView: false)
        NSLayoutConstraint.equalWitdh([self.animation], multiplier: 0.1)
        NSLayoutConstraint.aspectRatio(1, height: 1, views: self.animation)
        NSLayoutConstraint.allignHorizontaly([blurView],
                                             verticalAllignmentOptions: .center,
                                             boundToSuperView: true)
        NSLayoutConstraint.allignVerticaly([blurView], horizontalAllignmentOptions: .center, boundToSuperView: true)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    private func started() {
        onStart?()

    }

    override func didMoveToSuperview() {
        if let sView = self.superview {
            if !(sView is UICollectionView) {
               fatalError("\(ACMWRefreshControl.self) Can't be in hiearhy with superview exept \(UICollectionView.self) ")
            }
        }
    }

}
