//
//  ACMWLayoutAttributes.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-20.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

public struct CGAttributes {
    let itemSize: CGSize
    let initialOffsets: CGVector
    let sectionInsets: UIEdgeInsets
    let itemSpacig: CGFloat

    init(frame: CGRect, section: Int) {
        precondition(section >= 0 && section <= 3)
        switch section {
        case 0:
            itemSize = CGSize(width: frame.width / 1.39, height: frame.height / 2.743)
            initialOffsets = .zero
            sectionInsets = UIEdgeInsets(top: frame.height / 6.6, left: frame.width / 2 - itemSize.width / 2, bottom: frame.height / 4.1112, right: frame.width / 2 - itemSize.width / 2)
            itemSpacig = 0

        case 1:
            itemSize = CGSize(width: frame.width / 3.24, height: frame.height / 4.94)
            itemSpacig = (frame.width - itemSize.width * 3) / 4.0
            initialOffsets = CGVector(dx: itemSpacig, dy: 0)

            sectionInsets = UIEdgeInsets(top: 0, left: 0, bottom: frame.height / 26.109, right: 0)

        case 2:
            itemSize = CGSize(width: frame.width * 0.893, height: frame.height / 7.272)
            itemSpacig = 0
            initialOffsets = CGVector(dx: (frame.width / 2) - frame.width * 0.475, dy: 0)
            sectionInsets = UIEdgeInsets(top: 0, left: (itemSize.width / 16.667) / 2, bottom: 0, right: 0)
        case 3:
            itemSize = CGSize(width: frame.width * 0.893, height: frame.height / 13.509)
            itemSpacig = 0
            initialOffsets = CGVector(dx: frame.width / 2 - frame.width * 0.475, dy:0)
            sectionInsets = UIEdgeInsets(top: 0, left: (itemSize.width / 16.667) / 2, bottom: 0, right: 0)
        default:
            itemSize = .zero
            initialOffsets = CGVector(dx: 0, dy: 0)
            sectionInsets = .zero
            itemSpacig = 0
        }
    }
}
