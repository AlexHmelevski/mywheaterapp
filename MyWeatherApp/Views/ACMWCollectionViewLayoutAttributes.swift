//
//  ACMWCollectionViewLayoutAttributes.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-07.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import UIKit

class ACMWCollectionViewLayoutAttributes: UICollectionViewLayoutAttributes {
    var maskFrame: CGRect = .zero
    var contentOffset: CGPoint = .zero

    override func isEqual(_ object: Any?) -> Bool {
        return super.isEqual(object) && self.maskFrame == (object as AnyObject).maskFrame
    }

    override func copy() -> Any {
        guard let nV = super.copy() as? ACMWCollectionViewLayoutAttributes else {
            fatalError("Not an ACMWCollectionViewLayoutAttributes type")
        }
        nV.maskFrame = self.maskFrame
        nV.contentOffset = self.contentOffset
        return nV
    }

}
