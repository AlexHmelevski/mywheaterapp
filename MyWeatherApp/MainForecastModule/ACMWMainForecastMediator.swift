//
//  ACMWMainForecastMediator.swift
//  MyWeatherApp
//
//  Created by Alex Hmelevski on 2017-06-28.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import Foundation

protocol ForecastInput: class {
    func didRequestForecast()
    func didPressAdd()
    func didPressSettings()
}

final class ACMWMainForecastMediator {
    fileprivate let interactor: ForecastInteractor
    fileprivate weak var delegate: ForecastModuleDelegate?
    init(interactor: ForecastInteractor, delegate: ForecastModuleDelegate?) {
        self.interactor = interactor
        self.delegate = delegate
    }
}

extension ACMWMainForecastMediator: ForecastInput {
    func didRequestForecast() {
        interactor.loadForecast()
    }

    func didPressAdd() {
        delegate?.didPressAdd()
    }

    func didPressSettings() {
        delegate?.didPressSettings()
    }
}
