//
//  ACMWDayHourlyConverter.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-20.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

class ACMWDayHourlyConverter: ACMWDataConverter {
	func convert(_ data: ACWundergroundModel, metricSytem: ACWundergroundMetricType) -> [ACMWCellData] {
		return data.hourly_forecast?.map({ transform(model: $0, withMetric: metricSytem) }) ?? []
	}

    private func transform(model: ACWundergroundHourlyModel, withMetric metric: ACWundergroundMetricType) -> ACMWCellData {
        var hour = ACMWCellDataConditions()
        hour.temperature = Int(model.temp.value.valueForType(metric))
        hour.hour = model.FCTTIME.hour
        hour.weatherIcon = model.icon?.typeForPeriod(ACMWPeriodOfTime(hour: hour.hour)) ?? ACMWForecastType.Clear
        return hour
    }
}
