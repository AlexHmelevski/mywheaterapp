//
//  ACMWWeekInfoDataConverter.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-20.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

class ACMWWeekInfoDataConverter: ACMWDataConverter {
	func convert(_ data: ACWundergroundModel, metricSytem: ACWundergroundMetricType) -> [ACMWCellData] {
		var result = [ACMWCellData]()
		guard let week = data.forecast?.simpleforecast?.forecastday else {
			return result
		}

        for i in stride(from: 0, to: week.count, by: 1) {
			var day = ACMWCellDataConditions()
			day.temperature = Int(week[i].high.value.valueForType(metricSytem))
			day.day = week[i].date.weekday_name
			day.feels_like = Int(week[i].low.value.valueForType(metricSytem))
			day.weatherIcon = week[i].icon?.Day ?? ACMWForecastType.Clear
			result.append(day)
		}

		return result
	}
}
