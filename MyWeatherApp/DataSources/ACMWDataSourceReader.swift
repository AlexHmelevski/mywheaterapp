//
//  ACMWCollectionViewDataSource.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-15.
//  Copyright © 2016 Alex Crow. All rights reserved.
//
import AHFuture
import Foundation
import SystemConfiguration
// @TODO: SRP violator. Split into classes through decoration. 1 object reads data, 2 do check for reachability, 3 read/save from cash

public class  ACMWDataSourceReader: ACMWDataReader {
    private let networkProxy: ACMWDataReader
    private let cashReader: ACMWDataReader
    private let cashSaver: ACMWDataSaver?
    private let reachability = ACMWReachability(domain: "api.wunderground.com")
    private var statusFlags = SCNetworkReachabilityFlags()
    private(set) var data: ACWundergroundModel?

    class func defaultManager(_ networkReader: ACMWDataReader) -> ACMWDataReader {
        let persistence = ACMWPersistence()
        return ACMWDataSourceReader(networkReader: networkReader, cashReader: persistence, cashSaver: persistence)
    }

    public init(networkReader: ACMWDataReader, cashReader: ACMWDataReader, cashSaver: ACMWDataSaver) {
        self.networkProxy = networkReader
        self.cashSaver = cashSaver
        self.cashReader = cashReader
    }

    public func read() -> AHFuture<ACWundergroundModel, ACMWError> {
        return self.networkProxy.read().retry(attempt: 5)
                                       .flatMap(transform: { (model) -> AHFuture<ACWundergroundModel, ACMWError> in

                                        return AHFuture(scope: { (completion) in
                                            self.cashSaver?.save(model)
                                            completion(.right(value: model))
                                        })
        })
    }
}
