//
//  ACMWCellDataConditions.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-18.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACMWCellDataConditions: ACMWCellData {
	var temperature: Int = 0
	var feels_like: Int = 0
	var day: String = ""
	var weatherIcon: ACMWForecastType = .Clear
	var humidiy: Int = 0
	var wind: String = ""
	var hour: Int = 0
}
