//
//  ACMWAnimatedView.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-12.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import UIKit

class ACMWAnimatedView: UIImageView {

    init() {
      super.init(frame: .zero)
        let array = ["AMWLoader1.png", "ACMWLoader2.png", "ACMWLoader3.png", "ACMWLoader4.png",
            "ACMWLoader5.png", "ACMWLoader6.png", "ACMWLoader7.png", "ACMWLoader8.png",
            "ACMWLoader9.png", "ACMWLoader10.png", "ACMWLoader11.png", "ACMWLoader12.png"]
        var images = [UIImage]()
        for name in array {
            if let image = UIImage(named: name) {
                images.append(image)
            }
        }
        self.animationImages = images
        self.animationDuration = 0.5

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
