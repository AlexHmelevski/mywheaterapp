//  ACMWForecastWeatherType.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-20.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

public struct ACMWForecastWeatherType: CustomFromStringConvertable {
    let Night: ACMWForecastType
    let Day: ACMWForecastType

    public init?(_ text: String) {
        var nightString: String
        var dayString: String
        if text.hasPrefix("nt_") {
            nightString = text

            dayString = text.substring(from: text.index(text.startIndex, offsetBy: 3))
        } else {
            dayString = text
            nightString = "nt_" + text
        }
        if let nType = ACMWForecastType(nightString),
            let dType = ACMWForecastType(dayString) {
            self.Day = dType
            self.Night = nType
        } else {
            self.Night = ACMWForecastType.Clear
            self.Day = ACMWForecastType.Night_Clear
        }
    }

    public func typeForPeriod(_ period: ACMWPeriodOfTime) -> ACMWForecastType {
        if period == .morning || period == .day {
            return self.Day
        } else {
            return self.Night
        }
    }
}
