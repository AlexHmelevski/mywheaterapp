//
//  IRouterModule.swift
//  MyWeatherApp
//
//  Created by Alex Hmelevski on 2017-06-23.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import Foundation

protocol IRouterModule {
    var viewController: UIViewController { get }
}
