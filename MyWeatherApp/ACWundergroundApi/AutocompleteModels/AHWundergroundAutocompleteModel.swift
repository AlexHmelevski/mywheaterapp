//
//  AHWundergroundAutocompleteModel.swift
//  MyWeatherApp
//
//  Created by Alex Hmelevski on 2017-06-23.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import Foundation
import AHJSONSerializer
import CoreLocation

/*
 "name": "Montreuil, France",
 "type": "city",
 "c": "FR",
 "zmw": "00000.31.07038",
 "tz": "Europe/Paris",
 "tzs": "CEST",
 "l": "/q/zmw:00000.31.07038",
 "ll": "48.770000 1.370000",
 "lat": "48.770000",
 "lon": "1.370000"
 */

struct AHWundergroundAutocompleteResultModel: JSONDecodable {
    var results: [AHWundergroundAutocompleteModel]

    init(decoder: AHJSONDecoder) {
        results = decoder["RESULTS"].value() ?? []
    }
}

struct AHWundergroundAutocompleteModel: JSONDecodable {
    let name: String
    let type: String
    let country: String
    let zmw: String
    let timeZone: String
    let tzs: String
    let queryString: String
    let coordinateString: String
    let coordinate: CLLocationCoordinate2D

    init(decoder: AHJSONDecoder) {
        name = decoder["name"].value() ?? ""
        type = decoder["type"].value() ?? ""
        country = decoder["c"].value() ?? ""
        zmw = decoder["zmw"].value() ?? ""
        timeZone = decoder["tz"].value() ?? ""
        tzs = decoder["tzs"].value() ?? ""
        queryString = decoder["l"].value() ?? ""
        coordinateString = decoder["ll"].value() ?? ""
        let latitude: Double = decoder["lat"].value() ?? 0.0
        let longitude: Double = decoder["type"].value() ?? 0.0
        coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
}
