//
//  ACMWCurrentDayDataConverter.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-20.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

class ACMWCurrentDayDataConverter: ACMWDataConverter {
	func convert(_ data: ACWundergroundModel, metricSytem: ACWundergroundMetricType) -> [ACMWCellData] {

        return data.hourly_forecast?.enumerated()
            .filter({ $0.offset % 6 == 0 })
            .flatMap({ transfrom(model: $0.element, withMetric: metricSytem) })
            ?? []
	}

    private func transfrom(model: ACWundergroundHourlyModel, withMetric metric: ACWundergroundMetricType) -> ACMWCellData {
        var day = ACMWCellDataConditions()
        day.temperature = Int(model.temp.value.valueForType(metric))
        day.day = ACMWPeriodOfTime(hour: model.FCTTIME.hour).description
        day.feels_like = Int(model.feelslike.value.valueForType(metric))
        day.humidiy = model.humidity
        day.weatherIcon = (model.icon?.typeForPeriod(ACMWPeriodOfTime(hour: model.FCTTIME.hour)))!
        day.wind = model.wdir.text
        return day
    }

}
