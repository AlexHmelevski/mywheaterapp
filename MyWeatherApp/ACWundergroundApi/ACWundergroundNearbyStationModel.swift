//
//  ACWundergroundNearbyStationModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-29.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundergroundNearbyStationModel: ACMappableProtocol {
    private(set) var airport: ACWundergroundStationArrayModel?
    private(set) var pws: ACWundergroundStationArrayModel?

    init(dict: ACDictionary) {
        self.airport <~ dict["airport"]
        self.pws <~ dict["pws"]
    }
}
