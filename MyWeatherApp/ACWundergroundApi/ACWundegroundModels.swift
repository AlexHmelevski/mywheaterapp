//
//  ACWundegroundModels.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-27.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

public struct ACWundergroundModel: ACMappableProtocol {
    var query_zone: String?
    private(set)  var response: ACWundegroundResponseModel?
    private(set)  public var almanac: ACWundegroundAlmanacModel?
    private(set)  public var moon_phase: ACWundergroundAstronomy?
    private(set)  var conditions: ACWundegroundConditionsModel?
    private(set)  var currenthurricane: ACWundegroundCurrentHurricaneModel?
    private(set)  var forecast: ACWundegroundForecastModel?
    private(set)  var geolookup: ACWundergroundDisplayLocation?
    private(set)  var history: ACWundegroundHistoryModel?
    private(set)  var hourly_forecast: [ACWundergroundHourlyModel]?
    private(set)  var trip: ACWundergroundPlannerModel?
    private(set)  var rawtide: ACWundergroundRawTideModel?
    private(set)  var tide: ACWundergroundTideModel?
    private(set)  var webcams: [ACWundergroundWebCamModel]?

    public init(dict: ACDictionary) {
        self.almanac <~ dict["almanac"]
        self.moon_phase <~ dict["moon_phase"]
        self.conditions <~ dict["current_observation"]
        self.currenthurricane <~ dict["currenthurricane"]
        self.forecast <~ dict["forecast"]
        self.geolookup <~ dict["location"]
        self.history <~ dict["history"]
        self.hourly_forecast <~ dict["hourly_forecast"]
        self.trip <~ dict["trip"]
        self.rawtide <~ dict["rawtide"]
        self.tide <~ dict["tide"]
        self.webcams <~ dict["webcams"]
        self.response <~ dict["response"]
        self.query_zone <~ dict["query_zone"]
    }
}
