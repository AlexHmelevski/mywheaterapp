//
//  ACWundergroundMetricTest.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-22.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import XCTest

class ACWundergroundMetricTest: XCTestCase {

    func testMetricParse() {
        let dict = ["en": 10, "met": 20]
        var metric: ACWundergroundMetric<Int> = ACWundergroundMetric(english: 10, metric: 20)

        XCTAssertEqual(metric.English, 10)
        XCTAssertEqual(metric.Metric, 20)

        metric = ACWundergroundMetric(english: "10", metric: "20")
        XCTAssertEqual(metric.English, 10)
        XCTAssertEqual(metric.Metric, 20)

        metric = ACWundergroundMetric(english: "k", metric: "k")
        XCTAssertEqual(metric.English, 0)
        XCTAssertEqual(metric.Metric, 0)

        metric = ACWundergroundMetric(english: dict["en"], metric: dict["met"])
        XCTAssertEqual(metric.English, 10)
        XCTAssertEqual(metric.Metric, 20)

    }

}
