//
//  ACMWMainForecastInteractor.swift
//  MyWeatherApp
//
//  Created by Alex Hmelevski on 2017-06-28.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import Foundation
import AHFuture

protocol ForecastInteractor {
    func loadForecast()
}

struct ForecastDataModel {
    let wundergroundModel: ACWundergroundModel
    let state: UIUpdateState
}

protocol ForecastService {
    var forecastFuture: AHFuture<ACWundergroundModel, ACMWError> { get }
}

final class ACMWForecastService: ForecastService {
    let proxy: AHWundergroundProxy

    init(with queryTypes: [AHWundegroundQueryType]) {
        proxy = AHWundergroundProxy(queryTypes: queryTypes)
    }

    var forecastFuture: AHFuture<ACWundergroundModel, ACMWError> {
        return proxy.read()
    }
}

final class ACMWMainForecastInteractor {

    fileprivate let service: ForecastService
    fileprivate let presenter: ForecastPresenter

    init(forecastService: ForecastService, presenter: ForecastPresenter) {
        service = forecastService
        self.presenter = presenter
    }
}

extension ACMWMainForecastInteractor: ForecastInteractor {
    func loadForecast() {
        service.forecastFuture.map(transform: { ($0, UIUpdateState.loaded) })
                              .recover(transform: { (ACWundergroundModel(dict: [:]), UIUpdateState.error($0)) })
                              .map(transform: ForecastDataModel.init)
                              .onSuccess { [weak self] (model) in
                                    self?.presenter.present(forecastModel: model)
                              }
                              .run(on: .global())
                              .observe(on: .main)
                              .execute()
    }
}
