//
//  ACWundegroundResponseModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-28.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundegroundResponseModel: ACMappableProtocol {

    private(set) var termOfServies: String?
    private(set) var version: String?
    private(set) var features: ACWundegroundFeaturesModel?
    private(set) var results: [ACWundergroundDisplayLocation] = []
    init(dict: ACDictionary) {
        self.termOfServies <~ dict["termsofService"]
        self.version <~ dict["version"]
        self.features <~ dict["features"]
        self.results <~ dict["results"]

    }
}
