//
//  ACMWDesignProxy.h
//  MyWeatherApp
//
//  Created by Alex Crow on 2015-08-14.
//  Copyright (c) 2015 Alex Crow. All rights reserved.
//
#import "GlobalTypes.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#define ACMWDAILY_FORECAST_FEEL_COLOR @"383838"

typedef enum {
    ACMWReusableVerticalSeparator,
    ACMWReusableHorizontalSeparator,
    
}ACMWReusableViewTypes;





@interface ACMWDesignProxy : NSObject
+ (instancetype)designProxy;

+(UIColor *)colorFromHexString:(NSString *)hexString;

-(void) label:(UILabel *) label withType:(ACMWDesignType) type;
-(void) subTitleLabel: (UILabel *) label withType:(ACMWDesignType) type;

-(NSAttributedString*) attributedDegreeStringFrom:(NSString*)string withType:(ACMWDesignType) type;
- (void)setDegreeLabel: (UILabel *)degreeLabel of: (ACMWDegreeLabelType)degreeLettertype andDesignType: (ACMWDesignType)type;
-(CGFloat) fontSizeForLabel:(ACMWDesignType) type;
-(CGFloat) fontSizeForTemperatureLabel:(ACMWDesignType) type;
-(UIFont*) fontForBodyLabel:(ACMWDesignType) type;

-(NSAttributedString*) smallCapsStringFrom:(NSString*) string withType:(ACMWDesignType) type;

-(void) addGradientToView:(UIView *)view;
-(void) setImage:(UIImageView*) imageView withType: (ACMWWeatherIconType) iconType;
-(void) setBackgroundImage: (UIImageView*) imageView withType: (ACMWWeatherIconType) iconType;

-(CGRect) frameToFitString:(NSString*)string withType:(ACMWDesignType) type;


-(UIImage*) imageViewForReusableView:(UICollectionReusableView*) reusableView;
-(UIImage*) bluredBackgroundImageFor:(UIImage*) originalImage;

- (CGFloat)separatorWidthFor: (UICollectionReusableView *)reusableView;
- (CGFloat)separatorWidthForKind: (NSString *)kind;

- (UIImage *) imageForType: (ACMWWeatherIconType)iconType;
@end
