//
//  ACMWDesignProxy.m
//  MyWeatherApp
//
//  Created by Alex Crow on 2015-08-14.
//  Copyright (c) 2015 Alex Crow. All rights reserved.
//

#import "ACMWDesignProxy.h"
#import <CoreText/CoreText.h>
//#import "NSObject+Kind.h"
#import "UIImageEffects.h"

NSString * const designStylesDescription [] = {
     [ACMWShortForecast] = @"shortForecastView",
     [ACMWFullForecast]  = @"fullForecastView",
     [ACMWNavigationBarT] = @"navigationBar",
     [ACMWForecastHeader] = @"forecastHeader",
     [ACMWForecastFooter] = @"forecastFooter",
     [ACMWDailyForecast]  = @"dailyForecast",
     
};

NSString * const degreeString [] = {
    [degreeLabelTypeCelsius] = @"°C",
    [degreeLabelTypeFahrenheit] = @"°F",
};

NSString * const weatherIconStrings [] = {
        [ACMWWeatherIconTypeDayClear] = @"DayClear",
        [ACMWWeatherIconTypeCloudy] = @"Cloudy",
        [ACMWWeatherIconTypeDayPartlyCloudy] = @"DayPartlyCloudy",
        [ACMWWeatherIconTypeDayRainy] = @"DayRainy",
        [ACMWWeatherIconTypeHeavyClouds] = @"HeavyClouds",
        [ACMWWeatherIconTypeHeavyRain] = @"HeavyRain",
        [ACMWWeatherIconTypeHeavySnow] = @"HeavySnow",
        [ACMWWeatherIconTypeNighRainy] = @"NightRainy",
        [ACMWWeatherIconTypeNightClear] = @"NightClear",
        [ACMWWeatherIconTypeNightPartlyCloudy] = @"NightPartlyCloudy",
        [ACMWWeatherIconTypeThunderStrom] = @"Thunder"

};


NSString* const reusableViewTypes [] = {
    [ACMWReusableVerticalSeparator] = @"verticalSeparator",
    
};

NSString* const  ACMWBodyFontName = @"bodyFontName";
NSString* const  ACMWBodyFontSize = @"bodyFontSize";
NSString* const  ACMWBodyTextColor = @"bodyTextColor";
NSString* const  ACMWBodyShadowColor = @"bodyShadowColor";
NSString* const  ACMWBodyShadowOffsetX = @"bodyShadowOffsetX";
NSString* const  ACMWBodyShadowOffsetY = @"bodyShadowOffsetY";

NSString* const  ACMWTemperatureTitleFontName = @"temperatureTitleFont";
NSString* const  ACMWTemperatureTitleFontSize = @"temperatureTitleFontSize";
NSString* const  ACMWTemperatureTitleFontColor = @"temperatureTitleFontColor";


NSString* const  ACMWSubTitleFontName = @"subTitleFontName";
NSString* const  ACMWSubTitleFontSize = @"subTitleFontSize";
NSString* const  ACMWSubTitleTextColor = @"subTitleTextColor";

NSString* const  ACMWSmallCapsTextFont = @"smallCapsFont";
NSString* const  ACMWWeatherIconSet = @"weatherIcons";

NSString* const  ACMWBackgorundWeatherSet = @"mainBackground";

NSString* const  ACMWReusableViewImages = @"reusableViewImages";

@interface ACMWDesignProxy ()

@property (nonatomic) NSDictionary *designDefaults;

@end

static NSDictionary * _defaults;

@implementation ACMWDesignProxy


+ (instancetype)designProxy {
    static dispatch_once_t once;
    static id _designProxy = nil;
    dispatch_once(&once, ^{
        _designProxy = [[self alloc] init];
    
    });
    
    return _designProxy;
}

//TODO: understand how it works and what scanner does
+ (UIColor* )colorFromHexString:(NSString *)aHexString {
    unsigned rgbValue = 0;
    NSScanner* scanner = [NSScanner scannerWithString:aHexString];
    
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16) / 255.0 green:((rgbValue & 0xFF00) >> 8) / 255.0 blue:(rgbValue & 0xFF) / 255.0 alpha:1.0];
    
}

- (NSDictionary*)designDefaults {
    if (!_designDefaults) {
        NSString *path = [[NSBundle bundleForClass:self.class] pathForResource:ACMWDesignDefaultsFileName ofType:@"json"];
        NSData *data = [NSData dataWithContentsOfFile:path];
        RNAssert(data !=nil, @"Could not read config file from main bundle with name %@.json",ACMWDesignDefaultsFileName);
        NSError *error;
        _designDefaults = [NSDictionary dictionaryWithDictionary:[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error]];
        RNAssert([_designDefaults count]!=0, @"Error Serializing %@.json\n%@",ACMWDesignDefaultsFileName,error);
    }
    return  _designDefaults;
    
}

- (void)label: (UILabel *)label withType: (ACMWDesignType) type
{
    
    NSDictionary *currentDesign = [self.designDefaults valueForKey:designStylesDescription[type]];

    label.textColor = [ACMWDesignProxy colorFromHexString:[currentDesign valueForKey:ACMWBodyTextColor]];

    NSString *fontName = [currentDesign valueForKey:ACMWBodyFontName];
    CGFloat fontSize = [[currentDesign valueForKey:ACMWBodyFontSize]floatValue];
    label.font = [UIFont fontWithName:fontName size:fontSize];
    [label setTextAlignment:NSTextAlignmentCenter];
    
    
    label.shadowColor = [ACMWDesignProxy colorFromHexString:[currentDesign valueForKey:ACMWBodyShadowColor]];
    CGSize shadowOffset = CGSizeMake([[currentDesign valueForKey:ACMWBodyShadowOffsetX] floatValue], [[currentDesign valueForKey:ACMWBodyShadowOffsetY] floatValue]);
    label.numberOfLines=0;
    label.shadowOffset = shadowOffset;

}

-(void) subTitleLabel: (UILabel *) label withType:(ACMWDesignType) type
{

    NSDictionary *currentDesign = [self.designDefaults valueForKey:designStylesDescription[type]];
    
    label.textColor = [ACMWDesignProxy colorFromHexString:[currentDesign valueForKey:ACMWSubTitleTextColor]];
    
    NSString *fontName = [currentDesign valueForKey:ACMWSubTitleFontName];
    CGFloat fontSize = [[currentDesign valueForKey:ACMWSubTitleFontSize]floatValue];
    label.font = [UIFont fontWithName:fontName size:fontSize];
    [label setTextAlignment:NSTextAlignmentCenter];
    label.shadowColor = [ACMWDesignProxy colorFromHexString:[currentDesign valueForKey:ACMWBodyShadowColor]];
    CGSize shadowOffset = CGSizeMake([[currentDesign valueForKey:ACMWBodyShadowOffsetX] floatValue], [[currentDesign valueForKey:ACMWBodyShadowOffsetY] floatValue]);
    
    label.shadowOffset = shadowOffset;

}

- (UIFont*)fontForBodyLabel:(ACMWDesignType) type {
     NSDictionary *currentDesign = [self.designDefaults valueForKey:designStylesDescription[type]];
    return  [UIFont fontWithName:[currentDesign valueForKey:ACMWSmallCapsTextFont] size:[[currentDesign valueForKey:ACMWBodyFontSize]floatValue]];
}

- (NSAttributedString*)smallCapsStringFrom:(NSString*) string withType:(ACMWDesignType) type
{
    NSMutableAttributedString *newString = [[NSMutableAttributedString alloc]initWithString:string];
    NSDictionary *currentDesign = [[self designDefaults] valueForKey:designStylesDescription[type]];
    NSString *smallCapsFontName = [currentDesign valueForKey:ACMWBodyFontName];
    CGFloat fontSize = [[currentDesign valueForKey:ACMWBodyFontSize]floatValue]*0.7;
    UIFont *smallFont = [UIFont fontWithName:smallCapsFontName size:fontSize];

    [newString beginEditing];
    if (type == ACMWForecastFooter) {
         [newString addAttribute:NSFontAttributeName value:smallFont range:NSMakeRange(newString.length-1, 1)];
    } else {
       [newString addAttribute:NSFontAttributeName value:smallFont range:NSMakeRange(1, newString.length-1)];
    }

    [newString endEditing];
    return newString;
}


- (void)setImage:(UIImageView*)imageView withType: (ACMWWeatherIconType)iconType{
    
    //TODO: implement image check
    NSDictionary *currentDesign = [[self designDefaults] valueForKey:ACMWWeatherIconSet];
    
    [imageView setImage:[UIImage imageNamed:[currentDesign valueForKey:weatherIconStrings[iconType]]]];
  
}

- (UIImage *) imageForType: (ACMWWeatherIconType)iconType {
    NSDictionary *currentDesign = [[self designDefaults] valueForKey:ACMWWeatherIconSet];
    
    return [UIImage imageNamed:[currentDesign valueForKey:weatherIconStrings[iconType]]];
}

-(void) setBackgroundImage: (UIImageView*) imageView withType: (ACMWWeatherIconType) iconType
{
    NSDictionary *currentDesign = [[self designDefaults] valueForKey:ACMWBackgorundWeatherSet];
    [imageView setImage:[UIImage imageNamed:[currentDesign valueForKey:weatherIconStrings[iconType]]]];
}



-(CGRect) frameToFitString:(NSString*)string withType:(ACMWDesignType) type {
    
    NSDictionary *attributes = @{NSFontAttributeName: [self fontForBodyLabel:type]};
    
    CGRect rect = [string boundingRectWithSize:CGSizeMake(0, CGFLOAT_MAX)
                                       options:NSStringDrawingUsesLineFragmentOrigin
                                    attributes:attributes
                                       context:nil];
    return rect;
}

- (CGFloat)separatorWidthFor: (UICollectionReusableView *)reusableView {
    NSDictionary *currentDesign = [[self designDefaults] valueForKey:ACMWReusableViewImages];
    CGFloat linewidth =[[currentDesign valueForKey:[[reusableView class] kind]] floatValue];
    return linewidth;
}

- (CGFloat)separatorWidthForKind: (NSString *)kind {
    NSDictionary *currentDesign = [[self designDefaults] valueForKey: ACMWReusableViewImages];
    CGFloat linewidth =[[currentDesign valueForKey: kind] floatValue];
    return linewidth;
}

-(UIImage*) bluredBackgroundImageFor:(UIImage*) originalImage
{
   return  [UIImageEffects imageByApplyingBlurToImage:originalImage withRadius:60 tintColor:[UIColor clearColor] saturationDeltaFactor: 1 maskImage:nil];
}


@end

