//
//  ACWundegroundCurrentHurricaneModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-29.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundegroundCurrentHurricaneModel: ACMappableProtocol {
    private(set) var current: ACWundegroundHurricaneForecastModel?
    private(set) var forecast: [ACWundegroundHurricaneForecastModel]?
    private(set) var track: ACWundegroundHurricaneForecastModel?
    private(set) var stormInfo: ACWundegroundStormInfoModel?
    init(dict: ACDictionary) {
        self.current <~ dict["Current"]
        self.forecast <~ dict["forecast"]
        self.track <~ dict["track"]
        self.stormInfo <~ dict["stormInfo"]
    }
}
