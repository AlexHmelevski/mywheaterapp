//
//  ACWundegroundConditionsModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-28.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundegroundConditionsModel: ACMappableProtocol {
    private(set) var UV: Int = 0
    private(set) var conds: Double = 0
    private(set) var date = ACWundegroundTimeModel(dict: ["": "" as AnyObject])
    private(set) var dewpoint: ACWundergroundMetric<Double>
    private(set) var dewpoint_string = String()
    private(set) var display_location = ACWundergroundDisplayLocation(dict: ["": "" as AnyObject])
    private(set) var fog: Double = 0
    private(set) var hail: Double = 0
    private(set) var rain: Double = 0
    private(set) var snow: Double = 0
    private(set) var thunder: Double = 0
    private(set) var tornado: Double = 0
    private(set) var feelslike: ACWundergroundMetric<Double>
    private(set) var feelslike_string = String()
    private(set) var forecast_url = String()
    private(set) var heat_index: ACWundergroundMetric<Double>
    private(set) var heat_index_string = String()
    private(set) var history_url = String()
    private(set) var icon: ACMWForecastWeatherType?
    private(set) var image: ACWundegroundImageModel?
    private(set) var local_epoch: Int = 0
    private(set) var local_time_rfc822 = String()
    private(set) var local_tz_long = String()
    private(set) var local_tz_offset = String()
    private(set) var local_tz_short = String()
    private(set) var nowcast = String()
    private(set) var observation_location = ACWundergroundDisplayLocation(dict: ["": "" as AnyObject])

    private(set) var observation_time = String()
    private(set) var observation_time_rfc822 = String()
    private(set) var precip_1hr: ACWundergroundMetric<Double>
    private(set) var precip_1hr_string = String()
    private(set) var precip_today: ACWundergroundMetric<Double>
    private(set) var precip_today_string = String()

    private(set) var pressure: ACWundergroundMetric<Double>
    private(set) var pressure_trend = String()
    private(set) var relative_humidity = String()
    private(set) var solarradiation: Int?
    private(set) var station_id = String()
    private(set) var temp: ACWundergroundMetric<Double>
    private(set) var temperature_string = String()

    private(set) var visibility: ACWundergroundMetric<Double>
    private(set) var weather = ACMWForecastWeatherType("")

    private(set) var wind_degrees: Int = 0
    private(set) var wind_dir = String()
    private(set) var wind_gust: ACWundergroundMetric<Double>

    private(set) var wind: ACWundergroundMetric<Double>
    private(set) var wind_string = String()
    private(set) var windchill: ACWundergroundMetric<Double>

    private(set) var windchill_string = String()

    init(dict: ACDictionary) {
        self.UV <~ dict["UV"]
        self.conds <~ dict["conds"]
        self.date <~ dict["date"]

        self.dewpoint = ACWundergroundMetric(english: (dict["dewpoint_f"] ?? dict["dewpti"]),
                                              metric:(dict["dewpoint_c"] ?? dict["dewptm"]) )
        self.dewpoint_string <~ dict["dewpoint_string"]
        self.display_location <~ dict["display_location"]
        self.fog <~ dict["fog"]
        self.hail <~ dict["hail"]
        self.rain <~ dict["rain"]
        self.snow <~ dict["snow"]
        self.thunder <~ dict["thunder"]
        self.tornado <~ dict["tornado"]
        self.feelslike = ACWundergroundMetric(english: dict["feelslike_f"], metric: dict["feelslike_c"])
        self.feelslike_string <~ dict["feelslike_string"]
        self.forecast_url <~ dict["forecast_url"]
        self.heat_index = ACWundergroundMetric(english: (dict["heat_index_f"] ?? dict["heatindexi"]),
                                                metric: (dict["heat_index_c"] ?? dict["heatindexm"]))
        self.heat_index_string <~ dict["heat_index_string"]
        self.history_url <~ dict["heat_index_string"]
        self.icon <~ dict["icon"]
        self.image <~ dict["image"]
        self.local_epoch <~ dict["local_epoch"]
        self.local_tz_long <~ dict["local_tz_long"]
        self.local_tz_offset <~ dict["local_tz_offset"]
        self.local_time_rfc822 <~ dict["local_time_rfc822"]
        self.local_tz_short <~ dict["local_tz_short"]
        self.nowcast <~ dict["nowcast"]
        self.observation_location <~ dict["observation_location"]
        self.observation_time <~ dict["observation_time"]
        self.observation_time_rfc822 <~ dict["observation_time_rfc822"]
        self.precip_1hr = ACWundergroundMetric(english: dict["precip_1hr_in"],
                                                metric: dict["precip_1hr_metric"])
        self.precip_1hr_string <~ dict["precip_1hr_string"]
        self.precip_today = ACWundergroundMetric(english: (dict["precip_today_in"] ?? dict["precipi"]),
                                                  metric: (dict["precip_today_metric"] ?? dict["precipm"]))
        self.precip_today_string <~ dict["precip_today_string"]
        self.pressure = ACWundergroundMetric(english: (dict["pressure_in"] ?? dict["pressurei"]), metric: (dict["pressure_mb"] ?? dict["pressurem"]))
        self.pressure_trend <~ dict["pressure_trend"]
        self.relative_humidity <~ dict["relative_humidity"]
        self.solarradiation <~ dict["solarradiation"]
        self.station_id <~ dict["station_id"]
        self.temp = ACWundergroundMetric(english: (dict["temp_f"] ?? dict["tempi"]), metric: (dict["temp_c"] ?? dict["tempm"]))
        self.temperature_string <~ dict["temperature_string"]
        self.visibility = ACWundergroundMetric(english: (dict["visibility_mi"] ?? dict["visi"]), metric: (dict["visibility_km"] ?? dict["vism"]))
        self.weather <~ dict["weather"]
        self.wind_degrees <~ (dict["wind_degrees"] ?? dict["wdird"])
        self.wind_dir <~ (dict["wind_dir"] ?? dict["wdire"])

        self.wind_gust = ACWundergroundMetric(english: (dict["wind_gust_mph"] ?? dict["wgusti"]), metric: (dict["wind_gust_kph"] ?? dict["wgustm"]))

        self.wind = ACWundergroundMetric(english: (dict["wind_mph"] ?? dict["wspdi"]), metric: (dict["wind_kph"] ?? dict["wspdm"]))
        self.wind_string <~ dict["wind_string"]

        self.windchill = ACWundergroundMetric(english: (dict["windchill_f"] ?? dict["windchilli"]),
                                               metric: (dict["windchill_c"] ?? dict["windchillm"]))
        self.windchill_string <~ dict["windchill_string"]
    }
}
