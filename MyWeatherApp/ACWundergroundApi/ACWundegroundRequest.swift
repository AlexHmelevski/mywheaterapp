//
//  ACWundegroundRequest.swift
//  MyWeatherApp
//
//  Created by Alex Hmelevski on 2017-02-13.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import Foundation
import AHNetwork

enum AHNetworkRequestType {
    case weather(features: [AHWundegroundFeatureType],
                 settings: [ACWundegroundSettingType],
                 query: AHWundegroundQuery)
    case autocomplete(text: String)

    var request: IRequest {
        switch self {
            case let .weather(features, settings, query):
                return GeneralProjectConfig.isTestTarget ? AHWundegroundMockRequest() : AHWundegroundRequest(with: features, settings: settings, query: query)
            case let .autocomplete(query): return GeneralProjectConfig.isTestTarget ? AutocompleteMockRequest() : AutocompleteRequest(text: query)
        }
    }
}

struct AutocompleteRequest: IRequest {
    var baseURL: String { return "autocomplete.wunderground.com" }
    var path: String { return "/aq" }
    var headers: [String : String] { return [:] }
    var body: Data? { return nil }
    let parameters: [String : String]
    var method: AHMethod { return .get }
    var taskType: AHTaskType { return .request }
    var scheme: AHScheme { return .https }
    var port: Int? { return nil }

    init(text: String) {
         parameters = ["query": text]
    }

}

struct AutocompleteMockRequest: IRequest {
    var baseURL: String { return "localhost" }
    var path: String { return "/autocomplete/a" }
    var headers: [String : String] { return [:] }
    var body: Data? { return nil }
    var parameters: [String : String] { return [:] }
    var method: AHMethod { return .get }
    var taskType: AHTaskType { return .request }
    var scheme: AHScheme { return .http }
    var port: Int? { return 8080 }
}

struct AHWundegroundRequest: IRequest {
    private let apiKey: String = "eab569449bcc3ea8"
    var baseURL: String { return  "api.wunderground.com" }
    let path: String
    var headers: [String : String] { return [:] }
    var body: Data? { return nil }
    var parameters: [String : String] { return [:] }
    var method: AHMethod { return .get }
    var taskType: AHTaskType { return .request }
    var scheme: AHScheme { return .https }
    var port: Int? { return nil }

    init(with features: [AHWundegroundFeatureType],
         settings: [ACWundegroundSettingType],
         query: AHWundegroundQuery) {
        path = "/api/" + apiKey + features.map({ $0.path }).joined() + settings.map({ $0.path }).joined() + query.path
    }
}

struct AHWundegroundMockRequest: IRequest {

    var baseURL: String { return  "localhost" }
    var path: String { return "/api/1" }
    var headers: [String : String] { return [:] }
    var body: Data? { return nil }
    var parameters: [String : String] { return [:] }
    var method: AHMethod { return .get }
    var taskType: AHTaskType { return .request }
    var scheme: AHScheme { return .http }
    var port: Int? { return 8080 }

}
