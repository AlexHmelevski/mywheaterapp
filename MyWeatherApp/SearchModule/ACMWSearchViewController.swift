//
//  ALSearchViewController.swift
//  MyWeatherApp
//
//  Created by Alex Hmelevski on 2017-06-23.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import Foundation

protocol ISearchPresentationOutput {
    func update(with model: ACMWSearchResultViewModel)
}

final class SearchCell: UITableViewCell {

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = .clear
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

final class ACMWSearchViewController: UIViewController {
    weak var input: ISearchInput?
    private let searchBar: UISearchBar
    fileprivate let tableView: UITableView
    fileprivate var cellResuls: [String] = []

    init() {
        searchBar = UISearchBar(frame: .zero)
        searchBar.accessibilityLabel = "AutocompleteSearchBar"
        tableView = UITableView(frame: .zero, style: .grouped)
        tableView.register(SearchCell.self, forCellReuseIdentifier: "SearchCell")

        tableView.backgroundColor = .clear

        super.init(nibName: nil, bundle: nil)

        view.backgroundColor = .clear
        view.addSubviews(searchBar)
        view.addSubview(tableView)

        searchBar.topAnchor.constraint(equalTo: view.topAnchor, constant:20).isActive = true
        searchBar.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        searchBar.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        searchBar.heightAnchor.constraint(equalToConstant: 30).isActive = true

        tableView.topAnchor.constraint(equalTo: searchBar.bottomAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.delegate = self
        tableView.dataSource = self
        view.setTranslatesAutoresizingMaskInSubviews(false)

        searchBar.showsCancelButton = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
    }
}

extension ACMWSearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         input?.didSelectResult(at: indexPath.row)
    }
}

extension ACMWSearchViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellResuls.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath)
        cell.textLabel?.text = cellResuls.enumerated()
                                         .filter({ $0.offset == indexPath.row })
                                         .map({ $0.element })
                                         .first
        return cell
    }
}

extension ACMWSearchViewController: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.characters.count >= 3 {
            input?.didType(text: searchText)
        }
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        input?.didClose()
    }
}

extension ACMWSearchViewController: ISearchPresentationOutput {
    func update(with model: ACMWSearchResultViewModel) {
        cellResuls = model.names
        tableView.reloadData()
    }
}
