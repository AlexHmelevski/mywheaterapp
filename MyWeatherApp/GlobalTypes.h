//
//  GlobalTypes.h
//  MyWeatherApp
//
//  Created by Alex Crow on 2015-08-14.
//  Copyright (c) 2015 Alex Crow. All rights reserved.
//

#ifndef MyWeatherApp_GlobalTypes_h
#define MyWeatherApp_GlobalTypes_h

#define ACMWDesignDefaultsFileName @"ACMWDesignDefaults"

#define RNLogBug NSLog
#define RNAssert(condition, desc, ...) \
if (!(condition)) { \
RNLogBug((desc), ## __VA_ARGS__); \
NSAssert((condition), (desc), ## __VA_ARGS__); \
}
#define RNCAssert(condition, desc) \
if (!(condition)) { \
RNLogBug((desc), ## __VA_ARGS__); \
NSCAssert((condition), (desc), ## __VA_ARGS__); \
}




typedef enum {
    degreeLabelTypeCelsius,
    degreeLabelTypeFahrenheit,
}ACMWDegreeLabelType;


typedef enum {
    ACMWWeatherIconTypeDayClear,
    ACMWWeatherIconTypeNightClear,
    ACMWWeatherIconTypeCloudy,
    ACMWWeatherIconTypeHeavyClouds,
    ACMWWeatherIconTypeHeavyRain,
    ACMWWeatherIconTypeHeavySnow,
    ACMWWeatherIconTypeDayPartlyCloudy,
    ACMWWeatherIconTypeNightPartlyCloudy,
    ACMWWeatherIconTypeDayRainy,
    ACMWWeatherIconTypeNighRainy,
    ACMWWeatherIconTypeThunderStrom
    
}ACMWWeatherIconType;


typedef enum {
    ACMWShortForecast,
    ACMWFullForecast,
    ACMWNavigationBarT,
    ACMWForecastHeader,
    ACMWForecastFooter,
    ACMWDailyForecast,

    
}ACMWDesignType;  



//typedef enum {
//    ACMWReusableVerticalSeparator,
//    
//}ACMWReusableViewTypes;
//typedef enum {
//    degreeLabelTypeCelsius,
//     degreeLabelTypeFahrenheit,
//}degreeLabelType;



#endif



