//
//  ACWundegroundStormInfoModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-29.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundegroundStormInfoModel: ACMappableProtocol {
    private(set) var requesturl: String?
    private(set) var stormName: String?
    private(set) var stormName_Nice: String?
    private(set) var stormNumber: String?
    private(set) var wuiurl: String?

    init(dict: ACDictionary) {
        self.requesturl <~ dict["requesturl"]
        self.stormName <~ dict["stormName"]
        self.stormName_Nice <~ dict["stormName_Nice"]
        self.stormNumber <~ dict["stormNumber"]
        self.wuiurl <~ dict["wuiurl"]
    }
}
