//
//  ACWundegroundManager.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-25.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation
import SystemConfiguration
import AHFuture
import AHNetwork

public class AHWundergroundProxy: ACMWDataReader {
    var provider = AHNetworkRequester()
    private let requestType: AHNetworkRequestType

    init(with features: [AHWundegroundFeatureType] = [.geolookup,
                                                      .conditions,
                                                      .forecast10day,
                                                      .hourly],
         querySettings settings: [ACWundegroundSettingType] = [.bestForecast(true), .pws(false)],
         queryTypes types: [AHWundegroundQueryType]) {
        self.requestType = .weather(features: features,
                                    settings: settings,
                                    query: AHWundegroundQuery(with: types))
    }

    public func read() -> AHFuture<ACWundergroundModel, ACMWError> {
        return provider.getData(for: requestType).filter(predicate: resultIsEmpty, error: .tooManyResults)
                                                 .transform(transform: { $0 },
                                                            transformError: { .networkError(error: $0) })

    }

    private func resultIsEmpty(model: ACWundergroundModel) -> Bool {
        return model.response?.results.isEmpty ?? false
    }

    func mapErrors(error: ACMWNetworkError) -> ACMWError {
        return .networkError(error: .unknown)
    }

}
