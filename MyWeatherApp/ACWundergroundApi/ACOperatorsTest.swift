//
//  ACOperatorsTest.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-09.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

// swiftlint:disable force_cast
import Foundation
import XCTest

//import MyWeatherApp
//SHOULD BE DONE WHEN XCTEST SUPPORTS GENERICS

class ACOperatorsTest: XCTestCase {

    class OperatorTest: ACMappableProtocol {
        var anInt: Int?
        var anIntUnw: Int = 0
        var operatorSub: OperatorTestSub?
        var operatorSubUnw: OperatorTestSub = OperatorTestSub(dict: ["": "" as AnyObject])

        required init(dict: ACDictionary) {
            self.anInt <~ dict["anInt"]
            self.anIntUnw <~ dict["anInt"]
            self.operatorSub <~ dict["operator"]
            self.operatorSubUnw <~ dict["operator"]

        }
    }

    class OperatorTestSub: ACMappableProtocol {
        var anInt: Int?
        var anIntUnw: Int = 0

        required init(dict: ACDictionary) {
            self.anInt <~ dict["anInt"]
            self.anIntUnw <~ dict["anInt"]
        }
    }

    func testInt() {
        let testDict = ["Int": 100, "anIntFromString": "100"] as [String : Any]
        var anInt: Int?
        var anIntUnw: Int = 0

        anInt <~ testDict["Int"]
        XCTAssertEqual(anInt!, testDict["Int"] as? Int)

        anInt <~ testDict["anIntFromString"]
        XCTAssertEqual(anInt!, 100)

        anIntUnw <~ (testDict["Int"] as! Int)
        XCTAssertEqual(anIntUnw, testDict["Int"] as? Int)

        anIntUnw <~ (testDict["anIntFromString"] )
        XCTAssertEqual(anIntUnw, 100)

        anInt <~ 100
        XCTAssertEqual(anInt!, 100)

        anInt <~ "100"
        XCTAssertEqual(anInt!, 100)

        anIntUnw <~ 100
        XCTAssertEqual(anIntUnw, 100)

        anIntUnw <~ "100"
        XCTAssertEqual(anIntUnw, 100)

    }

    func testFloat() {
        let testDict  = ["aFloat": 100.123_456 as Float, "aFloatFromString": "100.123456"] as [String : Any]
        var aFloat: Float?
        var aFloatUnw: Float = 0.0
        let testValue: Float = 100.1
        let testString = "100.1"

        aFloat <~ testDict["aFloat"]
        XCTAssertEqual(aFloat, testDict["aFloat"] as? Float)
        aFloat <~ testDict["aFloatFromString"]
        XCTAssertEqual(aFloat, testDict["aFloat"] as? Float)

        aFloatUnw <~ (testDict["aFloat"])
        XCTAssertEqual(aFloatUnw, testDict["aFloat"] as? Float)
        aFloatUnw <~ (testDict["aFloatFromString"] )
        XCTAssertEqual(aFloatUnw, testDict["aFloat"] as? Float)

        aFloat <~ testValue
        XCTAssertEqual(aFloat!, testValue)

        aFloat <~ testString
        XCTAssertEqual(aFloat!, testValue)

        aFloatUnw <~ testValue
        XCTAssertEqual(aFloatUnw, testValue)

        aFloatUnw <~ testString
        XCTAssertEqual(aFloatUnw, testValue)

    }

    func testDouble() {
        let testDict = ["aDouble": Double(100.123_45), "aDoubleFromString": "100.12345"] as [String : Any]

        var aDouble: Double?
        var aDoubleUnw: Double = 0.0

        aDouble <~ testDict["aDouble"]
        XCTAssertEqual(aDouble, testDict["aDouble"] as? Double)
        aDouble <~ testDict["aDoubleFromString"]
        XCTAssertEqual(aDouble, testDict["aDouble"] as? Double)

        aDoubleUnw <~ (testDict["aDouble"] as! Double)
        XCTAssertEqual(aDoubleUnw, testDict["aDouble"] as? Double)
        aDoubleUnw <~ (testDict["aDoubleFromString"])
        XCTAssertEqual(aDoubleUnw, testDict["aDouble"] as? Double)
    }

    func testBool() {
        let testDict  = ["aBool": true, "aBoolFromString": "f"] as [String : Any]

        var aBool: Bool?
        var aBoolUnw: Bool = true

        aBool <~ testDict["aBool"]
        XCTAssertEqual(aBool, testDict["aBool"] as? Bool)
        aBool <~ testDict["aBoolFromString"]
        XCTAssertEqual(aBool, false)

        aBoolUnw <~ (testDict["aBool"] as! Bool)
        XCTAssertEqual(aBoolUnw, testDict["aBool"] as? Bool)
        aBoolUnw <~ (testDict["aBoolFromString"])
        XCTAssertEqual(aBoolUnw, false)
    }

    func testCGFloat() {
        let testDict = ["aCGFloat": CGFloat(100.2), "aCGFloatFromString": "100.2"] as [String : Any]
        var aFloat: CGFloat?
        var aFloatUnw: CGFloat = 0.0

        aFloat <~ testDict["aCGFloat"]
        XCTAssertEqual(aFloat, testDict["aCGFloat"] as? CGFloat)
        aFloat <~ testDict["aCGFloatFromString"]
        XCTAssertEqual(aFloat, testDict["aCGFloat"] as? CGFloat)

        aFloatUnw <~ (testDict["aCGFloat"] as! CGFloat)
        XCTAssertEqual(aFloatUnw, testDict["aCGFloat"] as? CGFloat)
        aFloatUnw <~ (testDict["aCGFloatFromString"])
        XCTAssertEqual(aFloatUnw, testDict["aCGFloat"] as? CGFloat)
    }

    func testString() {
        let testDict: ACDictionary  = ["aString": "aString" as AnyObject]
        var aString: String?
        var aStringUnw: String = String()

        aString <~ testDict["aString"]
        XCTAssertEqual(aString, testDict["aString"] as? String)
        aStringUnw <~ testDict["aString"]
        XCTAssertEqual(aStringUnw, testDict["aString"] as? String)

    }

    func testStringCollection() {
        let testDict: ACDictionary  = ["aStringCollection": [ "aString", "1", "2"] as AnyObject]

        var aString: [String]?
        var aStringUnw  = [String]()

        aString <~ testDict["aStringCollection"]
        XCTAssertEqual(aString!, (testDict["aStringCollection"] as? [String])!)
        aStringUnw <~ testDict["aStringCollection"]
        XCTAssertEqual(aStringUnw, (testDict["aStringCollection"] as? [String])!)

    }

    func testCollectionOfBools() {
        let testDict: ACDictionary = ["aBoolArray": [true, false, false, true] as AnyObject,
                                      "aBoolArrayFromStrings": ["t", "f", "f", "t"] as AnyObject]

        var aBoolArray: [Bool]?
        var aBoolUnwrapped  = [Bool]()

        aBoolArray <~ testDict["aBoolArray"]
        XCTAssertEqual(aBoolArray!, (testDict["aBoolArray"] as? [Bool])!)

        aBoolUnwrapped <~ testDict["aBoolArray"]
        XCTAssertEqual(aBoolUnwrapped, (testDict["aBoolArray"] as? [Bool])!)

        aBoolArray <~ testDict["aBoolArrayFromStrings"]
        XCTAssertEqual(aBoolArray!, [true, false, false, true])

        aBoolUnwrapped <~ testDict["aBoolArrayFromStrings"]
        XCTAssertEqual(aBoolUnwrapped, [true, false, false, true])

        aBoolArray <~ [true, false, false, true]
        XCTAssertEqual(aBoolArray!, [true, false, false, true])

        aBoolUnwrapped <~ [true, false, false, true]
        XCTAssertEqual(aBoolUnwrapped, [true, false, false, true])

    }

    func testCollectionOfInt() {
        let testDict: ACDictionary  = ["anIntArray": [10, 2, 25, 103] as AnyObject, "anIntArrayFromTheStrings": ["10", "2", "25", "103"] as AnyObject]

        var anIntArray: [Int]?

        var anIntArrayUnw = [Int]()

        anIntArray <~ testDict["anIntArray"]
        XCTAssertEqual(anIntArray!, (testDict["anIntArray"] as? [Int])!)

        anIntArrayUnw <~ testDict["anIntArray"]
        XCTAssertEqual(anIntArrayUnw, (testDict["anIntArray"] as? [Int])!)

        anIntArray <~ testDict["anIntArrayFromTheStrings"]
        XCTAssertEqual(anIntArray!, (testDict["anIntArray"] as? [Int])!)

        anIntArrayUnw <~ testDict["anIntArrayFromTheStrings"]
        XCTAssertEqual(anIntArrayUnw, (testDict["anIntArray"] as? [Int])!)

        anIntArray <~ [10, 2, 25, 103]
        XCTAssertEqual(anIntArray!, [10, 2, 25, 103])

        anIntArrayUnw <~ [10, 2, 25, 103]
        XCTAssertEqual(anIntArrayUnw, [10, 2, 25, 103])

    }

    func testCollectionOfFloats() {
        let testDict: ACDictionary = ["aFloatArray": [10.2, 2.3, 25.5, 103.5] as [Float],
                        "aFloatArrayFromTheStrings": ["10.2", "2.3", "25.5", "103.5"] as AnyObject]
        let floatArray: [Float] = [10.2, 2.3, 25.5, 103.5]
        var anFloatArray: [Float]?

        var anFloatArrayUnw = [Float]()

        anFloatArray <~ testDict["aFloatArray"]
        XCTAssertEqual(anFloatArray!, (testDict["aFloatArray"] as? [Float])!)

        anFloatArrayUnw <~ testDict["aFloatArray"]
        XCTAssertEqual(anFloatArrayUnw, (testDict["aFloatArray"] as? [Float])!)

        anFloatArray <~ testDict["aFloatArrayFromTheStrings"]
        XCTAssertEqual(anFloatArray!, (testDict["aFloatArray"] as? [Float])!)

        anFloatArrayUnw <~ testDict["aFloatArrayFromTheStrings"]
        XCTAssertEqual(anFloatArrayUnw, (testDict["aFloatArray"] as? [Float])!)

        anFloatArray <~ floatArray
        XCTAssertEqual(anFloatArray!, [10.2, 2.3, 25.5, 103.5])

    }

    func testCollectionOfDoubles() {

        let testDict: ACDictionary = ["aDoubleArray": ([10.2, 2.3, 25.5, 103.5] as AnyObject),
                                      "aDoubleArrayFromTheStrings": (["10.2", "2.3", "25.5", "103.5"] as AnyObject)]

        var anDoubleArray: [Double]?

        var anDoubleArrayUnw = [Double]()

        anDoubleArray <~ testDict["aDoubleArray"]
        XCTAssertEqual(anDoubleArray!, (testDict["aDoubleArray"] as? [Double])!)

        anDoubleArrayUnw <~ testDict["aDoubleArray"]
        XCTAssertEqual(anDoubleArrayUnw, (testDict["aDoubleArray"] as? [Double])!)

        anDoubleArray <~ testDict["aDoubleArrayFromTheStrings"]
        XCTAssertEqual(anDoubleArray!, (testDict["aDoubleArray"] as? [Double])!)

        anDoubleArrayUnw <~ testDict["aDoubleArrayFromTheStrings"]
        XCTAssertEqual(anDoubleArrayUnw, (testDict["aDoubleArray"] as? [Double])!)

        anDoubleArray <~ [10.2, 2.3, 25.5, 103.5]
        XCTAssertEqual(anDoubleArray!, [10.2, 2.3, 25.5, 103.5])

        anDoubleArrayUnw <~ [10.2, 2.3, 25.5, 103.5]
        XCTAssertEqual(anDoubleArrayUnw, [10.2, 2.3, 25.5, 103.5])

    }

    func testMappable() {
        let testDict: ACDictionary  = ["anInt": 10 as AnyObject, "operator": ["anInt": 8] as AnyObject]

        let mappableObj = OperatorTest(dict: testDict)
        XCTAssertEqual(mappableObj.anInt!, testDict["anInt"] as? Int)
        XCTAssertEqual(mappableObj.anIntUnw, testDict["anInt"] as? Int)
        XCTAssertEqual(mappableObj.operatorSub!.anInt, 8)
        XCTAssertEqual(mappableObj.operatorSubUnw.anInt, 8)

    }

//    func testEnumType() {
//        let testDict = "clear"
//        var typeEnum: ACMWForecastWeatherType?
//        typeEnum <~ testDict
//        XCTAssertEqual(typeEnum!.Day, ACMWForecastType.Clear)
//        XCTAssertEqual(typeEnum!.Night, ACMWForecastType.Night_Clear)
//
//    }

}
