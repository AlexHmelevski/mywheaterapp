//
//  ACWundegroundHurricaneForecastModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-29.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

class ACWundegroundHurricaneForecastModel: ACMappableProtocol {
    private(set) var category: String?
    private(set) var errorRadius: Double?
    private(set) var forecastHour: String?
    private(set) var fspeed: ACWundegroundSpeedModel?
    private(set) var movement: ACWundegroundMovementModel?
    private(set) var saffirSimpsonCategory: Int?
    private(set) var seaQuadrants: ACWundegroundWindQuadrantsModel?
    private(set) var seaRadius: ACWundegroundWindRadiusModel?
    private(set) var time: ACWundegroundTimeModel?
    private(set) var timeGMT: ACWundegroundTimeModel?
    private(set) var windGust: ACWundegroundSpeedModel?
    private(set) var windQuadrants: ACWundegroundWindQuadrantsModel?
    private(set) var windRadius: ACWundegroundWindRadiusModel?
    private(set) var windSpeed: ACWundegroundSpeedModel?
    private(set) var lat: Double?
    private(set) var lon: Double?

    required init(dict: ACDictionary) {
        self.category <~ dict["Category"]
        self.errorRadius <~ dict["ErrorRadius"]
        self.forecastHour <~ dict["ForecastHour"]
        self.fspeed <~ dict["Fspeed"]
        self.movement <~ dict["Movement"]
        self.saffirSimpsonCategory <~ dict["SaffirSimpsonCategory"]
        self.seaQuadrants <~ dict["SeaQuadrants"]
        self.seaRadius <~ dict["SeaRadius"]
        self.time <~ dict["Time"]
        self.timeGMT <~ dict["TimeGMT"]
        self.windGust <~ dict["WindGust"]
        self.windQuadrants <~ dict["WindQuadrants"]
        self.windRadius <~ dict["WindRadius"]
        self.windSpeed <~ dict["WindSpeed"]
        self.lat <~ dict["lat"]
        self.lon <~ dict["lon"]
    }
}
