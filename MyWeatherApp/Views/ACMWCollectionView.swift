//
//  ACMWCollectionView.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-12.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import UIKit

public class ACMWCollectionView: UICollectionView {

	var refresh: ACMWRefreshControl?

	override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
		super.init(frame: frame, collectionViewLayout: layout)
		self.refresh = ACMWRefreshControl()
		refresh?.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: 0)
		self.addSubview(refresh!)

	}

	override public var contentOffset: CGPoint {
		willSet {
			if let refresh = self.refresh {
				if newValue.y < -CGFloat(refresh.height) { refresh.refreshing = true }
				if newValue.y <= 0 && newValue.y > -CGFloat(refresh.height) && refresh.refreshing {
					self.contentInset = UIEdgeInsets(top: -newValue.y, left: 0, bottom: 0, right: newValue.y)
				}
			}
		}
	}

	required public init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	override public func layoutSubviews() {
		super.layoutSubviews()
		self.refresh?.frame = CGRect(x: 0, y: self.contentOffset.y, width: self.bounds.width, height: -self.contentOffset.y)
	}

	override public func  dequeueReusableCell(withReuseIdentifier identifier: String, for indexPath: IndexPath) -> ACMWCollectionViewCell {
        guard let cell = super.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as? ACMWCollectionViewCell  else {
            fatalError("Unexpected type in \(#function)")
        }
		return cell
	}

}
