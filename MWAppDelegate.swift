//
//  MyAppDelegate.swift
//  MyWeatherApp
//
//  Created by Alex Hmelevski on 2017-02-13.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import Foundation

@UIApplicationMain
class MWAppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var flowController = FlowController()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        window.do(work: flowController.start)
        
        return true
    }

}

