//
//  UIFont(Ext).swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-10.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {

	class func getFamilyFontsWithParametrs(_ fontName: String, bold: Bool, Italic: Bool, Condensed: Bool) -> [UIFont] {
		func createFont(_ name: String) -> UIFont {
			let font = UIFont(name: name, size: 1)!
			return font
		}

		func isSearchedAttributes(_ font: UIFont) -> Bool {
			var isNeededAttribute = false
			if let traits = font.fontDescriptor.object(forKey: UIFontDescriptorTraitsAttribute) as? [String : Any] ,
				let weight = traits[UIFontWeightTrait] as? Double,
				let slant = traits[UIFontSlantTrait] as? Double,
				let width = traits[UIFontWidthTrait] as? Double {

					if bold && Italic && Condensed {
						isNeededAttribute = isNeededAttribute || weight != 0 && width != 0 && slant != 0
					} else if bold && Italic {
						isNeededAttribute = isNeededAttribute || weight != 0 && slant != 0 && width == 0
					} else if bold && Condensed {
						isNeededAttribute = isNeededAttribute || weight != 0 && slant == 0 && width != 0
					} else if Italic && Condensed {
						isNeededAttribute = isNeededAttribute || weight == 0 && slant != 0 && width != 0
					} else {
						if bold {
							isNeededAttribute = isNeededAttribute || weight != 0 && width == 0 && slant == 0
						}
						if Italic {
							isNeededAttribute = isNeededAttribute || weight == 0 && width == 0 && slant != 0
						}

						if Condensed {
							isNeededAttribute = isNeededAttribute || weight == 0 && width != 0 && slant == 0
						}

					}
			}
			return isNeededAttribute
		}

		var arrayOfFonts: [UIFont] = []
		if let font = UIFont(name: fontName, size: 1) {
			let fontNames = UIFont.fontNames(forFamilyName: String(describing: font.fontDescriptor.object(forKey: UIFontDescriptorFamilyAttribute)!))
			arrayOfFonts = fontNames.map(createFont)
		}
		let filtered = arrayOfFonts.filter(isSearchedAttributes)

		return filtered
	}

	func fontWeight() -> Double {
		let traits = self.fontDescriptor.object(forKey: UIFontDescriptorTraitsAttribute) as? [String : Any]
		return traits?[UIFontWeightTrait] as? Double ?? 0
	}

	func fontSize() -> Double {
        guard let size = self.fontDescriptor.object(forKey: UIFontDescriptorSizeAttribute) as? Double else {
            fatalError("Expected double type in \(#function)")
        }
		return size
	}

	public class func getThickerFont(_ forFont: UIFont?) -> UIFont? {
		guard let font = forFont else { return nil }
		var fonts = UIFont.getFamilyFontsWithParametrs(font.fontName, bold: true, Italic: false, Condensed: false)
		fonts.append(contentsOf: UIFont.getFamilyFontsWithParametrs(font.fontName, bold: true, Italic: false, Condensed: true))
		fonts.append(contentsOf: UIFont.getFamilyFontsWithParametrs(font.fontName, bold: true, Italic: true, Condensed: false))

		guard !fonts.isEmpty else {
			print("\(#function) Couldn't find a thiker font for \(font). Returning the input Font")
			return forFont
		}

		let closure = { (newFont: UIFont) -> Bool in
			return newFont.fontWeight() >= font.fontWeight()
		}

		let thickerFonts = fonts.filter(closure).sorted { (first, second) -> Bool in
			first.fontWeight() < second.fontWeight()
		}

		return thickerFonts.first
	}

}
