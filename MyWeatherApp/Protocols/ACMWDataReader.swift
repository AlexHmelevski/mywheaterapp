//
//  ACMWDataSource.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-15.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation
import AHFuture

public protocol ACMWDataReader {

    func read() -> AHFuture<ACWundergroundModel, ACMWError>
}
