//
//  GlobalAlertErrorHandler.swift
//  MyWeatherApp
//
//  Created by Alex Hmelevski on 2017-02-09.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import Foundation
final class GlobalAlertErrorHandler: BasicErrorHandler {

    override func catchError(error: Error) {
        guard let er = error as? ACMWError else {
            super.catchError(error: error)
            return
        }
        showError(error: er)
    }

    private func showError(error: ACMWErrorType) {
        let alertController = alertControllerFor(error)

        DispatchQueue.main.async(execute: {
            UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
        })
    }

    private func alertControllerFor(_ error: ACMWErrorType) -> UIAlertController {

        let alertController = UIAlertController(title: "Network Error", message: error.description, preferredStyle: UIAlertControllerStyle.alert)

        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (_) in
            alertController.dismiss(animated: true, completion: nil)
        }))
        return alertController
    }
}
