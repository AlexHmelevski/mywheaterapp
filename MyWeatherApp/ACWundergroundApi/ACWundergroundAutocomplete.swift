//
//  ACWundergroundAutocomplete.swift
//  MyWeatherApp
//
//  Created by Alex Hmelevski on 2017-06-23.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import Foundation
import AHFuture

final class ACWundergroundAutocomplete {
    var network = AHNetworkRequester()

    func getAutocompleteResults(for text: String) -> AHFuture<AHWundergroundAutocompleteResultModel, ACMWNetworkError> {
       return network.getData(for: .autocomplete(text: text))
    }
}
