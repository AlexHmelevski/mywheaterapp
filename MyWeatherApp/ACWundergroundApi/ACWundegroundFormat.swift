//
//  ACWundegroundFormat.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-26.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation
enum ACWundegroundFormat: String {
    case JSON = ".json"
    case XML = ".xml"
}
