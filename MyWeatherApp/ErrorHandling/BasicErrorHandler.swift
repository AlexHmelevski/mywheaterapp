//
//  BasicErrorHandler.swift
//  MyWeatherApp
//
//  Created by Alex Hmelevski on 2017-02-09.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import Foundation
protocol ILogger {
    func log(message: String)
}

class BasicErrorHandler: IErrorHandler {
    private var nextLink: BasicErrorHandler?
    var logger: ILogger?

    required init() {}

    class func createChain(from types: [BasicErrorHandler.Type]) -> IErrorHandler {
        var link: BasicErrorHandler?
        for t in types.reversed() {
            let extLink = link
            link = t.init()
            link?.nextLink = extLink
        }
        guard let unLink = link else { fatalError("Can't create a chain from \(types)") }
        return unLink
    }

    func catchError(error: Error) {
        nextLink.do(work: { $0.catchError(error: error) })
                .doIfNone(work: { self.logError(error: error) })
    }

    private func logError(error: Error) {
        print("ERROR: \(error)")
    }
}
