//
//  TextFormatter.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-01-04.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

public enum ACMWFormatType: String {
    case Celsius = "°C"
    case Fahrenheit = "°F"
    case Simple = "°"
    case Percent = "%"

}

public class ValueFormatter {

    private var formatter: ValueFormatterProtocol?

    public init(formatType: ACMWFormatType) {
        switch formatType {
            case .Celsius: self.formatter = CelsiusDegreeFormatter()
            case .Fahrenheit: self.formatter = FahrenheitDegreeFormatter()
            case .Simple: self.formatter = SimpleDegreeFormatter()
            case .Percent: self.formatter = PercentValueFormatter()
        }
    }

    public func formatValue(_ value: Int) -> String? {
        //TODO: check here global settings and change formatter
        return formatter?.formatValue(value)
    }

    public var type: ACMWFormatType {
        return formatter!.type
    }

}

private protocol ValueFormatterProtocol {
    var type: ACMWFormatType { get }
    func formatValue(_ value: Int) -> String
}

private class CelsiusDegreeFormatter: ValueFormatterProtocol {
    func formatValue(_ value: Int) -> String {
        return "\(value)°C"
    }
    var type: ACMWFormatType { return .Celsius }
}

private class SimpleDegreeFormatter: ValueFormatterProtocol {
    func formatValue(_ value: Int) -> String {
        return "\(value)°"
    }

    var type: ACMWFormatType { return .Simple }
}

private class FahrenheitDegreeFormatter: ValueFormatterProtocol {
    func formatValue(_ value: Int) -> String {
        return "\(value)°F"
    }
    var type: ACMWFormatType { return .Fahrenheit }
}

private class PercentValueFormatter: ValueFormatterProtocol {
    func formatValue(_ value: Int) -> String {
        return "\(value)%"
    }
    var type: ACMWFormatType { return .Percent }
}
