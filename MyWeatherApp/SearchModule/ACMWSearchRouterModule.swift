//
//  ACMWSearchRouterModule.swift
//  MyWeatherApp
//
//  Created by Alex Hmelevski on 2017-06-23.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import Foundation

final class ACMWSearchRouterModule: IRouterModule {
    var viewController: UIViewController {
        return vc
    }

    private let vc: ACMWSearchViewController
    private let mediator: ACMWSearchMediator

    init(delegate: ACMWSearchRouterModuleDelegate?) {
        vc = ACMWSearchViewController()
        let presenter = ACMWSearchPresenter(viewOutput: vc)
        let interactor = ACMWSearchInteractor(searchPresenter: presenter)
        mediator = ACMWSearchMediator(interactor: interactor, delegate: delegate)
        vc.input = mediator
    }
}
