//
//  ACMWPersistenceError.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-17.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

public enum ACMWPersistenceError: ACMWErrorType {
	case fileNotFound(fileName: String)

    public var description: String {
		return "File not Found"
	}
}
