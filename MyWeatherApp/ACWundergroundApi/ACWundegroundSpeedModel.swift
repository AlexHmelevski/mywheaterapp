//
//  ACWundegroundSpeedModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-29.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundegroundSpeedModel: ACMappableProtocol {
    private(set) var degrees: Int = 0
    private(set) var dir = String()
    private(set) var kph: Int = 0
    private(set) var kts: Int = 0
    private(set) var mph: Int = 0
    init(dict: ACDictionary) {
        self.degrees <~ dict["degrees"]
        self.dir <~ dict["dir"]
        self.kph <~ (dict["Kph"] ?? dict["kph"])
        self.kts <~ dict["Kts"]
        self.mph <~ (dict["Mph"] ?? dict["mph"])
    }
}
