//
//  ACWundegroundSettings.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-25.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

enum ACWundegroundSettingType {
    case language(String)
    case bestForecast(Bool)
    case pws(Bool)

    var path: String {
        switch self {
        case let .language(lang): return "/lang:\(lang)"
        case let .bestForecast(use): return "/bestfct:\(use)"
        case let .pws(use):return "/pws:\(use)"
        }
    }
}
