//
//  ACWundergroundPeriodModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-30.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundergroundPeriodModel: ACMappableProtocol {
    private(set) var date_end: ACWundergroundDateModel?
    private(set) var date_start: ACWundergroundDateModel?

    init(dict: ACDictionary) {
        self.date_end <~ dict["date_end"]
        self.date_start <~ dict["date_start"]
    }
}
