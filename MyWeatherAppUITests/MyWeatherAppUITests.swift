//
//  MyWeatherAppUITests.swift
//  MyWeatherAppUITests
//
//  Created by Alex Hmelevski on 2017-07-01.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import XCTest


@testable import MyWeatherApp
class MyWeatherAppUITests: XCTestCase {
    let app = XCUIApplication()
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
//        XCTAssertEqual(GeneralProjectConfig.isTestTarget, true)
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app.launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_func() {
        
        app.collectionViews.buttons["addButton"].tap()
        app.otherElements["AutocompleteSearchBar"].children(matching: .searchField).element.tap()
        app.otherElements["AutocompleteSearchBar"].children(matching: .searchField).element.typeText("Montreal")
        
        
        app.tables/*@START_MENU_TOKEN@*/.staticTexts["Montreal, Canada"]/*[[".cells.staticTexts[\"Montreal, Canada\"]",".staticTexts[\"Montreal, Canada\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.collectionViews.element.swipeUp()
        
    }
    
    func testExample() {
        
                
        
    }
    
}
