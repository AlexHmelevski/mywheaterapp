//
//  UIFont+UIFont_SmallCaps.h
//  MyWeatherApp
//
//  Created by Alex Crow on 2015-08-16.
//  Copyright (c) 2015 Alex Crow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (UIFont_SmallCaps)
+ (UIFont *) applicationSmallCapsFontWithSize:(CGFloat) size;
@end
