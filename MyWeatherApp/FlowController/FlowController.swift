//
//  FlowController.swift
//  MyWeatherApp
//
//  Created by Alex Hmelevski on 2017-02-13.
//  Copyright © 2017 Alex Crow. All rights reserved.
//

import Foundation
import AHContainer

final class FlowController: NSObject {
    var currentController: UIViewController = UIViewController()
    fileprivate var currentControllerIndex: Int = 0
    fileprivate var controllers = [UIViewController]()
    fileprivate var page = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    fileprivate let slideSceneType = UISlideControllerScene.self
    private var scene: IUIScene?
    fileprivate var routerModule = ACMWSearchRouterModule(delegate: nil)
    fileprivate var modules: [IRouterModule]  = []
    var moduleFactory: MainForecastModuleFactory = ACMWMainForecastModuleFactory()
    var container: ALViewControllerContainer!
    var factory = DefaultAnimationProviderFactory()

    override init() {
        super.init()
    }

    func start(inWindow window: UIWindow) {
        currentController = moduleFactory.module(for: .empty, withDelegate: self).viewController
        page.dataSource = self
        page.setViewControllers([currentController], direction: .forward, animated: false, completion: nil)
        container = ALViewControllerContainer(initialVC: page)
        window.rootViewController = container
        window.makeKeyAndVisible()
    }

    func openSettings() {
        let slideMenu = ACMWSlideMenuViewController.create()
        let proportion = SizeProportion(width: 0.7, height: 1)
        var slideRightProvider = factory.provider(for: .slideRight(size: proportion),
                                                  dimmingViewType: .defaultBlur)
        slideRightProvider.dimmingTapHandler = {
            self.container.pop(using: self.factory.provider(for: .slideLeft(size: proportion), dimmingViewType: .noDimming), completion: nil)
        }

        container.push(controller: slideMenu,
                       with: slideRightProvider,
                       completion: {(_) in
                        print(self.container.view.subviews)

        })

    }

    func openShare() {
        let share = UIActivityViewController(activityItems: [], applicationActivities: nil)
        currentController.present(share, animated: true, completion: nil)
    }

    func addCity() {
        routerModule = ACMWSearchRouterModule(delegate: self)
        container.push(controller: routerModule.viewController,
                       with: factory.provider(for: .slideUp(size: .equal),
                       dimmingViewType: .defaultBlur), completion: nil)
    }

    func associateController(controller: UIPageViewController) {
        self.page = controller
    }
}

extension FlowController : ACMWSearchRouterModuleDelegate {
    func didFinish(with result: ACMWSearchRestultDataModel?) {
        container.pop(using: factory.provider(for: .slideDown, dimmingViewType: .noDimming), completion: nil)

        result.map({ [weak self] in ([.zmw(value: $0.queryZMW)], self) })
              .map(ACMWMainForecastRouterModule.init)
              .do(work: { (module) in
                self.modules += [module]
                self.currentController = module.viewController

                self.page.setViewControllers([module.viewController], direction: .forward, animated: false, completion: nil)
              })

    }

    func append(vc: UIViewController) {

    }
}

extension FlowController: ForecastModuleDelegate {
    func didPressAdd() {
        addCity()
    }
    func didPressSettings() {
        openSettings()
    }
}

extension FlowController : UIPageViewControllerDataSource {

    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
         return index(of: viewController).map({ $0 + 1 }).flatMap(nextController)
    }

    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        return index(of: viewController).map({ $0 - 1 }).flatMap(nextController)
    }

    private func index(of viewController: UIViewController) -> Int? {
        return modules.map({ $0.viewController }).index(of: viewController)
    }

    private func nextController(from index: Int) -> UIViewController? {
        return modules.enumerated().first(where: { $0.offset == index }).map({ $0.element.viewController })
    }
}

extension FlowController: ICityForecastVCObserver {
    func buttonPressed(type: NavigationButtonTypes) {
        switch type {
            case .add: addCity()
            case .settings: openSettings()
            case .share: openShare()
        }
    }
}
