//
//  ACMWHourForecastCell.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-18.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

class ACMWHourForecastCell: ACMWForecastCell {
    private var hourLabel: UILabel!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override init(frame: CGRect) {
        hourLabel = UILabel()
        super.init(frame: frame)
        configureAutoLayout()
    }

    override func configureCell(_ forecastData: ACMWCellData) {
        self.temperatureLabel.text = String(forecastData.temperature)
        self.forecastIcon.weatherType = forecastData.weatherIcon
        self.hourLabel.text = String(forecastData.hour) + ":00"
    }

    override internal func configureAutoLayout() {
        self.contentView.addSubviews(forecastIcon, hourLabel, temperatureLabel)
        self.contentView.setTranslatesAutoresizingMaskInSubviews(false)
        forecastIcon.contentMode = UIViewContentMode.scaleAspectFit
        let views: [UIView]  = [hourLabel, forecastIcon, temperatureLabel]
        NSLayoutConstraint.allignVerticaly(views, horizontalAllignmentOptions: .center, boundToSuperView: true)
        NSLayoutConstraint.equalWitdh([hourLabel, temperatureLabel, forecastIcon])
        appearanceProxy.label(hourLabel, with: ACMWDailyForecast)
    }

}
