//
//  ACMWSlideMenuViewController.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 2015-08-30.
//  Copyright (c) 2015 Alex Crow. All rights reserved.
//

import Foundation
import UIKit

class ACMWSlideMenuViewController: UIViewController {

    class func create() -> ACMWSlideMenuViewController {
        let nibFiles = Bundle.main.loadNibNamed("SlideMenu", owner: nil, options: nil)

        guard let vc = nibFiles?.first as? ACMWSlideMenuViewController else {
            fatalError("Can't create ACMWSlideMenuViewController")
        }

        return vc
    }

    override func awakeFromNib() {

        modalPresentationStyle = .custom
        self.view.alpha = 0.2
    }

    @IBAction func swipeLeftDone(_ sender: UISwipeGestureRecognizer) {

        self.presentingViewController?.dismiss(animated: true, completion: nil)

    }
}
