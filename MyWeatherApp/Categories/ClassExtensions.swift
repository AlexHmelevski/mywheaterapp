//
//  NSObject+Kind.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 15-11-20.
//  Copyright © 2015 Alex Crow. All rights reserved.
//

//import Foundation
import CoreImage
import UIKit

extension NSObject {
    class var kind: String {
		return NSStringFromClass(self.classForCoder())
	}
}

extension CAGradientLayer {
	class func alphaGradientLayer(_ color: UIColor, startAlpha: Float, endAlpha: Float) -> CAGradientLayer {
		let colorStart = color.withAlphaComponent(CGFloat(startAlpha)).cgColor
		let colorEnd = color.withAlphaComponent(CGFloat(endAlpha)).cgColor
		let gradient = CAGradientLayer()
		gradient.colors = [colorStart, colorEnd]
		return gradient
	}
}

extension UIView {
	func addSubviews(_ subviews: UIView ...) {
		for view in subviews {
			self.addSubview(view)

		}
	}

	func setTranslatesAutoresizingMaskInSubviews(_ value: Bool) {
		for view in self.subviews {
			view.translatesAutoresizingMaskIntoConstraints = value
		}
	}

	func constraintsReferenceView(_ view: UIView) -> [NSLayoutConstraint] {
		var array: [NSLayoutConstraint] = []
		for constraint in self.constraints {
			if constraint.firstItem.isEqual(view) || (constraint.secondItem != nil && constraint.secondItem!.isEqual(view)) {
				array += [constraint]
			}
		}
		return array
	}

	func containsObjectOfClass(_ classType: AnyClass) -> Bool {
		for subview in self.subviews {

            if subview.isKind(of: classType) { return true }
		}
		return false
	}
}

extension Locale {
	static func getCurrentLanguage() -> String {
		guard var lString = Locale.preferredLanguages.first,
            let charRange = lString.range(of: "-") else {

				return ""
		}

        lString.replaceSubrange(charRange.lowerBound..<lString.endIndex, with: "")
		return lString
	}
}
