//
//  ACWundergroundForecastDayModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-29.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundergroundForecastArrayModel: ACMappableProtocol {
    private(set) var date = String()
    private(set) var forecastday: [ACWundegroundForecastDayModel]?

    init(dict: ACDictionary) {
        self.date <~ dict["date"]
        self.forecastday <~ dict["forecastday"]
    }
}
