//
//  BasicTypeTest.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-10.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation
import CoreGraphics

class BasicTypeTest: ACMappableProtocol {
    var anInt: Int?
    var anIntUnw: Int = 0
    var anIntUnwFromString: Int = 0
    var anIntFromString: Int?

    var aBool: Bool?
    var aBoolFromString: Bool?
    var aBoolUnw: Bool = true
    var aBoolFromStringUnw: Bool = true

    var aDouble: Double?
    var aDoubleFromString: Double?
    var aDoubleUnw: Double = 0.0
    var aDoubleFromStringUnw: Double = 0.0

    var aCGFloat: CGFloat?
    var aCGFloatFromString: CGFloat?
    var aCGFloatUnw: CGFloat = 0.0
    var aCGFloatFromStringUnw: CGFloat = 0.0

    var aFloat: Float?
    var aFloatFromString: Float?
    var aFloatUnw: Float = 0.0
    var aFloatFromStringUnw: Float = 0.0

    var aString: String?
    var aStringUnw: String = String()
    var aStringCollection: [String]?
    var aStringCollectionUnw = [String]()

    var aDate: Date?
    var aBoolArray: [Bool]?
    var aBoolArrayFromStrings: [Bool]?

    var aBoolArrayUnw = [Bool]()
    var aBoolArrayFromStringsUnw = [Bool]()

    var anIntArray: [Int]?
    var anIntArrayFromTheStrings: [Int]?
    var anIntArrayUnw = [Int]()
    var anIntArrayFromTheStringsUnw = [Int]()

    var aFloatArray: [Float]?
    var aFloatArrayFromTheStrings: [Float]?
    var aFloatArrayUnw = [Float]()
    var aFloatArrayFromTheStringsUnw = [Float]()

    var aDoubleArray: [Double]?
    var aDoubleArrayFromTheStrings: [Double]?
    var aDoubleArrayUnw = [Double]()
    var aDoubleArrayFromTheStringsUnw = [Double]()

    var anEnum: ACMWForecastType?
    var anEnumFromString: ACMWForecastType?

    let timeStamp = CFAbsoluteTimeGetCurrent()
    let checkDate: Date

    required init(dict: ACDictionary) {
        self.anInt <~ dict["Int"]
        self.anIntFromString <~ dict["anIntFromString"]
        self.anIntUnw <~ dict["Int"]
        self.anIntUnwFromString <~ dict["anIntFromString"]

        self.aBool <~ dict["aBool"]
        self.aBoolFromString <~ dict["aBoolFromString"]
        self.aBoolUnw <~ dict["aBool"]
        self.aBoolFromStringUnw <~ dict["aBoolFromString"]

        self.aDouble <~ dict["aDouble"]
        self.aDoubleFromString <~ dict["aDoubleFromString"]
        self.aDoubleUnw <~ dict["aDouble"]
        self.aDoubleFromStringUnw <~ dict["aDoubleFromString"]

        self.aCGFloat <~ dict["aCGFloat"]
        self.aCGFloatFromString <~ dict["aCGFloatFromString"]
        self.aCGFloatUnw <~ dict["aCGFloat"]
        self.aCGFloatFromStringUnw <~ dict["aCGFloatFromString"]

        self.aFloatFromString <~ dict["aFloatFromString"]
        self.aFloat <~ dict["aFloat"]
        self.aFloatFromStringUnw <~ dict["aFloatFromString"]
        self.aFloatUnw <~ dict["aFloat"]

        self.aString <~ dict["aString"]
        self.aStringUnw <~ dict["aString"]
        self.aStringCollection <~ dict["aStringCollection"]
        self.aStringCollectionUnw <~ dict["aStringCollection"]
        self.checkDate = Date(timeIntervalSince1970: self.timeStamp)

        self.aBoolArray <~ dict["aBoolArray"]
        self.aBoolArrayFromStrings <~ dict["aBoolArrayFromStrings"]
        self.aBoolArrayUnw <~ dict["aBoolArray"]
        self.aBoolArrayFromStringsUnw <~ dict["aBoolArrayFromStrings"]

        self.anIntArray <~ dict["anIntArray"]
        self.anIntArrayFromTheStrings <~ dict["anIntArrayFromTheStrings"]
        self.anIntArrayUnw <~ dict["anIntArray"]
        self.anIntArrayFromTheStringsUnw <~ dict["anIntArrayFromTheStrings"]

        self.aFloatArray <~ dict["aFloatArray"]
        self.aFloatArrayFromTheStrings <~ dict["aFloatArrayFromTheStrings"]
        self.aFloatArrayUnw <~ dict["aFloatArray"]
        self.aFloatArrayFromTheStringsUnw <~ dict["aFloatArrayFromTheStrings"]

        self.aDoubleArray <~ dict["aDoubleArray"]
        self.aDoubleArrayFromTheStrings <~ dict["aDoubleArrayFromTheStrings"]
        self.aDoubleArrayUnw <~ dict["aDoubleArray"]
        self.aDoubleArrayFromTheStringsUnw <~ dict["aDoubleArrayFromTheStrings"]

        self.anEnum <~ dict["anEnum"]
        self.anEnumFromString <~ dict["anEnumFromString"]
    }
}
