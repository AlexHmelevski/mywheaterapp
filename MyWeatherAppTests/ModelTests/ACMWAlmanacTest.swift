//
//  ACMWAlmanacTest.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-21.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import XCTest
//import MyWeatherApp

class ACMWAlmanacTest: ACMWModelTest {
    override var modelName: String {
        return "AlmanacSource"
    }

    func testParse() {
        let model = ACWundergroundModel(dict: self.dataDictionary!)
        var normal = ACWundergroundValue(dict: ["F": 71.0, "C": 22.0 ])
        var record = ACWundergroundValue(dict: ["F": 89.0 as AnyObject, "C": 31.0 as AnyObject])
        var rYear = "1970"
        XCTAssertNotNil(model, "Cannot parse from dictionary")
        XCTAssertEqual(model.almanac!.airport_code, "KSFO")
        XCTAssertEqual(model.almanac!.temp_high.normal!, normal)
        XCTAssertEqual(model.almanac!.temp_high.record!, record)
        XCTAssertEqual(model.almanac!.temp_high.recordyear!, rYear)

        normal = ACWundergroundValue(dict: ["F": 54.0 as AnyObject, "C": 12.0 as AnyObject])
        record = ACWundergroundValue(dict: ["F": 48.0 as AnyObject, "C": 8.0 as AnyObject])
        rYear = "1953"
        XCTAssertEqual(model.almanac!.temp_low.normal!, normal)
        XCTAssertEqual(model.almanac!.temp_low.record!, record)
        XCTAssertEqual(model.almanac!.temp_low.recordyear!, rYear)
    }

}
