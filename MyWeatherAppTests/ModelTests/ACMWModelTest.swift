//
//  ACMWAlmanacModelTest.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-21.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import XCTest

class ACMWModelTest: XCTestCase {
    var modelName: String {
        return ""
    }
    var dataDictionary: ACDictionary?

    override func setUp() {
        super.setUp()
        let classBundle = Bundle(for: self.classForCoder)
        let path = classBundle.path(forResource: self.modelName, ofType: "json")
        let data = try? Data(contentsOf: URL(fileURLWithPath: path!))
        self.dataDictionary = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! ACDictionary
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
