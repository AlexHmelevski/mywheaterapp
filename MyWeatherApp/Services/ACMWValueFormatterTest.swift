//
//  ACMWValueFormatterTest.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-09.
//  Copyright © 2016 Alex Crow. All rights reserved.
//
@testable import MyWeatherApp
import XCTest

class ACMWFormatterTest: XCTestCase {

    func testValueFormatter() {
        let formatters = [ValueFormatter(formatType : (ACMWFormatType.Celsius )),
                          ValueFormatter(formatType : (ACMWFormatType.Fahrenheit )),
                          ValueFormatter(formatType : (ACMWFormatType.Simple )),
                          ValueFormatter(formatType : (ACMWFormatType.Percent ))]
        for formatter in formatters {
            for i in stride(from: 1, to: 100, by: 50) {
                let result = formatter.formatValue(i)
                XCTAssert(result!.contains(formatter.type.rawValue))
            }
        }
    }

    func testSmallCapsFormatterTemperature() {
        self.checkTemperatureString("11")
        self.checkTemperatureString("11T")
        self.checkTemperatureString("T11T")
        self.checkTemperatureString("")
    }

    func testSmallCapsFormatterLabels() {
        self.checkString("")
        self.checkString("T")
        self.checkString("TEST")
    }

    private func checkString(_ string: String) {
        let fontSize: CGFloat = 15.0

        let fonts = UIFont.familyNames
        for family in fonts {
            let names = UIFont.fontNames(forFamilyName: family)
            for fName in names {
                let smallCapsFormatter = ACMWTextFormatter(fontName: fName,
                                                           fontSize: Double(fontSize),
                                                               type: .textLabels)
                let string = smallCapsFormatter.convert(string)
                XCTAssertNotNil(string, string.string)

                var font: UIFont

                if string.length != 0 {
                    font = self.attributeAtIndex(string, index: 0)
                    XCTAssertEqual(fName, font.fontName)
                    XCTAssertEqual(font.pointSize, fontSize)
                }

                if string.length > 1 {
                    font = self.attributeAtIndex(string, index: 1)
                    XCTAssertLessThan(font.pointSize, fontSize)
                }
            }
        }
    }

    private func checkTemperatureString(_ string: String) {
        let fontSize: CGFloat = 15.0
        let set = CharacterSet.decimalDigits

        let fonts = UIFont.familyNames
        for family in fonts {
            let names = UIFont.fontNames(forFamilyName: family)
            for fName in names {
                let smallCapsFormatter = ACMWTextFormatter(fontName: fName,
                                                           fontSize: Double(fontSize),
                                                               type: ACMWTextFormatter.FormatterStyle.temperature)
                let atString = smallCapsFormatter.convert(string)
                XCTAssertNotNil(string, atString.string)

                var font: UIFont

                if atString.length != 0 {
                    font = self.attributeAtIndex(atString, index: 0)
                    XCTAssertEqual(fName, font.fontName)
                    XCTAssertEqual(font.pointSize, fontSize)
                }

                if atString.length > 1 {
                    if let range = string.rangeOfCharacter(from: set) {
                        let idx = string.characters.distance(from: string.startIndex, to: range.upperBound)
                        let idxStart = string.characters.distance(from: string.startIndex, to: range.lowerBound)
                        if  idx + 1 < atString.length && idxStart == 0 {
                            font = self.attributeAtIndex(atString, index: idx + 1)
                            XCTAssertLessThan(font.pointSize, fontSize, string)
                        }

                    }
                }
            }
        }
    }

    private func attributeAtIndex(_ string: NSAttributedString, index: Int) -> UIFont {
        var range = NSRange()
        var attrDictionary = string.attributes(at: index, effectiveRange: &range)
        guard let font = attrDictionary["NSFont"] as? UIFont else {
            fatalError("Not a UIFont")
        }
        return font
    }
}
