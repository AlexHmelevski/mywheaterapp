//
//  ACWundegroundConditionsModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-28.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundegroundHistoryModel: ACMappableProtocol {
    private(set) var dailysummary: [ACWundegroundHistorySummaryModel]?
    private(set) var observations: [ACWundegroundConditionsModel]?
    private(set) var utcdate: ACWundegroundTimeModel?

    init(dict: ACDictionary) {
        self.dailysummary <~ dict["dailysummary"]
        self.observations <~ dict["observations"]
        self.utcdate <~ dict["utcdate"]

    }
}
