//  ACMWPersistenceSaver.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-04-17.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation
import ALEither
import AHFuture

public class ACMWPersistence: ACMWDataSaver, ACMWDataReader {

    /// Async data getter
    ///
    /// - Parameter completion: Complition that contains data
    public func getAsyncData(_ completion: ALWundergroundCompletionClosure?) {
        completion?(ALEither.wrong(value: ACMWError(error: ACMWNetworkError.notJSONModel)))
    }

    /// Function to save data
    ///
    /// - Parameter data: Data to save
    public func save(_ data: ACWundergroundModel) {

    }

    public func read() -> AHFuture<ACWundergroundModel, ACMWError> {
        return AHFuture(scope: { (_) in

        })
    }
}
