//
//  ACWundergroundTideSummaryModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-30.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundergroundTideSummaryModel: ACMappableProtocol {
    private(set) var data: ACWundergroundDataModel?
    private(set) var date: ACWundegroundTimeModel?
    private(set) var utcdate: ACWundegroundTimeModel?

    init(dict: ACDictionary) {
        self.data <~ dict["data"]
        self.date <~ dict["date"]
        self.utcdate <~ dict["utcdate"]
    }
}
