//
//  ACMWSlideTransitioningDelegate.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-05.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

class ACMWSlideTransitioningDelegate: NSObject, UIViewControllerTransitioningDelegate {

    func presentationController(forPresented presented: UIViewController,
                                presenting: UIViewController?,
                                source: UIViewController) -> UIPresentationController? {
        return ACMWSlidePresentationController(presentedViewController: presented, presenting: presenting)
    }

   func animationController(forPresented presented: UIViewController,
                            presenting: UIViewController,
                            source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return ACMWSlideAnimationDelegate(presenting: true)
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return ACMWSlideAnimationDelegate(presenting: false)
    }

}

class ACMWSlideAnimationDelegate: NSObject, UIViewControllerAnimatedTransitioning {
    private let presenting: Bool
    private let duration = 0.9
    init(presenting: Bool) {
        self.presenting = presenting
    }

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return self.duration
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        // Here, we perform the animations necessary for the transition

        guard let fromVC = transitionContext.viewController(forKey: .from),
                let fromView = fromVC.view,
                let toVC = transitionContext.viewController(forKey: .to),
                let toView = toVC.view else { return }

        if self.presenting {
            transitionContext.containerView.addSubview(toView)
        }

        let animatingVC = self.presenting ? toVC : fromVC
        guard let animatingView = animatingVC.view else { return }
        let appearedFrame = transitionContext.finalFrame(for: animatingVC)
        var dismissedFrame = appearedFrame
        dismissedFrame.origin.x -= dismissedFrame.size.width
        let initialFrame = self.presenting ? dismissedFrame : appearedFrame
        let finalFrame = self.presenting ? appearedFrame : dismissedFrame
        animatingView.frame = initialFrame

        UIView.animate(withDuration: self.transitionDuration(using: transitionContext),
                              delay: 0,
             usingSpringWithDamping: 300,
              initialSpringVelocity: 0.2,
                            options: [.allowUserInteraction, .beginFromCurrentState],
                         animations: { () -> Void in animatingView.frame = finalFrame }) { (_) -> Void in
                if !self.presenting {
                    fromView.removeFromSuperview()
                }
                transitionContext.completeTransition(true)
        }
    }
}
