//  ACMappableProtocol.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-26.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

public protocol ACMappableProtocol {
    init(dict: ACDictionary)
}

public protocol CustomFromStringConvertable {
    init?(_ text: String)
}

public protocol CustomFromStringConvertableInt {
    init?(_ text: String, radix: Int)
}
