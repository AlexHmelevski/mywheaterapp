//
//  ACWundergroundRawTideModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-30.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation

struct ACWundergroundRawTideModel: ACMappableProtocol {
    private(set) var rawTideObs: [ACWundergroundRawTideObsModel]?
    private(set) var rawTideStats: [ACWundergroundRawTideStatsModel]?
    private(set) var tideInfo: [ACWundergroundTideInfoModel]?

    init(dict: ACDictionary) {
        self.rawTideObs <~ dict["rawTideObs"]
        self.rawTideStats <~ dict["rawTideStats"]
        self.tideInfo <~ dict["tideInfo"]

    }
}
