//
//  ACWundergroundRawTideStatsModel.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 16-03-30.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation
struct ACWundergroundRawTideStatsModel: ACMappableProtocol {
    private(set) var maxheight: Double?
    private(set) var minheight: Double?
    init(dict: ACDictionary) {
        self.maxheight <~ dict["maxheight"]
        self.minheight <~ dict["minheight"]
    }
}
